/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.security;

import com.betafase.mcmanager.MCManager;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

/**
 *
 * @author dbrosch
 */
public class ModuleManager {

    private static List<String> temporary = null;

    public static boolean isValid(Player p, String permission) {
        if (isTempBanned(permission)) {
            return false;
        }
        boolean enabled = isModuleEnabled(permission);
        if (p != null) {
            if (MCManager.getConfiguration().getBoolean("negated_permissions")) {
                if (p.hasPermission("mcm.!" + permission) || p.hasPermission("mcm.not_" + permission)) {
                    if (!p.hasPermission("mcm.*") && !p.hasPermission("*") && !p.isOp()) {
                        return false;
                    }
                }
            }
            return enabled && (p.isOp() || p.hasPermission("*") || p.hasPermission("mcm.*") || p.hasPermission("mcm." + permission));
        }
        return enabled;
    }

    public static void tempban(String permission) {
        if (temporary == null) {
            temporary = new LinkedList<>();
        }
        if (!temporary.contains(permission)) {
            temporary.add(permission);
        }
    }

    public static boolean isTempBanned(String permission) {
        return temporary != null && temporary.contains(permission);
    }

    public static void untempban(String permission) {
        if (temporary != null) {
            if (temporary.contains(permission)) {
                temporary.remove(permission);
                if (temporary.isEmpty()) {
                    temporary = null;
                }
            }
        }
    }

    public static boolean isValid(String permission) {
        return isValid(null, permission);
    }

    private static boolean isMCMFileValid(Player p, File f) {
        if (f.getAbsolutePath().contains(MCManager.getInstance().getDataFolder().getAbsolutePath())) {
            return ModuleManager.isValid(p, "mcmanager");
        } else {
            return true;
        }
    }

    private static boolean isModuleEnabled(String module) {
        FileConfiguration f = MCManager.getConfiguration();
        List<String> disabled_modules = MCManager.getConfiguration().getStringList("disabled_modules");
        if (f.getBoolean("sandbox")) {
            disabled_modules.add("mcmanager");
            disabled_modules.add("file.run");
            disabled_modules.add("spigot_update");
            disabled_modules.add("webserver");
        }
        return !disabled_modules.contains(module);

    }

    public static boolean isFileEnabled(Player p, File file) {
        if (file.getAbsolutePath().startsWith(System.getProperty("user.dir")) && isMCMFileValid(p, file)) {
            FileConfiguration f = MCManager.getConfiguration();
            for (String st : MCManager.getConfiguration().getStringList("disabled_files")) {
                if (file.getAbsolutePath().startsWith(new File(st).getAbsolutePath()) || file.getAbsolutePath().equalsIgnoreCase(new File(st).getAbsolutePath())) {
                    return false;
                }
            }
            return true;
        } else {
            return false;
        }

    }

    public static void initializeData() {
        FileConfiguration f = MCManager.getConfiguration();
        if (!f.isList("disabled_modules")) {
            MCManager.getConfiguration().set("disabled_modules", new LinkedList<String>());
            MCManager.saveConfiguration();
        }
        if (!f.isBoolean("sandbox")) {
            MCManager.getConfiguration().set("sandbox", false);
            MCManager.saveConfiguration();
        }
    }
}
