/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

import com.betafase.mcmanager.api.MenuItem;

/**
 *
 * @author dbrosch
 */
public interface RawMenuItem {

    MenuItem getDisplayItem(String lang);
}
