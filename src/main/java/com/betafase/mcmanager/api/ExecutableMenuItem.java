/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author dbrosch
 */
public interface ExecutableMenuItem extends RawMenuItem {

    void onClick(InventoryClickEvent e);

}
