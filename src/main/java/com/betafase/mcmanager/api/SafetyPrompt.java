/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

import com.betafase.mcmanager.utils.Text;
import org.bukkit.Material;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author dbrosch
 */
public abstract class SafetyPrompt extends Menu {

    public SafetyPrompt(String title, String lang) {
        super(title, 9, lang);
        this.setItem(8, GUIUtils.backOld(lang));
        this.setItem(1, new MenuItem(Material.STAINED_CLAY, 1, (short) 5, "§a§l" + new Text("mcm.gui.safety_yes", lang).toString()));
        this.setItem(2, new MenuItem(Material.STAINED_CLAY, 1, (short) 14, "§c§l" + new Text("mcm.gui.safety_no", lang).toString()));
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        onApprove(e.getClick() == ClickType.LEFT && e.getSlot() == 1);
    }

    public abstract void onApprove(boolean approve);

}
