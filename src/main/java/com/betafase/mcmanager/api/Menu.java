/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.api;

import com.betafase.mcmanager.MCManager;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;

/**
 * © Betafase Developers, 2015
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public abstract class Menu implements InventoryHolder, Listener {

    private Inventory inv;
    public final String lang;
    int slots;
    private String title;
    private boolean autoCancel = true;

    public Menu(String title, int slots, String lang) {
        this.lang = lang;
        this.title = title;
        this.slots = slots;
    }

    /**
     * Toggles whether click events should automatically be cancelled (true by
     * default)
     *
     * @param autoCancel
     */
    public void setAutoCancel(boolean autoCancel) {
        this.autoCancel = autoCancel;
    }

    /**
     * Sets a Menu item.
     *
     * @param slot the middle slot (0-13) to set the item
     * @param item the item to set
     */
    public void setMenuItem(int slot, RawMenuItem item) {
        if (slot >= 0 && slot <= 13) {
            setItem(GUIUtils.convertCounterSlot(slot), item);
        } else {
            throw new IndexOutOfBoundsException("Slot must be between 0 and 13.");
        }
    }

    /**
     * Sets the size of the inventory.
     *
     * @param newSize the new size
     * @throws IllegalStateException Occurs when the menu has already been
     * created.
     */
    public final void setInventorySize(int newSize) throws IllegalStateException {
        if (inv != null) {
            throw new IllegalStateException("Can't set size of already created menu.");
        }
        slots = newSize;
    }

    /**
     * Returns whether click events will automatically be cancelled by the
     * plugin.
     *
     * @return true if enabled, false otherwise. default: true
     */
    public boolean isAutoCancel() {
        return autoCancel;
    }

    public abstract void onClick(InventoryClickEvent e);

    /**
     * Sets a MenuItem to a given slot of the inventory.
     *
     * @param slot the slot to set.
     * @param stack the item to set.
     */
    public void setItem(int slot, RawMenuItem stack) {
        getInventory().setItem(slot, stack.getDisplayItem(lang));
    }

    /**
     * The Language of the Menu, can be directly referred with 'lang'
     *
     * @return the language, e.g. en_US
     */
    public String getLanguage() {
        return lang;
    }

    @Override
    public Inventory getInventory() {
        if (inv == null) {
            inv = Bukkit.createInventory(this, slots, title);
        }
        return inv;
    }

    /**
     * Safely opens the Menu for a given player.
     *
     * @param p the player to open for.
     */
    public void open(final Player p) {
        Validate.notNull(p, "Player must not be null.");
        Bukkit.getScheduler().scheduleSyncDelayedTask(MCManager.getInstance(), () -> {
            p.openInventory(getInventory());
            onOpen(p);
        }, 2L);
    }

    public void onClose(InventoryCloseEvent e) {

    }

    public void onOpen(Player p) {

    }

}
