/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

import com.betafase.mcmanager.utils.spiget.ServerRequest;
import com.betafase.mcmanager.utils.spiget.SpigetPlugin;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.io.UnsupportedEncodingException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author dbrosch
 */
public class SpigotUpdateChecker implements UpdateChecker {
    
    private final int spigot_id;
    private final Plugin plugin;
    
    public SpigotUpdateChecker(Plugin plugin, int spigot_id) {
        this.spigot_id = spigot_id;
        this.plugin = plugin;
    }
    
    public int getSpigotID() {
        return spigot_id;
    }
    
    @Override
    public Plugin getPlugin() {
        return plugin;
    }
    
    @Override
    public UpdateInfo checkUpdate() {
        ServerRequest r = new ServerRequest("resources/" + spigot_id + "/versions?sort=-releaseDate");
        JsonElement e = r.getAsJsonElement();
        if (e.isJsonArray()) {
            JsonObject current = e.getAsJsonArray().get(0).getAsJsonObject();
            String version = current.get("name").getAsString();
            if (!version.equalsIgnoreCase(plugin.getDescription().getVersion()) && !version.substring(1).equalsIgnoreCase(plugin.getDescription().getVersion())) {
                ServerRequest r2 = new ServerRequest("resources/" + spigot_id + "/updates?size=1&sort=-date");
                JsonObject update = r2.getAsJsonElement().getAsJsonArray().get(0).getAsJsonObject();
                String title = update.get("title").getAsString();
                String description = "Update found via SpigetUpdater";
                try {
                    description = new String(Base64.getDecoder().decode(update.get("description").getAsString()), "UTF-8");
                } catch (UnsupportedEncodingException ex) {
                    Logger.getLogger(SpigotUpdateChecker.class.getName()).log(Level.SEVERE, null, ex);
                }
                description = description.replaceAll("(?s)<[^>]*>(\\s*<[^>]*>)*", " ");
                if (description.length() > 300) {
                    description = description.substring(0, 300) + "...";
                }
                return new UpdateInfo(plugin, title, version, description, current.has("url") ? current.get("url").getAsString() : null) {
                    @Override
                    public void performUpdate(Player notifier) {
                        SpigetPlugin d = new SpigetPlugin(spigot_id);
                        d.loadInformation();
                        if (d.hasDirectDownload()) {
                            notifier.sendMessage("§aCached Download from spiget.org found. Downloading directly...");
                            d.downloadDirectly(notifier, "plugins/" + getJarName());
                        } else if (d.isExternalDownload()) {
                            notifier.sendMessage("§c§lError §e§lThis Plugin has an external download. You must download it manually: " + d.getDownloadURL());
                        } else {
                            notifier.sendMessage("§aDownload started. Please wait...");
                            notifier.performCommand("wget plugins " + d.getDownloadURL());
                        }
                    }
                    
                };
            }
        } else {
            System.out.println("Could not check update for " + plugin.getName() + " : " + r.getAsString());
        }
        return null;
    }
    
}
