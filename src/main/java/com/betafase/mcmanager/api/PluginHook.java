/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

import java.util.Objects;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author dbrosch
 */
public class PluginHook {

    private final Plugin plugin;
    private UpdateChecker checker;
    private ExecutableMenuItem[] items = new ExecutableMenuItem[9];

    /**
     * Creates a new PluginHook instance. Please refer to
     * <code>MCManager.getPluginHook(Plugin pl)</code> instead.
     *
     * @param plugin
     */
    public PluginHook(Plugin plugin) {
        this.plugin = plugin;
    }

    /**
     * Gets the Plugin connected to this hook.
     *
     * @return the plugin
     */
    public Plugin getPlugin() {
        return plugin;
    }

    /**
     * Checks if this hook has an Updater registered.
     *
     * @return true if an UpdateChecker is found.
     */
    public boolean hasUpdateChecker() {
        return checker != null || items[5] != null;
    }

    /**
     * Gets the UpdateChecker of this PluginHook.
     *
     * @return the UpdateChecker or null if not existant.
     */
    public UpdateChecker getUpdateChecker() {
        return checker;
    }

    /**
     * Sets the UpdateChecker for this PluginHook.
     *
     * @param checker The new UpdateChecker
     * @see SpigotUpdateChecker for simple use.
     */
    public void setUpdateChecker(UpdateChecker checker) {
        this.checker = checker;
    }

    /**
     * Gets the PluginHooks registered by this instance.
     *
     * @return
     */
    public ExecutableMenuItem[] getCustomMenuItems() {
        return items;
    }

    /**
     * Creates a CustomMenu item. This allows you to override the default items.
     * <p>
     * Slot 1: Config Menu</p>
     * <p>
     * Slot 5: Updater (if available). If slot 5 is overridden, any
     * UpdateChecker registered for this Plugin is disabled.</p>
     * <p>
     * Slot 6: Plugin Folder</p>
     * <p>
     * Slot 7: Enable/Disable plugin</p>
     * <p>
     * Slot 8: Delete Plugin</p>
     *
     * @param slot the slot to set, must be between 0 und 8
     * @param item the item to set or null for default
     */
    public void setCustomMenuItem(int slot, ExecutableMenuItem item) {
        if (slot >= 0 && slot < 9) {
            items[slot] = item;
        } else {
            throw new IllegalArgumentException("Slot must be between 0 and 8");
        }
    }

    /**
     * Checks if a CustomItem exists at a given slot.
     *
     * @param slot the slot to check
     * @return true if there is a custom item, otherwise false.
     */
    public boolean isCustomItem(int slot) {
        if (slot >= 0 && slot < 9) {
            return items[slot] != null;
        } else {
            return false;
        }
    }

    /**
     * Returns the CustomItem at a given slot.
     *
     * @param slot The Slot to check
     * @return The CustomItem or null if not found.
     */
    public ExecutableMenuItem getCustomItem(int slot) {
        if (slot >= 0 && slot < 9) {
            return items[slot];
        } else {
            return null;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Plugin) {
            return obj.equals(getPlugin());
        } else if (obj instanceof PluginHook) {
            return super.equals(obj);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 11 * hash + Objects.hashCode(this.plugin);
        return hash;
    }

}
