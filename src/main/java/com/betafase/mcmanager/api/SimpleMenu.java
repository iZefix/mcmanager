/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.api;

import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author dbrosch
 */
public class SimpleMenu extends Menu {
    
    private final ExecutableMenuItem[] items;
    
    public SimpleMenu(String title, int slots, String lang) {
        super(title, slots, lang);
        items = new ExecutableMenuItem[slots];
    }
    
    @Override
    public final void setItem(int slot, RawMenuItem stack) {
        super.setItem(slot, stack);
        if (stack instanceof ExecutableMenuItem) {
            items[slot] = (ExecutableMenuItem) stack;
        }
    }
    
    @Override
    public final void onClick(InventoryClickEvent e) {
        int slot = e.getSlot();
        if (items[slot] != null) {
            items[slot].onClick(e);
        }
    }
    
}
