/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.utils.Text;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author dbrosch
 */
public abstract class NotificationPrompt extends Menu {

    public NotificationPrompt(String title, Type type, String message, String lang) {
        super(title, 9, lang);
        this.setItem(0, new MenuItem(Material.STAINED_CLAY, BlockColor.LIME, new Text("mcm.notification.continue", lang).toString()));
        this.setItem(8, GUIUtils.backOld(lang));
        String t;
        BlockColor c;
        switch (type) {
            case SUCCESS:
                t = new Text("mcm.notification.success", lang).toString();
                c = BlockColor.LIME;
                break;
            case ERROR:
                t = new Text("mcm.notification.error", lang).toString();
                c = BlockColor.RED;
                break;
            case WARNING:
                t = new Text("mcm.notification.warning", lang).toString();
                c = BlockColor.YELLOW;
                break;
            default:
                t = "§8§lNotification";
                c = BlockColor.GRAY;
                break;
        }
        setItem(4, new MenuItem(Material.STAINED_GLASS, 1, c.toShort(), t, Text.wordWrapAsArray(message, 40, ChatColor.GRAY)));
        for (int i = 1; i < 8; i++) {
            if (getInventory().getItem(i) == null || getInventory().getItem(i).getType() == Material.AIR) {
                setItem(i, new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.GRAY, " "));
            }
        }
        new BukkitRunnable() {

            int counter = 7;

            @Override
            public void run() {
                if (counter > 0) {
                    if (getInventory().getItem(counter).getType() == Material.STAINED_GLASS_PANE) {
                        setItem(counter, new MenuItem(Material.STAINED_GLASS_PANE, c, " "));
                    }
                    counter--;
                } else {
                    if (!getInventory().getViewers().isEmpty()) {
                        getInventory().getViewers().stream().forEach((h) -> {
                            onContinue((Player) h, lang);
                        });
                    }
                    this.cancel();
                }
            }
        }.runTaskTimer(MCManager.getInstance(), 20L, 20L);
    }

    public abstract void onBack(Player p, String lang);

    public abstract void onContinue(Player p, String lang);

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        switch (e.getSlot()) {
            case 0:
                e.getView().close();
                onContinue(p, lang);
                break;
            case 8:
                e.getView().close();
                onBack(p, lang);
                break;
        }
    }

    public enum Type {
        SUCCESS, ERROR, WARNING
    }

}
