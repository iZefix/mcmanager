/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author dbrosch
 */
public abstract class MaterialSelector extends Menu {

    Material[] values;
    Material selected = null;
    int maxselected;
    int page;

    public MaterialSelector(String title, String lang, Material[] values) {
        this(title, lang, 0, values, 1);
        // 48 prev, 49 back, 50 next
    }

    public MaterialSelector(String title, String lang, Material[] values, int maxselected) {
        this(title, lang, 0, values, maxselected);
    }

    private MaterialSelector(String title, String lang, int page, Material[] values, int maxselected) {
        super(title, 54, lang);
        this.values = values;
        this.maxselected = maxselected;
        this.page = page;
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        if (e.getSlot() < 43) {
            
        }
    }

    /**
     * This Method is called once the Selection is complete.
     *
     * @param selected The selected Material or null if nothing is selected.
     */
    public abstract void onSelectionComplete(Material selected);

}
