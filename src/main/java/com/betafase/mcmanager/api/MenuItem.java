/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.api;

import com.betafase.mcmanager.api.BlockColor;
import com.betafase.mcmanager.utils.Text;
import java.util.Arrays;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class MenuItem extends ItemStack implements RawMenuItem {

    public MenuItem(ItemStack stack) throws IllegalArgumentException {
        super(stack);
    }

    public MenuItem(Material m, int count) {
        super(m, count);
    }

    public MenuItem(Material type) {
        super(type);
    }

    public MenuItem(Material type, String displayname, String... lore) {
        super(type);
        ItemMeta meta = this.getItemMeta();
        meta.setDisplayName(displayname);
        meta.setLore(Arrays.asList(lore));
        this.setItemMeta(meta);
    }

    public MenuItem(Material type, String displayname) {
        super(type);
        ItemMeta meta = this.getItemMeta();
        meta.setDisplayName(displayname);
        this.setItemMeta(meta);
    }

    public MenuItem(Material type, Text displayname) {
        super(type);
        ItemMeta meta = this.getItemMeta();
        meta.setDisplayName(displayname.toString());
        this.setItemMeta(meta);
    }

    public MenuItem(Material type, int amount, short damage, String displayname, String... lore) {
        super(type, amount, damage);
        ItemMeta meta = this.getItemMeta();
        meta.setDisplayName(displayname);
        meta.setLore(Arrays.asList(lore));
        this.setItemMeta(meta);
    }

    public MenuItem(Material type, int amount, short damage, String displayname) {
        super(type, amount, damage);
        ItemMeta meta = this.getItemMeta();
        meta.setDisplayName(displayname);
        this.setItemMeta(meta);
    }
    
    public MenuItem(Material type, BlockColor color, String displayname){
        this(type, 1, color.toShort(), displayname);
    }
    
    public void addGlow() {
        ItemMeta meta = this.getItemMeta();
        this.addEnchantment(Enchantment.LUCK, 1);
        meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        this.setItemMeta(meta);
    }

    @Override
    public MenuItem getDisplayItem(String lang) {
        return this;
    }

}
