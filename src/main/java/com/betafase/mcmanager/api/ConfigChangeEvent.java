/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author dbrosch
 */
public class ConfigChangeEvent extends Event {

    private static final HandlerList handlers = new HandlerList();

    Plugin plugin;
    String key;
    Object value;

    public ConfigChangeEvent(Plugin plugin, String key, Object value) {
        this.plugin = plugin;
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public Object getValue() {
        return value;
    }

    public Plugin getPlugin() {
        return plugin;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

}
