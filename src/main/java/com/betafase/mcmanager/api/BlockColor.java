/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

import org.apache.commons.lang.StringUtils;

/**
 *
 * @author dbrosch
 */
public enum BlockColor {

    WHITE((short) 0),
    ORANGE((short) 1),
    MAGENTA(((short) 2)),
    LIGHT_BLUE(((short) 3)),
    YELLOW((short) 4),
    LIME((short) 5),
    PINK((short) 6),
    GRAY(((short) 7)),
    SILVER(((short) 8)),
    CYAN((short) 9),
    PURPLE((short) 10),
    BLUE((short) 11),
    BROWN((short) 12),
    GREEN((short) 13),
    RED((short) 14),
    BLACK((short) 15);

    private short id;

    BlockColor(short id) {
        this.id = id;
    }

    public short toShort() {
        return id;
    }

    public String toStateColor() {
        return name().toLowerCase();
    }

    @Override
    public String toString() {
        return StringUtils.capitalize(name().toLowerCase());
    }

}
