/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.command.WgetCommand;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author dbrosch
 */
public interface UpdateChecker {

    UpdateInfo checkUpdate();

    Plugin getPlugin();

    class UpdateInfo {

        private Plugin plugin;
        private String title;
        private String version;
        private String description;
        private String download_url;
        private String jar_name;

        public UpdateInfo(Plugin plugin, String title, String version, String description, String download_url) {
            this.title = title;
            this.version = version;
            this.description = description;
            this.download_url = download_url;
            try {
                this.jar_name = new File(plugin.getClass().getProtectionDomain().getCodeSource().getLocation().toURI()).getName();
            } catch (Exception ex) {
                Logger.getLogger(MCManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        public void setJarName(String jar_name) {
            this.jar_name = jar_name;
        }

        public String getJarName() {
            return jar_name;
        }

        public Plugin getPlugin() {
            return plugin;
        }

        public String getTitle() {
            return title;
        }

        public String getVersion() {
            return version;
        }

        public String getDescription() {
            return description;
        }

        public void performUpdate(Player notifier) {
            if (jar_name != null && download_url != null) {
                WgetCommand.downloadFile(download_url, jar_name, notifier);
            }
        }

    }

}
