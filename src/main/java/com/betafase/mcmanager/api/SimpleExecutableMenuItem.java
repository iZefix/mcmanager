/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

/**
 *
 * @author dbrosch
 */
public abstract class SimpleExecutableMenuItem implements ExecutableMenuItem {

    private MenuItem display;

    public SimpleExecutableMenuItem(MenuItem display) {
        this.display = display;
    }

    @Override
    public MenuItem getDisplayItem(String lang) {
        return display;
    }

}
