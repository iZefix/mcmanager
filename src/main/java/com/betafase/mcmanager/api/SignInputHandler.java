/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

/**
 *
 * @author dbrosch
 */
public interface SignInputHandler {

    void handleTextInput(String[] lines);

    String[] getDefault();

}
