/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.api;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.command.WgetCommand;
import com.betafase.mcmanager.gui.FileMenu;
import com.betafase.mcmanager.gui.plugin.PluginMenu;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import java.io.File;
import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author dbrosch
 */
public class MCManagerAPI {

    /**
     * Gets the modifiable PluginHook of a certain plugin.
     *
     * @param pl The Plugin to get the hook for
     * @return the PluginHook, never null
     */
    public static PluginHook getPluginHook(Plugin pl) {
        return MCManager.getPluginHook(pl);
    }

    /**
     * Registers a new custom UpdateChecker. Convenience method, can also be
     * accessed via plugin hook.
     *
     * @param checker The Checker to use
     */
    public static void registerUpdater(UpdateChecker checker) {
        getPluginHook(checker.getPlugin()).setUpdateChecker(checker);
    }

    /**
     * Registers a new SpigotUpdateChecker for a certain plugin. Convenience
     * method, can also be accessed via plugin hook.
     *
     * @param plugin The Plugin to register the checker for
     * @param id The Spigot.org ID of the plugin
     */
    public static void registerSpigotUpdater(Plugin plugin, int id) {
        registerUpdater(new SpigotUpdateChecker(plugin, id));
    }

    /**
     * Requests a SignInput of a certain player. Please note that the default
     * text might not always work.
     *
     * @param p The Player to request the input from
     * @param handler The Handler for processing the input
     * @return true if the SignGUI can be shown (ProtocolLib installed)
     * otherwise false.
     */
    public static boolean requestSignTextInput(Player p, SignInputHandler handler) {
        return MCManager.requestInput(p, handler);
    }

    /**
     * Downloads a File from the Web.
     *
     * @param url The direct URL to download the file from
     * @param save The directory to save in or the savepath of the file
     * @param notifier The Player to notify
     */
    public static void downloadFile(String url, String save, Player notifier) {
        if (ModuleManager.isValid(notifier, "wget")) {
            WgetCommand.downloadFile(url, save, notifier);
        } else {
            throw new SecurityException("Access denied.");
        }
    }

    /**
     * Opens the FileMenu for a given file or directory.
     *
     * @param p The Player to open the Menu for
     * @param f The File to open
     */
    public static void openFileMenu(Player p, File f) {
        if (ModuleManager.isValid(p, "file")) {
            new FileMenu(f, Text.getLanguage(p), p.getUniqueId(), 0).open(p);
        } else {
            throw new SecurityException("Access denied.");
        }
    }

    /**
     * Opens the PluginMenu for a given plugin.
     *
     * @param p The Player to open the Menu for
     * @param pl The Plugin to open
     */
    public static void openPluginMenu(Player p, Plugin pl) {
        if (ModuleManager.isValid(p, "plugin")) {
            new PluginMenu(pl, Text.getLanguage(p)).open(p);
        } else {
            throw new SecurityException("Access denied.");
        }
    }

    public static void setMainMenuItem(int slot, ExecutableMenuItem item) {
        Validate.isTrue(slot < 14 && slot >= 0, "Slot must be between 0 and 15", slot);
        MCManager.getMenuDesign().put(slot, item);
    }

    public static boolean recommendAddMainMenuItem(int pref_slot, ExecutableMenuItem item) {
        if (!MCManager.getMenuDesign().containsKey(pref_slot)) {
            setMainMenuItem(pref_slot, item);
            return true;
        } else {
            return addMenuItem(item);
        }
    }

    public static boolean addMenuItem(ExecutableMenuItem item) {
        int slot = getNextMainMenuSlot();
        if (slot < 0) {
            return false;
        } else {
            MCManager.getMenuDesign().put(slot, item);
            return true;
        }
    }

    public static int getNextMainMenuSlot() {
        int[] array = new int[16];
        for (Integer in : MCManager.getMenuDesign().keySet()) {
            array[in] = 1;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] == 0) {
                return i;
            }
        }
        return -1;
    }

}
