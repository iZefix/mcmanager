/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by the Betafase Developers (betafase.com) and currently developed by iZefix.
 */
package com.betafase.mcmanager.api;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.utils.Text;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 * © Betafase Developers, 2015-2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class GUIUtils {

    public static void openAsync(Inventory inventory, HumanEntity player) {
        final Inventory inv = inventory;
        final HumanEntity p = player;
        Runnable r = () -> {
            p.openInventory(inv);
        };
        Bukkit.getScheduler().scheduleSyncDelayedTask(MCManager.getInstance(), r, 2L);
    }

    /*
    ======================================================
        Old Menu Design Section
    ======================================================
     */
    public static MenuItem loadingOld(String lang) {
        return new MenuItem(Material.POTATO_ITEM, "§7Loading...");
    }

    public static MenuItem no_access(String lang) {
        return new MenuItem(Material.STAINED_GLASS_PANE, 0, (short) 14, new Text("mcm.gui.security", lang).toString());
    }

    public static MenuItem backOld(String lang) {
        return new MenuItem(Material.ARROW, "§c" + new Text("mcm.gui.back", lang));
    }

    public static MenuItem next_pageOld(String lang, int page) {
        MenuItem stack = new MenuItem(Material.STICK, page);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName("§a" + new Text("mcm.gui.next_page", lang));
        stack.setItemMeta(meta);
        return stack;
    }

    public static MenuItem previous_pageOld(String lang, int page) {
        MenuItem stack = new MenuItem(Material.STICK, page);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName("§a" + new Text("mcm.gui.previous_page", lang));
        stack.setItemMeta(meta);
        return stack;
    }

    /*
    ======================================================
        New Menu Design Section
    ======================================================
     */
    public static MenuItem back(String lang) {
        return new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.RED, "§c" + new Text("mcm.gui.back", lang));
    }

    public static MenuItem previous_page(String lang, int page) {
        MenuItem stack = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.WHITE, ChatColor.WHITE.toString() + new Text("mcm.gui.previous_page", lang));
        stack.setAmount(page);
        return stack;
    }

    public static MenuItem next_page(String lang, int page) {
        MenuItem stack = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.WHITE, ChatColor.WHITE.toString() + new Text("mcm.gui.next_page", lang));
        stack.setAmount(page);
        return stack;
    }

    public static MenuItem black() {
        return new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 15, " ");
    }

    public static MenuItem loading(String lang) {
        return new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.YELLOW, new Text("mcm.gui.loading", lang).toString());
    }

    /*
    ======================================================
        Display Section
    ======================================================
     */
    public static MenuItem displayInteger(int value, String lang) {
        //9
        return new MenuItem(Material.INK_SACK, (value > 0 && value <= 64 ? value : 1), (short) 9, "§e" + value);
    }

    public static MenuItem displayString(String value, String lang) {
        //5
        return new MenuItem(Material.INK_SACK, 1, (short) 5, "§e" + value);
    }

    public static MenuItem displayBoolean(boolean value, String lang) {
        if (value) {
            return enabled(lang);
        } else {
            return disabled(lang);
        }
    }

    public static MenuItem disabled(String lang) {
        return new MenuItem(Material.INK_SACK, 1, (short) 8, "§7" + new Text("mcm.gui.disabled", lang));
    }

    public static MenuItem enabled(String lang) {
        return new MenuItem(Material.INK_SACK, 1, (short) 10, "§a" + new Text("mcm.gui.enabled", lang));
    }

    /*
    ======================================================
        Player Toggle Status Display Section
    ======================================================
     */
    public static MenuItem all(String lang, boolean selected) {
        MenuItem stack = new MenuItem(Material.GLOWSTONE_DUST);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName("§6" + new Text("mcm.gui.all", lang));
        if (selected) {
            meta.addEnchant(Enchantment.LUCK, 1, selected);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        stack.setItemMeta(meta);
        return stack;
    }

    public static ItemStack friends(String lang, boolean selected) {
        ItemStack stack = new ItemStack(Material.REDSTONE);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName("§4" + new Text("mcm.gui.friends", lang));
        if (selected) {
            meta.addEnchant(Enchantment.LUCK, 1, selected);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        stack.setItemMeta(meta);
        return stack;
    }

    public static ItemStack none(String lang, boolean selected) {
        ItemStack stack = new ItemStack(Material.SULPHUR);
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName("§7" + new Text("mcm.gui.none", lang));
        if (selected) {
            meta.addEnchant(Enchantment.LUCK, 1, selected);
            meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        }
        stack.setItemMeta(meta);
        return stack;
    }

    /*
    ======================================================
        Menu Design Implementation
    ======================================================
     */
    public static int convertCounterSlot(int counter) {
        if (counter >= 7) {
            counter += 2;
        }
        counter += 10;
        return counter;
    }

    public static int convertInventorySlot(int inv) {
        if (inv >= 9 && inv < 27 && inv % 9 < 8 && inv % 9 > 0) {
            inv -= 10;
            if (inv >= 9) {
                inv -= 2;
            }
            return inv;
        }
        return -1;
    }

    /*
    ======================================================
        Event Connector Section
    ======================================================
     */
    public static void showNoAccess(InventoryClickEvent e, String lang) {
        final ItemStack item = e.getCurrentItem();
        e.setCurrentItem(GUIUtils.no_access(lang));
        Bukkit.getScheduler().runTaskLater(MCManager.getInstance(), () -> {
            e.setCurrentItem(item);
        }, 20L);
    }
}
