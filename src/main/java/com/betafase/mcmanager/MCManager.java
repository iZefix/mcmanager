/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager;

import com.betafase.mcmanager.api.ExecutableMenuItem;
import com.betafase.mcmanager.api.MCManagerAPI;
import com.betafase.mcmanager.api.PluginHook;
import com.betafase.mcmanager.api.SpigotUpdateChecker;
import com.betafase.mcmanager.command.ConfigCommand;
import com.betafase.mcmanager.command.MainCommand;
import com.betafase.mcmanager.command.MkdirCommand;
import com.betafase.mcmanager.command.MoveCommand;
import com.betafase.mcmanager.command.SearchPluginCommand;
import com.betafase.mcmanager.listener.WorldEventHandler;
import com.betafase.mcmanager.command.WgetCommand;
import com.betafase.mcmanager.command.WorldCreateCommand;
import com.betafase.mcmanager.gui.backup.BackupsMenu;
import com.betafase.mcmanager.gui.FileMenu;
import com.betafase.mcmanager.gui.GUIManager;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.gui.player.MainPlayerMenu;
import com.betafase.mcmanager.gui.plugin.MainPluginMenu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.utils.TextInputHandleWrapper;
import com.betafase.mcmanager.gui.world.WorldsMenu;
import com.betafase.mcmanager.listener.BookHandler;
import com.betafase.mcmanager.listener.DamageHandler;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.BackupManager;
import com.betafase.mcmanager.utils.Text;
import com.betafase.mcmanager.web.WebManager;
import com.betafase.mcmanager.web.WebServer;
import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang.RandomStringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.PluginCommand;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.ChatPaginator;
import com.betafase.mcmanager.api.SignInputHandler;
import com.betafase.mcmanager.command.QuickCommandHandler;
import java.io.Reader;
import java.util.Arrays;

/**
 * © Betafase Developers, 2016
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class MCManager extends JavaPlugin {

    private static MCManager instance;
    private GUIManager manager;
    private TextInputHandleWrapper inputHandler;
    private WebServer webserver;
    private BackupManager backup;
    private final String prefix = "§8MCManager > ";
    private HashMap<String, PluginHook> plugin_hooks;
    private List<String> req_updates = new LinkedList<>();

    public static MCManager getInstance() {
        return instance;
    }

    private HashMap<Integer, ExecutableMenuItem> menu;

    public static String getInstanceName() {
        return instance.getName();
    }

    public GUIManager getGUIManager() {
        return manager;
    }

    public static Logger getLog() {
        return instance.getLogger();
    }

    public static String getPrefix() {
        return instance.prefix;
    }

    public static List<String> getUpdates() {
        return instance.req_updates;
    }

    public static HashMap<Integer, ExecutableMenuItem> getMenuDesign() {
        return instance.menu;
    }

    @Override
    public void onEnable() {
        instance = this;
        menu = new LinkedHashMap();
        if (Bukkit.getPluginManager().isPluginEnabled("ProtocolLib")) {
            getLogger().log(Level.INFO, "Injecting into ProtocolLib...");
            inputHandler = new TextInputHandleWrapper(this);
        } else {
            getLogger().log(Level.WARNING, "Dependency ProtocolLib not found. Please install it to use all functions.");
            inputHandler = null;
        }
        manager = new GUIManager(this);
        ModuleManager.initializeData();
        Bukkit.getPluginCommand("manager").setExecutor(new MainCommand());
        Bukkit.getPluginCommand("manager").setAliases(Arrays.asList("mcm", "m"));
        Bukkit.getPluginCommand("wget").setExecutor(new WgetCommand());
        Bukkit.getPluginCommand("mkdir").setExecutor(new MkdirCommand());
        Bukkit.getPluginCommand("config").setExecutor(new ConfigCommand());
        Bukkit.getPluginCommand("move").setExecutor(new MoveCommand());
        Bukkit.getPluginCommand("searchplugin").setExecutor(new SearchPluginCommand());

        QuickCommandHandler handler = new QuickCommandHandler();
        Bukkit.getPluginCommand("players").setExecutor(handler);
        Bukkit.getPluginCommand("files").setExecutor(handler);
        Bukkit.getPluginCommand("log").setExecutor(handler);
        Bukkit.getPluginCommand("backup").setExecutor(handler);

        PluginCommand cmd = Bukkit.getPluginCommand("createworld");
        WorldCreateCommand wcc = new WorldCreateCommand();
        cmd.setExecutor(wcc);
        cmd.setTabCompleter(wcc);

        Bukkit.getPluginManager().registerEvents(new DamageHandler(), this);
        Bukkit.getPluginManager().registerEvents(new BookHandler(), this);
        Bukkit.getPluginManager().registerEvents(new WorldEventHandler(), this);
        getLogger().log(Level.INFO, "Everything is working fine. Enabling API now.");

        plugin_hooks = new HashMap<>();

        getPluginHook(this).setUpdateChecker(new SpigotUpdateChecker(this, 7297));

        File f = new File(getDataFolder(), "config.yml");
        if (!f.exists()) {
            for (Player a : Bukkit.getOnlinePlayers()) {
                if (a.isOp()) {
                    a.sendMessage("§9§lThank you for downloading MCManager!");
                    String text = "If you enjoy the plugin, please consider donating via PayPal as it is offered for free and/or write a review on Spigot. If there is a bug or any other error, please create an issue on Bitbucket. If you need help, please refer to the wiki  (https://bitbucket.org/iZefix/mcmanager/wiki/Home) or create an issue. You can view this message again by clicking on report an error in the main GUI panel. Thanks for reading and now go ahead with /manager. ";
                    for (String s : ChatPaginator.wordWrap(text, 55)) {
                        a.sendMessage("§7" + ChatColor.stripColor(s));
                    }
                    TextComponent donate = new TextComponent("§6§nDonate via PayPal");
                    donate.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=N4YLZ42CGRP3E"));
                    donate.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=N4YLZ42CGRP3E").create()));
                    a.spigot().sendMessage(donate);
                    TextComponent issue = new TextComponent("§c§nReport an issue on Bitbucket");
                    issue.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://bitbucket.org/iZefix/mcmanager/issues/new"));
                    issue.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7https://bitbucket.org/iZefix/mcmanager/issues/new").create()));
                    a.spigot().sendMessage(issue);
                    TextComponent spigot = new TextComponent("§e§nVisit Spigot Page");
                    spigot.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/resources/mcmanager-ingame-server-management.7297/"));
                    spigot.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7https://www.spigotmc.org/resources/mcmanager-ingame-server-management.7297/").create()));
                    a.spigot().sendMessage(spigot);
                }
            }
            saveDefaultConfig();
        }
        getConfig().options().copyDefaults(true);

        reloadLanguage();

        MCManagerAPI.addMenuItem(new ExecutableMenuItem() {
            @Override
            public void onClick(InventoryClickEvent e) {
                Player p = (Player) e.getWhoClicked();
                String lang = Text.getLanguage(p);
                if (ModuleManager.isValid(p, "player")) {
                    new MainPlayerMenu(lang, 0, false).open(p);
                } else {
                    GUIUtils.showNoAccess(e, lang);
                }
            }

            @Override
            public MenuItem getDisplayItem(String lang) {
                return new MenuItem(Material.SKULL_ITEM, 1, (short) 3, new Text("mcm.gui.main.players", lang).toString());
            }
        });
        MCManagerAPI.addMenuItem(new ExecutableMenuItem() {
            @Override
            public void onClick(InventoryClickEvent e) {
                Player p = (Player) e.getWhoClicked();
                String lang = Text.getLanguage(p);
                if (ModuleManager.isValid(p, "plugin")) {
                    new MainPluginMenu(lang, 0).open(p);
                } else {
                    GUIUtils.showNoAccess(e, lang);
                }
            }

            @Override
            public MenuItem getDisplayItem(String lang) {
                return new MenuItem(Material.SLIME_BALL, new Text("mcm.gui.main.plugins", lang).toString());
            }
        });
        MCManagerAPI.addMenuItem(new ExecutableMenuItem() {
            @Override
            public void onClick(InventoryClickEvent e) {
                Player p = (Player) e.getWhoClicked();
                String lang = Text.getLanguage(p);
                if (ModuleManager.isValid(p, "file")) {
                    new FileMenu(new File(System.getProperty("user.dir")), lang, p.getUniqueId(), 0).open(p);
                } else {
                    GUIUtils.showNoAccess(e, lang);
                }
            }

            @Override
            public MenuItem getDisplayItem(String lang) {
                return new MenuItem(Material.PAPER, new Text("mcm.gui.main.files", lang).toString());
            }
        });
        MCManagerAPI.addMenuItem(new ExecutableMenuItem() {
            @Override
            public void onClick(InventoryClickEvent e) {
                Player p = (Player) e.getWhoClicked();
                String lang = Text.getLanguage(p);
                if (ModuleManager.isValid(p, "world")) {
                    new WorldsMenu(lang, 0).open(p);
                } else {
                    GUIUtils.showNoAccess(e, lang);
                }
            }

            @Override
            public MenuItem getDisplayItem(String lang) {
                return new MenuItem(Material.GRASS, new Text("mcm.gui.main.world", lang));
            }
        });
        MCManagerAPI.addMenuItem(new ExecutableMenuItem() {
            @Override
            public void onClick(InventoryClickEvent e) {
                Player p = (Player) e.getWhoClicked();
                String lang = Text.getLanguage(p);
                if (ModuleManager.isValid(p, "backup")) {
                    new BackupsMenu(lang).open(p);
                } else {
                    GUIUtils.showNoAccess(e, lang);
                }
            }

            @Override
            public MenuItem getDisplayItem(String lang) {
                return new MenuItem(Material.RECORD_3, new Text("mcm.gui.main.backup", lang).toString());
            }
        });

        if (ModuleManager.isValid("backup")) {
            backup = new BackupManager();
            if (getConfiguration().getBoolean("backup.enabled")) {
                MCManager.getLog().log(Level.INFO, "Starting automated backup...");
                backup.start();
            }
        }
        int port = getConfiguration().getInt("webserver.port");
        if (port == 0) {
            getConfig().set("webserver.port", 8282);
            port = 8282;
        }
        if (!getConfiguration().contains("webserver.accounts")) {
            ConfigurationSection section = getConfig().createSection("webserver.accounts");
            section.set("user", RandomStringUtils.randomAlphanumeric(7));
        }
        if (!getConfiguration().isString("ban_command")) {
            getConfig().set("ban_command", "ban %player% %arg0%%arg1%%arg2%%arg3%");
        }
        if (!getConfiguration().isBoolean("webserver.autostart")) {
            getConfig().set("webserver.autostart", true);
        }
        if (!getConfig().contains("disabled_modules")) {
            getConfig().set("disabled_modules", new LinkedList<>());
        }
        saveConfig();

        webserver = new WebServer();
        if (ModuleManager.isValid("webserver")) {
            if (getConfiguration().getBoolean("webserver.autostart")) {
                getLog().log(Level.INFO, "Starting WebServer on port {0} ...", port);
                webserver.start(port);
            } else {
                getLog().log(Level.INFO, "Autostart is disabled. You have to start the WebServer manually to access Web features.");
            }
            MCManagerAPI.recommendAddMainMenuItem(12, new WebManager());
            //getPluginHook(this).setCustomMenuItem(1, new WebManager());
        }
        if (ModuleManager.isValid("plugin_updater")) {
            getLog().log(Level.INFO, "Checking for Plugin Updates...");
            Bukkit.getScheduler().runTaskLaterAsynchronously(MCManager.getInstance(), () -> {
                for (Plugin pl : Bukkit.getPluginManager().getPlugins()) {
                    if (!getPluginHook(pl).hasUpdateChecker()) {
                        try {
                            String executed_file = new File(pl.getClass().getProtectionDomain().getCodeSource().getLocation().toURI()).getName();
                            if (executed_file.startsWith("SpigotPlugin_")) {
                                executed_file = executed_file.substring("SpigotPlugin_".length(), executed_file.lastIndexOf("."));
                                getLog().log(Level.INFO, "Adding automated Updater for SpigotPlugin {0} ({1})", new Object[]{executed_file, pl.getName()});
                                SpigotUpdateChecker checker = new SpigotUpdateChecker(pl, Integer.parseInt(executed_file));
                                getPluginHook(pl).setUpdateChecker(checker);
                            } else if (pl.getName().equalsIgnoreCase("LuckPerms")) {
                                getPluginHook(pl).setUpdateChecker(new SpigotUpdateChecker(pl, 28140));
                                getLog().log(Level.INFO, "Initialized Updater for  {0} ({1})", new Object[]{executed_file, pl.getName()});
                            } else if (pl.getName().equalsIgnoreCase("crazyauctions")) {
                                getPluginHook(pl).setUpdateChecker(new SpigotUpdateChecker(pl, 25219));
                                getLog().log(Level.INFO, "Initialized Updater for  {0} ({1})", new Object[]{executed_file, pl.getName()});
                            } else if (pl.getName().equalsIgnoreCase("viaversion")) {
                                getPluginHook(pl).setUpdateChecker(new SpigotUpdateChecker(pl, 19254));
                                getLog().log(Level.INFO, "Initialized Updater for  {0} ({1})", new Object[]{executed_file, pl.getName()});
                            }
                        } catch (Exception ex) {
                            Logger.getLogger(MCManager.class.getName()).log(Level.WARNING, "Failed to register auto-generated SpigotUpdater for Plugin " + pl.getName(), ex);
                        }
                    }
                }

                for (PluginHook h : getPluginHooks()) {
                    if (h.hasUpdateChecker()) {
                        if (h.getUpdateChecker().checkUpdate() != null) {
                            getLog().log(Level.INFO, "A Update for {0} has been found!", h.getPlugin().getName());
                            req_updates.add(h.getPlugin().getName());
                        } else {
                            //getLog().log(Level.INFO, "{0} is up to date!", h.getPlugin().getName());
                        }
                    }
                }
            }, 20L);
        }
    }

    public static WebServer getWebServer() {
        return getInstance().webserver;
    }

    public static Collection<PluginHook> getPluginHooks() {
        return getInstance().plugin_hooks.values();
    }

    public static PluginHook getPluginHook(Plugin pl) {
        if (getInstance().plugin_hooks.containsKey(pl.getName())) {
            return getInstance().plugin_hooks.get(pl.getName());
        } else {
            PluginHook hook = new PluginHook(pl);
            getInstance().plugin_hooks.put(pl.getName(), hook);
            return hook;
        }
    }

    public static FileConfiguration getConfiguration() {
        return getInstance().getConfig();
    }

    public static void saveConfiguration() {
        getInstance().saveConfig();
    }

    @Override
    public void onDisable() {
        if (inputHandler != null) {
            inputHandler.disable();
        }
        if (webserver != null) {
            webserver.stop();
        }
        if (backup != null) {
            backup.cancel();
        }
    }

    public static boolean requestInput(Player p, SignInputHandler handler) {
        if (instance == null || instance.inputHandler == null) {
            return false;
        }
        return instance.inputHandler.requestInput(p, handler);
    }

    public void reloadLanguage() {
        if (!Text.getTranslations().isEmpty()) {
            Text.getTranslations().clear();
        }
        String lang = getConfig().getString("language");
        if (lang == null || lang.equalsIgnoreCase("default")) {
            getConfig().set("language", "en_US");
            saveConfig();
            lang = "en_US";
        }
        Text.addLanguage(YamlConfiguration.loadConfiguration(this.getTextResource("texts/en_US.yml")));
        Text.setDefaultLanguage("en_US");
        try {
            if (lang.equalsIgnoreCase("en_US")) {
                this.getLogger().log(Level.INFO, "Loaded en_US as the default language.");
                Text.setPerUserLanguage(false);
            } else if (lang.equalsIgnoreCase("de_DE")) {
                Text.addLanguage(YamlConfiguration.loadConfiguration(this.getTextResource("texts/de_DE.yml")));
                Text.setPerUserLanguage(true);
                Text.setPlayerLanguageExecutor((Player p) -> "de_DE");
                this.getLogger().log(Level.INFO, "Loaded de_DE.");
            } else if (lang.equalsIgnoreCase("pt_BR")) {
                Text.addLanguage(YamlConfiguration.loadConfiguration(this.getTextResource("texts/pt_BR.yml")));
                Text.setPerUserLanguage(true);
                Text.setPlayerLanguageExecutor((Player p) -> "pt_BR");
                this.getLogger().log(Level.INFO, "Loaded pt_BR.");
            } else {
                this.getLogger().log(Level.WARNING, "The Language you were trying to access does not exist.");
                File f = new File(lang);
                if (!f.exists()) {
                    this.getLogger().log(Level.SEVERE, "The Language File from the config does not exist. Falling back to default language.");
                } else {
                    Text.addLanguage(YamlConfiguration.loadConfiguration(f));
                    String l = Text.getTranslations().keySet().iterator().next();
                    Text.setPerUserLanguage(true);
                    Text.setPlayerLanguageExecutor((Player p) -> l);
                    this.getLogger().log(Level.INFO, "Loaded language {0} instead of default language.", l);

                }
            }
        } catch (Exception ex) {
            this.getLogger().log(Level.SEVERE, "The Language File from the config could not be loaded. Falling back to default language.");
        }
    }

    public static BackupManager getBackupManager() {
        return instance.backup;
    }

    public Reader getTheDamnTextResource(String name) {
        return super.getTextResource(name);
    }

}
