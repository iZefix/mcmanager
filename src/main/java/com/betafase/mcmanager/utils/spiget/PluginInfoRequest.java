/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.utils.spiget;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.ArrayList;
import java.util.Map;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class PluginInfoRequest extends ServerRequest {

    public PluginInfoRequest(String path, Map<String, String> params) {
        super(path, params);
    }

    public PluginInfoRequest(String path) {
        super(path);
    }

    public SpigetPlugin[] getPlugins() {
        try {
            JsonArray a = getAsJsonElement().getAsJsonArray();
            ArrayList<SpigetPlugin> list = new ArrayList<>();
            for (JsonElement e : a.getAsJsonArray()) {
                JsonObject o = e.getAsJsonObject();
                int id = o.get("id").getAsInt();
                SpigetPlugin pl = new SpigetPlugin(id);
                list.add(pl);
            }
            return list.toArray(new SpigetPlugin[list.size()]);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new SpigetPlugin[0];
    }

}
