/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.utils.spiget;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.command.WgetCommand;
import com.betafase.mcmanager.utils.Utils;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import org.bukkit.entity.Player;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class SpigetPlugin {

    String name;
    String author;
    String tag;
    String last_update;
    final int id;
    boolean external;
    String file_size;
    String download_url;
    String rating;
    int downloads;

    public SpigetPlugin(int id) {
        this.name = "Unknown";
        this.author = "Unknown";
        this.tag = "Unknown";
        this.last_update = "Unknown";
        this.id = id;
        this.external = true;
        this.file_size = "Unknown";
        this.download_url = "";
        this.rating = "-/-";
        this.downloads = 0;
    }

    public void loadInformation() {
        author = new AuthorNameRequest(id).getName();
        ServerRequest r = new ServerRequest("resources/" + id);
        JsonObject o = r.getAsJsonObject();
        name = o.get("name").getAsString();
        if (name.length() > 60) {
            name = name.substring(0, 60);
        }
        tag = o.get("tag").getAsString();
        JsonObject file = o.getAsJsonObject("file");
        external = file.get("type").getAsString().equalsIgnoreCase("external");
        file_size = file.get("size").getAsDouble() + " " + file.get("sizeUnit").getAsString();
        download_url = "https://spigotmc.org/" + file.get("url").getAsString();
        JsonObject ro = o.getAsJsonObject("rating");
        rating = ro.get("average").getAsDouble() + " / 5 (" + ro.get("count") + " reviews)";
        downloads = o.get("downloads").getAsInt();
        last_update = Utils.makeDateReadable(o.get("updateDate").getAsLong() * 1000);
    }

    public void downloadDirectly(Player notifier, String path) {
        WgetCommand.downloadFile("https://api.spiget.org/v2/resources/" + this.id + "/download", path, notifier);
    }

//    public void download(Player notifier) {
//        if (hasDirectDownload()) {
//            downloadDirectly(notifier, "plugins/SpigotPlugin_" + id + ".jar");
//        } else {
//            WgetCommand.downloadFile(download_url, "plugins", notifier);
//        }
//    }

    public boolean hasDirectDownload() {
        HttpURLConnection con = null;
        try {
            con = (HttpURLConnection) new URL("https://api.spiget.org/v2/resources/" + this.id + "/download").openConnection();
            if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                con.disconnect();
                return true;
            }
        } catch (MalformedURLException ex) {
            MCManager.getLog().log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            MCManager.getLog().log(Level.SEVERE, null, ex);
        }
        if (con != null) {
            con.disconnect();
        }
        return false;
    }

    public String getName() {
        return name;
    }

    public int getID() {
        return id;
    }

    public String getRating() {
        return rating;
    }

    public String getAuthor() {
        return author;
    }

    public String getTag() {
        return tag;
    }

    public String getLastUpdate() {
        return last_update;
    }

    public boolean isExternalDownload() {
        return external;
    }

    public String getFileSize() {
        return isExternalDownload() ? "-/-" : file_size;
    }

    public String getDownloadURL() {
        return download_url;
    }

    public int getDownloads() {
        return downloads;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public void setLast_update(String last_update) {
        this.last_update = last_update;
    }

    public void setExternal(boolean external) {
        this.external = external;
    }

    public void setFile_size(String file_size) {
        this.file_size = file_size;
    }

    public void setDownload_url(String download_url) {
        this.download_url = download_url;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public void setDownloads(int downloads) {
        this.downloads = downloads;
    }

}
