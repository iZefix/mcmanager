/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This I18n-Class is owned by the Betafase Developer group and currently developed by iZefix.
 */
package com.betafase.mcmanager.utils;

import com.betafase.mcmanager.MCManager;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.util.ChatPaginator;

/**
 * © Betafase Developers, 2015
 *
 * You are <b>allowed</b> to modify this given code to help to build a better
 * future and to save drinking water. We are so kind, aren't we.
 *
 * @author Dominik B. (iZefix)
 */
public class Text {

    String t;
    Object[] params = null;
    String lang = defaultlang;

    private static HashMap<String, HashMap<String, String>> translations = new HashMap<String, HashMap<String, String>>();
    private static String defaultlang = "en_US";
    private static boolean debug = false;
    private static boolean peruserlang = true;
    private static PlayerLanguage playerlang = (Player p) -> defaultlang;

    public static void setPlayerLanguageExecutor(PlayerLanguage player_language) {
        playerlang = player_language;
    }

    /**
     * Sets the resource to load language files from. If a resource already
     * exists for this language, it will be overridden. Warning: Loading more
     * languages will decrease performance. Only load neccessairy languages.
     *
     * @param cfg The FileConfiguration to load the texts from.
     */
    public static void addLanguage(FileConfiguration cfg) {
        if (cfg.isString("language.id")) {
            if (cfg.isString("language.name") && cfg.isString("language.region") && cfg.isString("language.plugin") && cfg.isString("language.prefix")) {
                HashMap<String, String> lang;
                if (translations.containsKey(cfg.getString("language.id"))) {
                    lang = translations.get(cfg.getString("language.id"));
                } else {
                    lang = new HashMap<>();
                }
                for (String s : cfg.getKeys(true)) {
                    lang.put(cfg.getString("language.prefix") + "." + s, cfg.getString(s));
                }
                translations.put(cfg.getString("language.id"), lang);
                Bukkit.getLogger().log(Level.INFO, "[Translator] Loaded language ''{0}({1})'' into cache for Plugin ''{2}''", new Object[]{cfg.getString("language.name"), cfg.getString("language.region"), cfg.getString("language.plugin")});
            } else {
                throw new IllegalArgumentException("Please provide a region or a list of regions");
            }
        } else {
            throw new IllegalArgumentException("Invalid Language file format. Check wiki for more information.");
        }
    }

    public static void addLanguage(Properties props) {
        if (props.containsKey("language.id")) {
            if (props.containsKey("language.name") && props.containsKey("language.region") && props.containsKey("language.plugin") && props.containsKey("language.prefix")) {
                HashMap<String, String> lang;
                if (translations.containsKey((String)props.get("language.id"))) {
                    lang = translations.get((String)props.get("language.id"));
                } else {
                    lang = new HashMap<>();
                }
                for (String s : props.stringPropertyNames()) {
                    lang.put(props.getProperty("language.prefix") + "." + s, props.getProperty(s));
                }
                translations.put(props.getProperty("language.id"), lang);
                Bukkit.getLogger().log(Level.INFO, "[Translator] Loaded language ''{0}({1})'' into cache for Plugin ''{2}''", new Object[]{props.getProperty("language.name"), props.getProperty("language.region"), props.getProperty("language.plugin")});
            } else {
                throw new IllegalArgumentException("Please provide a region or a list of regions");
            }
        } else {
            throw new IllegalArgumentException("Invalid Language file format. Check wiki for more information.");
        }
    }

    public static boolean removeLanguage(String language) {
        if (translations.containsKey(language)) {
            translations.remove(language);
            return true;
        }
        return false;
    }

    public static HashMap<String, HashMap<String, String>> getTranslations() {
        return translations;
    }

    /**
     * Sets the default language for this instance. This will be used as a
     * fallback language if a text does not exist for a certain language.
     * Default: en_US
     *
     * @param language The language
     */
    public static void setDefaultLanguage(String language) {
        defaultlang = language;
    }

    public static void setPerUserLanguage(boolean peruser) {
        peruserlang = peruser;
    }

    public static void setDebugMode(boolean debug) {
        Text.debug = debug;
    }

    public static String getDefaultLanguage() {
        return defaultlang;
    }

    /**
     * This will use the default language.
     *
     * @param msg
     */
    @Deprecated
    public Text(String msg) {
        this.t = msg;
    }

    public Text(String msg, String lang) {
        this.t = msg;
        this.lang = lang;
    }

    public Text(String msg, String lang, Object... params) {
        this.t = msg;
        this.params = params;
        this.lang = lang;
    }

    public static String getLanguage(Player p) {
        return playerlang.getLanguage(p);
    }

    public List<String> wordWrap(int linelenght, ChatColor color) {
        ArrayList<String> list = new ArrayList<>();
        for (String s : ChatPaginator.wordWrap(this.toString(), linelenght)) {
            list.add(color + ChatColor.stripColor(s));
        }
        return list;
    }

    public String[] wordWrapAsArray(int linelength, ChatColor color) {
        List<String> list = wordWrap(linelength, color);
        return list.toArray(new String[list.size()]);
    }

    public static List<String> wordWrap(String text, int linelenght, ChatColor color) {
        ArrayList<String> list = new ArrayList<>();
        for (String s : ChatPaginator.wordWrap(text, linelenght)) {
            list.add(color + ChatColor.stripColor(s));
        }
        return list;
    }

    public static String[] wordWrapAsArray(String text, int linelength, ChatColor color) {
        List<String> list = wordWrap(text, linelength, color);
        return list.toArray(new String[list.size()]);
    }

    @Override
    public String toString() {
        String t = this.t;
        if (debug) {
            return t;
        }
        if (peruserlang) {
            if (translations.containsKey(lang)) {
                HashMap<String, String> cfg = translations.get(lang);
                if (cfg.containsKey(t)) {
                    t = cfg.get(t);
                } else {
                    Bukkit.getLogger().log(Level.WARNING, "[Translator] Language ''{0}'' does not contain the text ''{1}''. Falling back to default language.", new Object[]{lang, t});
                }
            } else {
                Bukkit.getLogger().log(Level.WARNING, "[Translator] Did not find client region ''{0}''. Checking for similar regions...", lang.substring(2));
                String language = defaultlang;
                for (String s : translations.keySet()) {
                    if (s.substring(0, 2).equalsIgnoreCase(lang.substring(0, 2))) {
                        language = s;
                        break;
                    }
                }
                HashMap<String, String> cfg = translations.get(language);
                if (cfg.containsKey(t)) {
                    t = cfg.get(t);
                } else {
                    Bukkit.getLogger().log(Level.WARNING, "[Translator] Language ''{0}'' does not contain the text ''{1}''. Falling back to default language.", new Object[]{lang, t});
                }
            }

        }
        if (!peruserlang || t.equalsIgnoreCase(this.t)) {
            if (translations.containsKey(defaultlang)) {
                if (translations.get(defaultlang).containsKey(t)) {
                    t = translations.get(defaultlang).get(t);
                } else {
                    Bukkit.getLogger().log(Level.SEVERE, "[Translator] Missing text for ''{0}'' in defaultlanguage.", t);
                }
            } else {
                Bukkit.getLogger().log(Level.SEVERE, "[Translator] Fatal error: No language file for the default language existing.");
            }
        }
        t = ChatColor.translateAlternateColorCodes('&', t);
        if (params != null && params.length != 0) {
            return MessageFormat.format(t, params);
        } else {
            return t;
        }
    }

    public interface PlayerLanguage {

        String getLanguage(Player p);
    }

    /**
     * Full message ready to be sent, including prefix
     *
     * @param ex
     * @param lang
     * @return
     */
    public static String reportError(Exception ex, String lang) {
        return MCManager.getPrefix() + new Text("mcm.exception.message", lang, ex.getClass().getSimpleName(), ex.getMessage()).toString();
    }

}
