/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by the Betafase Developers (Betafase.de) and currently developed by iZefix.
 */
package com.betafase.mcmanager.utils;

import java.io.File;

/**
 * © Betafase Developers, 2015
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class SystemInfo {

    public String OsName() {
        return System.getProperty("os.name");
    }

    public String OsVersion() {
        return System.getProperty("os.version");
    }

    public String OsArch() {
        return System.getProperty("os.arch");
    }

    public String JavaVersion() {
        return System.getProperty("java.version");
    }

    public long totalMem() {
        return Runtime.getRuntime().totalMemory();
    }

    public long usedMem() {
        return Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
    }

    public long usedSpace() {
        return maxSpace() - new File(System.getProperty("user.dir")).getFreeSpace();
    }

    public long maxSpace() {
        return new File(System.getProperty("user.dir")).getTotalSpace();
    }
}
