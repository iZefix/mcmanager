/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.utils;

import com.betafase.mcmanager.MCManager;
import java.util.HashMap;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public interface CommandExecuter {

    HashMap<String, CommandExecuter> items = new HashMap<>();

    static void addNewExecuter(CommandExecuter ex) {
        items.put(System.currentTimeMillis() + "", ex);
    }

    /**
     * Adds a timed executor which becomes invalid after a certain period of
     * time.
     *
     * @param time time in seconds
     * @param ex
     */
    static void addTimedExecuter(int time, CommandExecuter ex) {
        String id = System.currentTimeMillis() + "";
        items.put(id, ex);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (items.containsKey(id)) {
                    items.remove(id);
                }
            }
        }.runTaskLater(MCManager.getInstance(), time * 20);
    }

    /**
     * Executes the predefined action.
     *
     * @param p The CommandSender
     * @return true if item should be deleted.
     */
    boolean onExecute(Player p);

}
