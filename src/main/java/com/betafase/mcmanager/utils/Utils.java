/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.utils;

import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class Utils {

    public static String makeDateReadable(long date) {
        return new SimpleDateFormat("dd.MM.YYYY HH:mm").format(new Date(date));
    }

    public static String makeHealthReadable(double health) {
        StringBuilder b = new StringBuilder();
        int i = 0;
        while (i <= health / 2) {
            b.append('❤');
            i++;
        }
        if (health % 2 == 1) {
            b.append('❥');
        }
        return "§c" + b.toString();
    }

    public static void unloadWorld(World w) {
        WorldLoadManager.unloadWorld(w);
    }

    public static boolean isUnloading(String world) {
        return WorldLoadManager.isUnloading(world);
    }

    public static int getPing(Player p) {
        try {
            Object entityPlayer = p.getClass().getMethod("getHandle").invoke(p);
            return (int) entityPlayer.getClass().getField("ping").get(entityPlayer);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException | NoSuchFieldException e) {
            e.printStackTrace();
        }
        return -1;
    }

    /*public static long getFileSize(File file) {
     if (file.isDirectory()) {
     long result = 0;
     for (File f : file.listFiles()) {
     result = result + getFileSize(file);
     }
     return result;
     } else {
     return file.length();
     }
     }*/
}
