/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.utils;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.command.WgetCommand;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DurationFormatUtils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * © Betafase Developers, 2016
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class SpigotUpdater {

    private String version;
    private static Process p = null;
    private Logger l = MCManager.getInstance().getLogger();

    public String getVersion() {
        return version;
    }

    public static void cancelUpdate() {
        if (isUpdating()) {
            p.destroyForcibly();
            p = null;
        }
    }

    public static boolean isUpdating() {
        if (p != null) {
            if (p.isAlive()) {
                return true;
            }
            p = null;
        }
        return false;
    }

    public int buildsBehind() {
        version = Bukkit.getVersion();
        if (version == null) {
            version = "Custom";
        }
        if (version.startsWith("git-Spigot-")) {
            String[] parts = version.substring("git-Spigot-".length()).split("-");
            int cbVersions = getDistance("craftbukkit", parts[1].substring(0, parts[1].indexOf(' ')));
            int spigotVersions = getDistance("spigot", parts[0]);
            version = "Spigot";
            if (cbVersions == -1 || spigotVersions == -1) {
                return -1;
            } else if (cbVersions == 0 && spigotVersions == 0) {
                return 0;
            } else {
                return cbVersions + spigotVersions;
            }
        } else if (version.startsWith("git-Bukkit-")) {
            version = version.substring("git-Bukkit-".length());
            int cbVersions = getDistance("craftbukkit", version.substring(0, version.indexOf(' ')));
            version = "Bukkit";
            switch (cbVersions) {
                case -1:
                    return -1;
                case 0:
                    return 0;
                default:
                    return cbVersions;
            }
        } else {
            return -2;
        }
    }

    private int getDistance(String repo, String hash) {
        try {
            BufferedReader reader = Resources.asCharSource(
                    new URL("https://hub.spigotmc.org/stash/rest/api/1.0/projects/SPIGOT/repos/" + repo + "/commits?since=" + URLEncoder.encode(hash, "UTF-8") + "&withCounts=true"),
                    Charsets.UTF_8
            ).openBufferedStream();
            try {
                JSONObject obj = (JSONObject) new JSONParser().parse(reader);
                return ((Number) obj.get("totalCount")).intValue();
            } catch (ParseException ex) {
                ex.printStackTrace();
                return -1;
            } finally {
                reader.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void performUpdate(String version, Player notifier) {
        //Create Paths, download BuildTools
        if (isUpdating()) {
            l.log(Level.SEVERE, "A Update is currently running. Please cancel it first.");
            return;
        }
        long time = System.currentTimeMillis();
        String prefix = MCManager.getPrefix();
        Bukkit.broadcastMessage(prefix + new Text("mcm.spigot.lag_warning"));
        if (notifier != null) {
            notifier.sendMessage(prefix + new Text("mcm.spigot.update_info"));
        }
        if (Bukkit.isPrimaryThread()) {
            l.log(Level.SEVERE, "This Action takes a lot of time. Running it in the primary thread would cause a lot of lag and might result in a server crash. Cancelling.");
            return;
        }
        if (version == null) {
            version = "";
        }
        //new Message("spigot_update.started", "Update started - DO NOT TURN OFF SERVER", true).addAll().send();
        File builder = new File("SpigotUpdater");
        if (builder.exists()) {
            try {
                FileUtils.deleteDirectory(builder);
            } catch (IOException ex) {
                Logger.getLogger(SpigotUpdater.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        builder.mkdir();
        File buildTools = new File(builder, "BuildTools.jar");
        if (buildTools.exists()) {
            buildTools.delete();
        }
        if (notifier != null) {
            notifier.sendMessage(prefix + new Text("mcm.spigot.build_tools"));
        }
        WgetCommand.downloadFile("https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar", builder.getPath(), notifier);
        //FileDownloader.downloadSyncFile(, buildTools, "Firefox/5.0", true, token);
        if (!buildTools.exists()) {
            l.log(Level.SEVERE, "Failed to download BuildTools, aborting!");
            if (notifier != null) {
                notifier.sendMessage(prefix + "§cFailed to download BuildTools, aborting. See the log for more details.");
            }
            return;
        }
        try {
            if (notifier != null) {
                notifier.sendMessage(prefix + new Text("mcm.spigot.build_start"));
            }
            p = new ProcessBuilder("java", "-jar", "BuildTools.jar", version).directory(builder).start();
            // = Runtime.getRuntime().exec("java -jar  " + version, null, builder);
            InputStream toRead = p.getInputStream();
            BufferedReader r = new BufferedReader(new InputStreamReader(toRead));
            String next = null;
            while ((next = r.readLine()) != null) {
                l.log(Level.INFO, next);
                if (notifier != null && notifier.isOnline()) {
                    notifier.sendMessage(prefix + "§7" + next);
                }
            }
            p.waitFor();
            p = null;
            l.log(Level.INFO, "New Files have been created. Check the SpigotUpdater directory ;)");
            time = System.currentTimeMillis() - time;
            if (notifier != null) {
                String lang = Text.getLanguage(notifier);
                notifier.sendMessage(prefix + new Text("mcm.spigot.build_done", Text.getDefaultLanguage(), DurationFormatUtils.formatDurationHMS(time)));
                TextComponent pre = new TextComponent(prefix);
                for (File f : builder.listFiles((File dir, String name) -> !name.equalsIgnoreCase("BuildTools.jar") && name.endsWith(".jar"))) {
                    TextComponent usr = new TextComponent(new Text("mcm.spigot.usr", lang, f.getName()).toString());
                    usr.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/manager usr " + f.getName()));
                    notifier.spigot().sendMessage(pre, usr);
                }
                notifier.playSound(notifier.getLocation(), Sound.ENTITY_WITHER_DEATH, (float) 0.3, (float) 1.0);
            }
            //new Message("spigot_update.complete", "Spigot has been updated. New Files are in the SpigotUpdater directory", true).addAll().send();
        } catch (IOException | InterruptedException ex) {
            ex.printStackTrace();
        }
    }

}
