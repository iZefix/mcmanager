/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.utils;

import com.betafase.mcmanager.MCManager;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class BackupManager {

    private BukkitTask backup = null;
    private int timing;

    private boolean paused = false;

    public BackupManager() {
        timing = MCManager.getConfiguration().getInt("backup.interval", 30);
    }

    public void restart() {
        timing = MCManager.getConfiguration().getInt("backup.interval", 30);
        cancel();
        start();
    }

    public boolean isActive() {
        return backup != null;
    }

    public void start() {
        MCManager.getLog().log(Level.INFO, "Automatic Backup is currently set to {0} minutes. You can change this in the config.", timing);
        backup = new BukkitRunnable() {
            @Override
            public void run() {
                while (paused) {

                }
                backupData();
            }
        }.runTaskTimerAsynchronously(MCManager.getInstance(), 20L, timing * 60 * 20L);
    }

    public void cancel() {
        if (isActive()) {
            backup.cancel();
        }
    }

    public boolean isPaused() {
        return paused;
    }

    public void setPaused(boolean paused) {
        this.paused = paused;
    }

    public void backupData() {
        FileConfiguration cfg = MCManager.getConfiguration();
        if (cfg.getBoolean("backup.empty", false) || !Bukkit.getOnlinePlayers().isEmpty()) {
            try {
                MCManager.getLog().log(Level.INFO, "Starting Backup...");
                Bukkit.broadcastMessage(MCManager.getPrefix() + new Text("mcm.backup.warning"));
                Bukkit.savePlayers();
                for (World w : Bukkit.getWorlds()) {
                    w.save();
                }
                try {
                    Thread.sleep(500);
                } catch (InterruptedException ex) {
                    Logger.getLogger(BackupManager.class.getName()).log(Level.SEVERE, null, ex);
                }
                File backup = new File(cfg.getString("backup.folder", "backups"));
                boolean zip = cfg.getBoolean("backup.zip", true);
                if (cfg.getBoolean("backup.worlds", false)) {
                    String name = "worlds-" + new SimpleDateFormat("DDD-HH:mm").format(new Date());
                    if (!backup.exists()) {
                        backup.mkdirs();
                    } else {
                        int amount = cfg.getInt("backup.amount", 5);
                        if (amount != -1) {
                            if (backup.list().length >= amount) {
                                MCManager.getLog().log(Level.INFO, "Deleting previous files to make space...");
                                List<File> files = Arrays.asList(backup.listFiles());
                                files.sort((File o1, File o2) -> {
                                    if (o1.lastModified() < o2.lastModified()) {
                                        return -1;
                                    } else {
                                        return 1;
                                    }
                                });
                                for (File f : files) {
                                    if (backup.list().length >= amount) {
                                        if (f.isFile()) {
                                            f.delete();
                                        } else {
                                            try {
                                                FileUtils.deleteDirectory(f);
                                            } catch (IOException ex) {
                                                MCManager.getLog().log(Level.SEVERE, null, ex);
                                            }
                                        }
                                    } else {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (zip) {
                        try {
                            List<File> toZip = new LinkedList<>();
                            for (World w : Bukkit.getWorlds()) {
                                getAllFiles(w.getWorldFolder(), toZip);
                            }
                            writeZipFile(Bukkit.getWorldContainer(), new File(backup, name + ".zip"), toZip);
                        } catch (IOException ex) {
                            MCManager.getLog().log(Level.SEVERE, "Failed to create compressed backup.", ex);
                        }
                    } else {
                        try {
                            File newB = new File(backup, name);
                            newB.mkdirs();
                            for (World w : Bukkit.getWorlds()) {
                                FileUtils.copyDirectoryToDirectory(w.getWorldFolder(), newB);
                            }
                        } catch (IOException ex) {
                            MCManager.getLog().log(Level.SEVERE, "Failed to create uncompressed backup.", ex);
                        }
                    }
                }
                if (cfg.getBoolean("backup.plugins", false)) {
                    MCManager.getLog().log(Level.INFO, "Creating additional plugin backups...");
                    String name = "plugindata-" + new SimpleDateFormat("DDD-HH:mm").format(new Date());
                    if (!backup.exists()) {
                        backup.mkdirs();
                    } else {
                        int amount = cfg.getInt("backup.amount", 5);
                        if (amount != -1) {
                            if (backup.list().length >= amount) {
                                MCManager.getLog().log(Level.INFO, "Deleting previous files to make space...");
                                List<File> files = Arrays.asList(backup.listFiles());
                                files.sort((File o1, File o2) -> {
                                    if (o1.lastModified() < o2.lastModified()) {
                                        return -1;
                                    } else {
                                        return 1;
                                    }
                                });
                                for (File f : files) {
                                    if (backup.list().length >= amount) {
                                        if (f.isFile()) {
                                            f.delete();
                                        } else {
                                            try {
                                                FileUtils.deleteDirectory(f);
                                            } catch (IOException ex) {
                                                MCManager.getLog().log(Level.SEVERE, null, ex);
                                            }
                                        }
                                    } else {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (zip) {
                        try {
                            List<File> toZip = new LinkedList<>();
                            for (Plugin pl : Bukkit.getPluginManager().getPlugins()) {
                                if (pl.getDataFolder().exists()) {
                                    getAllFiles(pl.getDataFolder(), toZip);
                                }
                            }
                            writeZipFile(Bukkit.getWorldContainer(), new File(backup, name + ".zip"), toZip);
                        } catch (IOException ex) {
                            MCManager.getLog().log(Level.SEVERE, "Failed to create compressed plugindata backup.", ex);
                        }
                    } else {
                        try {
                            File newB = new File(backup, name);
                            newB.mkdirs();
                            for (Plugin pl : Bukkit.getPluginManager().getPlugins()) {
                                if (pl.getDataFolder().exists()) {
                                    FileUtils.copyDirectoryToDirectory(pl.getDataFolder(), newB);
                                }
                            }
                        } catch (IOException ex) {
                            MCManager.getLog().log(Level.SEVERE, "Failed to create uncompressed plugindata backup.", ex);
                        }
                    }
                }
                Bukkit.broadcastMessage(MCManager.getPrefix() + new Text("mcm.backup.complete"));
            } catch (Exception e) {
                MCManager.getLog().log(Level.SEVERE, "Failed to create Backup", e);
            }
        }
    }

    public static void getAllFiles(File dir, List<File> fileList) {
        File[] files = dir.listFiles();
        for (File file : files) {
            fileList.add(file);
            if (file.isDirectory()) {
                getAllFiles(file, fileList);
            }
        }
    }

    public static void writeZipFile(File directory, File zipfile, List<File> fileList) throws FileNotFoundException, IOException {
        FileOutputStream fos = new FileOutputStream(zipfile);
        ZipOutputStream zos = new ZipOutputStream(fos);

        for (File file : fileList) {
            if (!file.isDirectory()) {
                addToZip(directory, file, zos);
            }
        }

        zos.close();
        fos.close();
    }

    private static void addToZip(File directoryToZip, File file, ZipOutputStream zos) throws FileNotFoundException,
            IOException {

        FileInputStream fis = new FileInputStream(file);
        String zipFilePath = file.getCanonicalPath().substring(directoryToZip.getCanonicalPath().length() + 1,
                file.getCanonicalPath().length());
        //System.out.println("Writing '" + zipFilePath + "' to zip file");
        ZipEntry zipEntry = new ZipEntry(zipFilePath);
        zos.putNextEntry(zipEntry);

        byte[] bytes = new byte[1024];
        int length;
        while ((length = fis.read(bytes)) >= 0) {
            zos.write(bytes, 0, length);
        }

        zos.closeEntry();
        fis.close();
    }

}
