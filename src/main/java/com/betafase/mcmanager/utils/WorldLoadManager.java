/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.utils;

import com.betafase.mcmanager.MCManager;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class WorldLoadManager {

    private static List<String> unloading = new LinkedList<>();

    public static void unloadWorld(World w) {
        if (isUnloading(w.getName())) {
            return;
        }
        unloading.add(w.getName());
        for (Chunk c : w.getLoadedChunks()) {
            boolean unload = c.unload(false, false);
        }
        if (Bukkit.unloadWorld(w, false)) {
        } else {
            MCManager.getLog().log(Level.WARNING, "Unloading {0} with unsafe NMS Methods. If there is anything wrong, please restart the server.", w.getName());
            try {
                Map<String, World> worlds = new HashMap<>();
                for (World wo : Bukkit.getWorlds()) {
                    worlds.put(wo.getName().toLowerCase(), wo);
                }

                Class MinecraftServer = NMSUtils.getNMSClass("MinecraftServer");
                Class CraftWorld = NMSUtils.getCBClass("CraftWorld");
                //Class WorldServer = NMSUtils.getNMSClass("WorldServer");
                Class CraftServer = NMSUtils.getCBClass("CraftServer");

                Object console = MinecraftServer.getMethod("getServer").invoke(null);
                Object handle = CraftWorld.getMethod("getHandle").invoke(CraftWorld.cast(w));

                worlds.remove(w.getName().toLowerCase());

                List<?> worldsObject = (List<?>) MinecraftServer.getField("worlds").get(console);
                worldsObject.remove(worldsObject.indexOf(handle));

                setFinalStatic(CraftServer.cast(Bukkit.getServer()), CraftServer.getDeclaredField("worlds"), worlds);
            } catch (Exception ex) {
                MCManager.getLog().log(Level.SEVERE, "Failed to unload " + w.getName() + " with NMS Code.", ex);
            }
        }
        unloading.remove(w.getName());
    }

    public static boolean isUnloading(String world) {
        return unloading.contains(world);
    }

    //            MinecraftServer console = MinecraftServer.getServer();
//            WorldServer handle = ((CraftWorld) world).getHandle();
//            worlds.remove(world.getName().toLowerCase());
//            console.worlds.remove(console.worlds.indexOf(handle));
//            try {
//                setFinalStatic((CraftServer) Bukkit.getServer(), CraftServer.class.getDeclaredField("worlds"), worlds);
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
    private static void setFinalStatic(Object instance, Field field, Object newValue) throws Exception {
        field.setAccessible(true);

        Field modifiersField = Field.class.getDeclaredField("modifiers");
        modifiersField.setAccessible(true);
        modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

        field.set(instance, newValue);
    }
}
