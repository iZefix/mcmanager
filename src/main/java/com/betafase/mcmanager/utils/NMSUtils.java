/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.utils;

import java.util.logging.Level;
import org.bukkit.Bukkit;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class NMSUtils {

    private static String nms;

    public static String getNMSVersion() {
        if (nms == null) {
            Bukkit.getLogger().log(Level.INFO, "Bukkit version is {0}", Bukkit.getBukkitVersion());
            nms = Bukkit.getServer().getClass().getPackage().getName();
            nms = nms.substring(nms.lastIndexOf(".") + 1);
        }
        return nms;
    }

    public static Class<?> getNMSClass(String name) throws ClassNotFoundException {
        return Class.forName("net.minecraft.server." + getNMSVersion() + "." + name);
    }

    public static Class<?> getCBClass(String name) throws ClassNotFoundException {
        return Class.forName("org.bukkit.craftbukkit." + getNMSVersion() + "." + name);
    }

}
