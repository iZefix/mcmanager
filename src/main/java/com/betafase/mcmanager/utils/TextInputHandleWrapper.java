/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.utils;

import com.betafase.mcmanager.MCManager;
import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.BlockPosition;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import com.betafase.mcmanager.api.SignInputHandler;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class TextInputHandleWrapper extends PacketAdapter {

    private final HashMap<String, SignInputHandler> handlers;
    private final Random r;
    private final ProtocolManager protocolManager;

    public TextInputHandleWrapper(Plugin plugin) {
        super(plugin, ListenerPriority.NORMAL, PacketType.Play.Client.UPDATE_SIGN);
        handlers = new HashMap<>();
        r = new Random();
        protocolManager = com.comphenix.protocol.ProtocolLibrary.getProtocolManager();
        protocolManager.addPacketListener(this);
    }

    public boolean requestInput(Player p, SignInputHandler h) {
        try {
            BlockPosition pos = new BlockPosition(r.nextInt(200) + 100000, 0, r.nextInt(200) + 100000);
            String[] s = h.getDefault();
            Location loc = null;
            if (s != null) {
                loc = new Location(p.getWorld(), pos.getX(), pos.getY(), pos.getZ());
                p.sendBlockChange(loc, Material.SIGN, (byte) 0);
                p.sendSignChange(loc, s);
//                PacketContainer signText = new PacketContainer(PacketType.Play.Server.TILE_ENTITY_DATA);
//                try {
//                    signText.getBlockPositionModifier().write(0, pos);
//                    signText.getIntegers().write(0, 9);
//                    try {
//                        WrappedChatComponent[] components = new WrappedChatComponent[4];
//                        for (int i = 0; i < components.length; i++) {
//                            components[i] = WrappedChatComponent.fromText(s[i]);
//                        }
//                        signText.getChatComponentArrays().write(0, components);
//                    } catch (FieldAccessException ex) {
//                        System.out.println("Using this method.");
//                        NbtCompound nbt = (NbtCompound) signText.getNbtModifier().read(0);
//                        for (int i = 0; i < 4; i++) {
//                            nbt.put("Text" + (i + 1), s[i]);
//                            //nbt.put("Text" + (i + 1), "{\"extra\":[{\"text\":\"" + s[i] + "\"}],\"text\":\"\"}");
//                        }
//                        signText.getNbtModifier().write(0, nbt);
//                    }
//                } catch (NoSuchMethodError nsme) {
//                    signText.getIntegers().write(0, pos.getX()).write(1, pos.getY()).write(2, pos.getZ());
//                    signText.getStringArrays().write(0, s);
//                }
//                try {
//                    protocolManager.sendServerPacket(p, signText);
//                } catch (InvocationTargetException ex) {
//                    Logger.getLogger(TextInputHandleWrapper.class.getName()).log(Level.SEVERE, null, ex);
//                }
            }
            PacketContainer signGui = new PacketContainer(PacketType.Play.Server.OPEN_SIGN_EDITOR);
            signGui.getBlockPositionModifier().write(0, pos);

            try {
                protocolManager.sendServerPacket(p, signGui);
                if (loc != null) {
                    p.sendBlockChange(loc, Material.BEDROCK, (byte) 0);
                }
                String dataset = pos.getX() + ":" + pos.getY() + ":" + pos.getZ();
                handlers.put(dataset, h);
                //System.out.println("Sign input for " + dataset);
                return true;
            } catch (InvocationTargetException ex) {
                Logger.getLogger(TextInputHandleWrapper.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        } catch (Exception exception) {
            MCManager.getLog().log(Level.SEVERE, "Failed to send sign packets", exception);
            return false;
        }
    }

    public void disable() {
        protocolManager.removePacketListener(this);
    }

    @Override
    public void onPacketReceiving(PacketEvent event) {
        PacketContainer packet = event.getPacket();
        BlockPosition pos = packet.getBlockPositionModifier().read(0);
        String dataset = pos.getX() + ":" + pos.getY() + ":" + pos.getZ();
        //System.out.println("Sign input for " + dataset);
        //System.out.println("Size of Handlers: " + handlers.size());
        if (handlers.containsKey(dataset)) {
            event.setCancelled(true);
            try {
                String[] lines = event.getPacket().getStringArrays().getValues().get(0);
                handlers.get(pos.getX() + ":" + pos.getY() + ":" + pos.getZ()).handleTextInput(lines);
            } catch (Exception ex) {
                MCManager.getLog().log(Level.SEVERE, "Failed to correctly parse SignInput.", ex);
                return;
            }
            handlers.remove(dataset);
            //System.out.println("Received and handled packet.");
        } else {
            //System.out.println("Received packet but not handling it. (wrong pos)");
        }
    }

}
