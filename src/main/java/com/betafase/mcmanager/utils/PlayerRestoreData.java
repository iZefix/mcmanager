/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.utils;

import java.util.UUID;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.PlayerInventory;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class PlayerRestoreData {

    private final PlayerInventory inv;
    private final double health;
    private final int food;
    private final Location loc;
    private final GameMode gm;
    private final UUID uuid;

    public PlayerRestoreData(UUID uuid, PlayerInventory inv, double health, int food, Location loc, GameMode gm) {
        this.inv = inv;
        this.health = health;
        this.food = food;
        this.loc = loc;
        this.gm = gm;
        this.uuid = uuid;
    }

    public void restore(boolean fromDisc) {
        Player p = Bukkit.getPlayer(uuid);
        if (p != null && p.isOnline()) {
            if (loc.getBlock().getType() != Material.AIR || loc.clone().add(0, 1, 0).getBlock().getType() != Material.AIR) {
                p.teleport(p.getBedSpawnLocation() == null ? loc.getWorld().getSpawnLocation() : p.getBedSpawnLocation());
            } else {
                p.teleport(loc, PlayerTeleportEvent.TeleportCause.PLUGIN);
            }
            if (fromDisc) {
                p.loadData();
            } else {
                p.getInventory().setContents(inv.getContents());
                p.setHealth(health);
                p.setFoodLevel(food);
            }
            p.setGameMode(gm);
        }
    }

}
