/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.gui;

import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.MCManager;
import java.util.logging.Level;
import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.plugin.Plugin;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class GUIManager implements Listener {

    final Plugin pl;

    public GUIManager(Plugin pl) {
        this.pl = pl;
        registerListener();
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        if (e.getClickedInventory() != null && e.getClickedInventory().getHolder() instanceof Menu) {
            Menu m = (Menu) e.getInventory().getHolder();
            try {
                m.onClick(e);
                if (m.isAutoCancel()) {
                    e.setCancelled(true);
                    e.setResult(Event.Result.DENY);
                }
            } catch (Exception ex) {
                e.setCancelled(true);
                e.setResult(Event.Result.DENY);
                MCManager.getLog().log(Level.SEVERE, "Menu " + m.getClass().getSimpleName() + " caused an uncaught " + ex.getClass().getName() + ".", ex);
            }

        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent e) {
        if (e.getInventory() != null && e.getInventory().getHolder() instanceof Menu) {
            Menu m = (Menu) e.getInventory().getHolder();
            try {
                m.onClose(e);
            } catch (Exception ex) {
                MCManager.getLog().log(Level.SEVERE, "Menu " + m.getClass().getSimpleName() + " caused an uncaught " + ex.getClass().getName() + " while closing.", ex);
            }
        }

    }

    private void registerListener() {
        Bukkit.getPluginManager().registerEvents(this, pl);
    }

}
