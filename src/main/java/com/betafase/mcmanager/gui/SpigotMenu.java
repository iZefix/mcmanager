/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.gui;

import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.BlockColor;
import com.betafase.mcmanager.command.WgetCommand;
import com.betafase.mcmanager.utils.Text;
import com.betafase.mcmanager.utils.spiget.SpigetPlugin;
import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.ChatPaginator;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class SpigotMenu extends Menu {

    int page;
    final PluginDataLoader loader;

    public SpigotMenu(String lang, int page, PluginDataLoader loader) {
        super(new Text("mcm.gui.spigot.title", lang).toString(), 36, lang);
        this.page = page;
        this.loader = loader;
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        this.setItem(31, new MenuItem(GUIUtils.back(lang)));
        if (page > 0) {
            this.setItem(30, new MenuItem(GUIUtils.previous_page(lang, page)));
        }
        MenuItem loading = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.YELLOW, "§eLoading data from spiget.org...");
        this.setItem(4, loading);
        new BukkitRunnable() {
            @Override
            public void run() {
                int counter = -page * 14;
                if (loader.requireLoading()) {
                    loader.publishData();
                }
                MenuItem loading = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.YELLOW, "§eReading results...");
                setItem(4, loading);
                for (SpigetPlugin d : loader.display) {
                    if (counter < 0) {
                    } else if (counter == 14) {
                        setItem(32, new MenuItem(GUIUtils.next_page(lang, page + 2)));
                        break;
                    } else {
                        try {
                            d.loadInformation();
                            String[] tag = ChatPaginator.wordWrap(d.getTag(), 40);
                            ArrayList<String> lore = new ArrayList<>(7);
                            for (String s : tag) {
                                lore.add("§7" + ChatColor.stripColor(s));
                            }
                            lore.add(new Text("mcm.gui.spigot.author", lang, d.getAuthor()).toString());
                            lore.add(new Text("mcm.gui.spigot.rating", lang, d.getRating()).toString());
                            lore.add(new Text("mcm.gui.spigot.downloads", lang, d.getDownloads()).toString());
                            lore.add(new Text("mcm.gui.spigot.last_update", lang, d.getLastUpdate()).toString());
                            lore.add(" ");
                            lore.add(loader.getActionCall(d, lang));
                            MenuItem plugin_view = new MenuItem(Material.MAGMA_CREAM, "§6§l" + d.getName(), lore.toArray(new String[lore.size()]));
                            setItem(GUIUtils.convertCounterSlot(counter), plugin_view);

                        } catch (Exception ex) {
                            MenuItem error = new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 14, new Text("mcm.gui.spigot.error", lang).toString(), "§7" + ex.getMessage());
                            setItem(GUIUtils.convertCounterSlot(counter), error);
                            ex.printStackTrace();
                        }
                    }
                    counter++;
                }

                MenuItem loaded = new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 5, "§a" + new Text("mcm.gui.spigot.all_loaded", lang));
                setItem(4, loaded);
            }
        }.runTaskAsynchronously(MCManager.getInstance());
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        int slot = e.getSlot();
        Player p = (Player) e.getWhoClicked();
        switch (slot) {
            case 30:
                if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new SpigotMenu(lang, page - 1, loader).open(p);
                }
                break;
            case 31:
                loader.onBack(p);
                break;
            case 32:
                if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new SpigotMenu(lang, page + 1, loader).open(p);
                }
            default:
                int converted = GUIUtils.convertInventorySlot(slot);
                if (converted != -1) {
                    int id = converted + page * 14;
                    if (id < loader.display.length) {
                        SpigetPlugin d = loader.display[id];
                        loader.onClick(p, d, lang, e);
                    }
                }
                break;
        }

    }

    public abstract static class PluginDataLoader {

        public SpigetPlugin[] display;

        public PluginDataLoader() {

        }

        public boolean requireLoading() {
            return display == null;
        }

        public void publishData() {
            display = loadData();
        }

        public abstract SpigetPlugin[] loadData();

        public abstract void onBack(Player p);

        public String getActionCall(SpigetPlugin plugin, String lang) {
            return new Text("mcm.gui.spigot.download", lang, (plugin.isExternalDownload() ? "(External)" : "§6(" + plugin.getFileSize() + ")")).toString();
        }

        public void onClick(Player p, SpigetPlugin plugin, String lang, InventoryClickEvent e) {
            e.getView().close();
            Bukkit.getScheduler().runTaskAsynchronously(MCManager.getInstance(), () -> {
                if (plugin.hasDirectDownload()) {
                    p.sendMessage(MCManager.getPrefix() + "§aCached Download from spiget.org found. Downloading directly...");
                    plugin.downloadDirectly(p, "plugins/SpigotPlugin_" + plugin.getID() + ".jar");
                } else if (plugin.isExternalDownload()) {
                    p.sendMessage(MCManager.getPrefix() + "§eThis Plugin has an external download. You must download it manually: §n" + plugin.getDownloadURL());
                } else {
                    p.sendMessage(MCManager.getPrefix() + "§aDownload started. Please wait...");
                    WgetCommand.downloadFile(plugin.getDownloadURL(), "plugins/SpigotPlugin_" + plugin.getID() + ".jar", p);
                }
            });
        }
    }
}
