/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.gui;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.utils.Text;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author dbrosch
 */
public class SettingsMenu extends Menu {

    public SettingsMenu(String lang) {
        super(new Text("mcm.gui.settings.title", lang).toString(), 36, lang);
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        setItem(31, GUIUtils.back(lang));

        new BukkitRunnable() {
            @Override
            public void run() {
                FileConfiguration cfg = MCManager.getConfiguration();
                setItem(13, new MenuItem(Material.BOOK,
                        new Text("mcm.gui.settings.lang_t", lang).toString(),
                        new Text("mcm.gui.settings.lang_d", lang).wordWrapAsArray(40, ChatColor.GRAY)));
                setItem(22, new MenuItem(Material.IRON_PICKAXE, new Text("mcm.gui.settings.lang_c", lang).toString()));
                /*setItem(11, new MenuItem(Material.ENDER_CHEST,
                        new Text("mcm.gui.backup.set_auto_t", lang).toString(),
                        new Text("mcm.gui.backup.set_auto_d", lang).wordWrapAsArray(40, ChatColor.GRAY)));
                 */
            }
        }.runTaskAsynchronously(MCManager.getInstance());
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        switch (e.getSlot()) {
            case 22:
                new LanguageSettingsMenu(lang).open(p);
                break;
            case 31:
                new MainMenu(p, lang).open(p);
                break;
        }
    }

    private static class LanguageSettingsMenu extends Menu {

        public LanguageSettingsMenu(String lang) {
            super(new Text("mcm.gui.lang_settings.title", lang).toString(), 36, lang);
            MenuItem black = GUIUtils.black();
            for (int i = 0; i < 9; i++) {
                setItem(i, black);
                setItem(27 + i, black);
            }
            setItem(9, black);
            setItem(18, black);
            setItem(17, black);
            setItem(26, black);
            setItem(31, GUIUtils.back(lang));

            MenuItem english = new MenuItem(Material.BOOK, new Text("mcm.gui.lang_settings.english", lang).toString());
            setItem(13, english);
            setItem(22, GUIUtils.displayBoolean(lang.equalsIgnoreCase("en_US") || lang.equalsIgnoreCase("default"), lang));
            setItem(10, new MenuItem(Material.BOOK, new Text("mcm.gui.lang_settings.german", lang).toString()));
            setItem(19, GUIUtils.displayBoolean(lang.equalsIgnoreCase("de_DE"), lang));
            setItem(11, new MenuItem(Material.BOOK, "§6§lPortugese (Brazil) by ManolosCraft"));
            setItem(20, GUIUtils.displayBoolean(lang.equalsIgnoreCase("pt_BR"), lang));
            setItem(16, new MenuItem(Material.BOOK, new Text("mcm.gui.lang_settings.custom", lang).toString()));
            setItem(25, GUIUtils.displayString(MCManager.getConfiguration().getString("language"), lang));
        }

        @Override
        public void onClick(InventoryClickEvent e) {
            Player p = (Player) e.getWhoClicked();
            switch (e.getSlot()) {
                case 22:
                    Text.setPerUserLanguage(false);
                    MCManager.getConfiguration().set("language", "en_US");
                    MCManager.saveConfiguration();
                    new LanguageSettingsMenu("en_US").open(p);
                    break;
                case 19:
                    Text.addLanguage(YamlConfiguration.loadConfiguration(MCManager.getInstance().getTheDamnTextResource("texts/de_DE.yml")));
                    Text.setPerUserLanguage(true);
                    Text.setPlayerLanguageExecutor((Player n) -> "de_DE");
                    MCManager.getConfiguration().set("language", "de_DE");
                    MCManager.saveConfiguration();
                    new LanguageSettingsMenu("de_DE").open(p);
                    break;
                case 20:
                    Text.addLanguage(YamlConfiguration.loadConfiguration(MCManager.getInstance().getTheDamnTextResource("texts/pt_BR.yml")));
                    Text.setPerUserLanguage(true);
                    Text.setPlayerLanguageExecutor((Player n) -> "pt_BR");
                    MCManager.getConfiguration().set("language", "pt_BR");
                    MCManager.saveConfiguration();
                    new LanguageSettingsMenu("pt_BR").open(p);
                    break;
                case 25:
                    p.sendMessage(MCManager.getPrefix() + "§cThis is unfortunatly currently impossible in this interface.");
                    break;
                case 31:
                    new SettingsMenu(lang).open(p);
                    break;
            }
        }

    }

}
