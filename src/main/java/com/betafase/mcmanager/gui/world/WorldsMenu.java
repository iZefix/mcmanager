/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.gui.world;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.BlockColor;
import com.betafase.mcmanager.api.NotificationPrompt;
import com.betafase.mcmanager.api.SafetyPrompt;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.gui.MainMenu;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.api.RawMenuItem;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import com.betafase.mcmanager.utils.Utils;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import com.betafase.mcmanager.api.SignInputHandler;
import com.betafase.mcmanager.gui.plugin.MainPluginMenu;
import com.betafase.mcmanager.gui.plugin.PluginMenu;
import org.apache.commons.io.FilenameUtils;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author iZefix
 */
public class WorldsMenu extends Menu {

    private int page;

    public WorldsMenu(String lang, int page) {
        super(new Text("mcm.gui.worlds.title", lang).toString(), 36, lang);
        this.page = page;
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        setItem(31, GUIUtils.back(lang));
        if (page != 0) {
            setItem(30, GUIUtils.previous_page(lang, page));
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                MenuItem search = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.BROWN, new Text("mcm.gui.worlds.search", lang).toString());
                setItem(27, search);
                MenuItem add = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.LIME, new Text("mcm.gui.worlds.add", lang).toString());
                setItem(28, add);
                int counter = -page * 14;
                for (World w : Bukkit.getWorlds()) {
                    if (counter < 0) {
                        counter++;
                    } else if (counter == 14) {
                        setItem(32, GUIUtils.next_page(lang, page + 2));
                        break;
                    } else {
                        Material mat;
                        switch (w.getEnvironment()) {
                            case NETHER:
                                mat = Material.NETHERRACK;
                                break;
                            case THE_END:
                                mat = Material.ENDER_STONE;
                                break;
                            default:
                                mat = Material.GRASS;
                                break;
                        }
                        MenuItem m = new MenuItem(mat);
                        ItemMeta im = m.getItemMeta();
                        im.setDisplayName("§9§l" + w.getName());
                        List<String> lore = new LinkedList<>();
                        //lore.add("§8UUID: §6" + w.getUID());
                        //lore.add("§8Seed: §6" + w.getSeed());
                        lore.add("§8Players: §6" + w.getPlayers().size());
                        lore.add("§8Entities: §6" + w.getEntities().size());
                        lore.add("§8PVP: §6" + w.getPVP());
                        lore.add("§8Difficulty: §6" + w.getDifficulty());
                        lore.add(" ");
                        lore.add(new Text("mcm.gui.worlds.manage", lang).toString());
                        im.setLore(lore);
                        m.setItemMeta(im);
                        setItem(GUIUtils.convertCounterSlot(counter), m);
                        counter++;
                    }
                }
                File worldFolder = new File(FilenameUtils.getFullPath(Bukkit.getWorldContainer().getAbsolutePath()));
                for (File f : worldFolder.listFiles((File pathname) -> pathname.isDirectory() && pathname.list((File dir, String name) -> name.equalsIgnoreCase("level.dat")).length > 0)) {
                    if (counter < 0) {
                        counter++;
                    } else if (counter == 14) {
                        setItem(32, GUIUtils.next_page(lang, page + 2));
                        break;
                    } else if (Bukkit.getWorld(f.getName()) == null) {
                        MenuItem folder = new MenuItem(Material.CHEST);
                        ItemMeta fMeta = folder.getItemMeta();
                        fMeta.setDisplayName("§9§l" + f.getName());
                        List<String> lore = new Text("mcm.gui.worlds.unloaded", lang).wordWrap(40, ChatColor.GRAY);
                        lore.add(" ");
                        lore.add(new Text("mcm.gui.worlds.load", lang).toString());
                        //Add more info here
                        fMeta.setLore(lore);
                        folder.setItemMeta(fMeta);
                        setItem(GUIUtils.convertCounterSlot(counter), folder);
                        counter++;
                    }
                }
            }
        }.runTaskAsynchronously(MCManager.getInstance());
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        int converted = GUIUtils.convertInventorySlot(e.getSlot());
        if (converted != -1) {
            if (e.getCurrentItem().getType() != Material.AIR) {
                if (e.getCurrentItem().getType() != null) {
                    switch (e.getCurrentItem().getType()) {
                        case CHEST:
                            new WorldCreateMenu(new WorldCreator(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())), lang).open(p);
                            break;
                        default:
                            new WorldMenu(Bukkit.getWorlds().get(converted), lang).open(p);
                            break;
                    }
                }
            }
        } else {
            switch (e.getSlot()) {
                case 27:
                    e.getView().close();

                    new NotificationPrompt(e.getInventory().getTitle(), NotificationPrompt.Type.ERROR, "Not supported, yet.", lang) {
                        @Override
                        public void onBack(Player p, String lang) {
                            WorldsMenu.this.open(p);
                        }

                        @Override
                        public void onContinue(Player p, String lang) {
                            WorldsMenu.this.open(p);
                        }
                    }.open(p);
                    break;
                case 28:
                    e.getView().close();
                    TextComponent create = new TextComponent(new Text("mcm.gui.createworld.prepare", lang).toString());
                    create.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/createworld <name> [args]"));
                    p.spigot().sendMessage(new TextComponent(MCManager.getPrefix()), create);
                    break;
                case 31:
                    new MainMenu(p, lang).open(p);
                    break;
                case 32:
                    if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                        new WorldsMenu(lang, page + 1).open(p);
                    }
                    break;
                case 30:
                    if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                        new WorldsMenu(lang, page - 1).open(p);
                    }
                    break;
            }
        }
    }

    private static class WorldMenu extends Menu {

        private World w;

        public WorldMenu(World w, String lang) {
            super(new Text("mcm.gui.world.title", lang, w.getName()).toString(), 36, lang);
            this.w = w;
            MenuItem black = GUIUtils.black();
            for (int i = 0; i < 9; i++) {
                setItem(i, black);
                setItem(27 + i, black);
            }
            setItem(9, black);
            setItem(18, black);
            setItem(17, black);
            setItem(26, black);
            setItem(31, GUIUtils.back(lang));

            Material mat;
            switch (w.getEnvironment()) {
                case NETHER:
                    mat = Material.NETHERRACK;
                    break;
                case THE_END:
                    mat = Material.ENDER_STONE;
                    break;
                default:
                    mat = Material.GRASS;
                    break;
            }
            MenuItem m = new MenuItem(mat);
            ItemMeta im = m.getItemMeta();
            im.setDisplayName("§9§l" + w.getName());
            List<String> lore = new LinkedList<>();
            lore.add("§8UUID: §6" + w.getUID());
            lore.add("§8Seed: §6" + w.getSeed());
            lore.add("§8Players: §6" + w.getPlayers().size());
            lore.add("§8Entities: §6" + w.getEntities().size());
            lore.add("§8Loaded Chunks: §6" + w.getLoadedChunks().length);
            im.setLore(lore);
            m.setItemMeta(im);
            this.setItem(4, m);
            MenuItem teleport = new MenuItem(Material.ENDER_PEARL, new Text("mcm.gui.world.teleport", lang).toString());
            this.setItem(GUIUtils.convertCounterSlot(3), teleport);
            updateDifficulty(w.getDifficulty()); //slot 1
            updateTime(w.getTime()); //slot 5
            updateBoolean(GUIUtils.convertCounterSlot(7), new Text("mcm.gui.world.pvp", lang).toString(), w.getPVP());
            updateBoolean(GUIUtils.convertCounterSlot(8), new Text("mcm.gui.world.weather", lang).toString(), w.getPVP());
            this.setItem(GUIUtils.convertCounterSlot(9), new MenuItem(Material.BOOK, new Text("mcm.gui.world.gamerules", lang).toString()));
            this.setItem(GUIUtils.convertCounterSlot(11), new MenuItem(Material.BARRIER, new Text("mcm.gui.world.unload", lang).toString(), new Text("mcm.gui.world.unload_warning", lang).toString()));
            this.setItem(GUIUtils.convertCounterSlot(13), new MenuItem(Material.LAVA_BUCKET, new Text("mcm.gui.world.delete", lang)));
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (getInventory().getViewers().isEmpty()) {
                        this.cancel();
                    } else {
                        updateTime(w.getTime());
                    }
                }
            }.runTaskTimer(MCManager.getInstance(), 10L, 10L);
        }

        public void updateBoolean(int slot, String name, boolean newB) {
            //REDSTONE / EMERALD
            Material mat;
            String prefix;
            if (newB) {
                mat = Material.EMERALD_BLOCK;
                prefix = "§a";
            } else {
                mat = Material.REDSTONE_BLOCK;
                prefix = "§c";
            }
            this.setItem(slot, new MenuItem(mat, prefix + name, "§7Value: §6" + newB));
        }

        public void updateDifficulty(Difficulty newD) {
            short id;
            String prefix;
            switch (newD) {
                case PEACEFUL:
                    //BLUE
                    id = 3;
                    prefix = "§9";
                    break;
                case EASY:
                    //GREEN
                    id = 5;
                    prefix = "§a";
                    break;
                case NORMAL:
                    //YELLOW
                    id = 4;
                    prefix = "§e";
                    break;
                case HARD:
                    //RED
                    id = 6;
                    prefix = "§c";
                    break;
                default:
                    //BLACK
                    prefix = "§0";
                    id = 15;
                    break;
            }
            MenuItem item = new MenuItem(Material.STAINED_CLAY, 1, id, prefix + StringUtils.capitalize(newD.toString().toLowerCase()));
            this.setItem(GUIUtils.convertCounterSlot(1), item);
        }

        public void updateTime(long time) {
            int hours = (int) Math.floor(time / 1000);
            hours += 6;
            if (hours > 23) {
                hours = hours % 24;
            }
            int minutes = (int) ((time % 1000) / 1000.0 * 60);
            MenuItem item = new MenuItem(Material.WATCH, "§6" + (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes), "§8" + time);
            this.setItem(GUIUtils.convertCounterSlot(5), item);
        }

        @Override
        public void onClick(InventoryClickEvent e) {
            Player p = (Player) e.getWhoClicked();
            int converted = GUIUtils.convertInventorySlot(e.getSlot());
            if (converted != -1) {
                switch (converted) {
                    case 1:
                        switch (w.getDifficulty()) {
                            case PEACEFUL:
                                w.setDifficulty(Difficulty.EASY);
                                break;
                            case EASY:
                                w.setDifficulty(Difficulty.NORMAL);
                                break;
                            case NORMAL:
                                w.setDifficulty(Difficulty.HARD);
                                break;
                            default:
                                w.setDifficulty(Difficulty.PEACEFUL);
                                break;
                        }
                        updateDifficulty(w.getDifficulty());
                        break;
                    case 3:
                        e.getView().close();
                        p.teleport(w.getSpawnLocation());
                        break;
                    case 5:
                        if (w.getTime() <= 23000) {
                            w.setTime(w.getTime() + 1000 - w.getTime() % 1000);
                        } else {
                            w.setTime(0);
                        }
                        updateTime(w.getTime());
                        break;
                    case 7:
                        w.setPVP(!w.getPVP());
                        updateBoolean(e.getSlot(), new Text("mcm.gui.world.pvp", lang).toString(), w.getPVP());
                        break;
                    case 9:
                        new GameruleMenu(w, lang).open(p);
                        break;
                    case 11:
                        if (ModuleManager.isValid(p, "world.unload")) {
                            if (!w.getPlayers().isEmpty()) {
                                p.sendMessage(MCManager.getPrefix() + new Text("mcm.gui.world.unload_error", lang));
                            } else {
                                e.getView().close();
                                p.sendMessage(MCManager.getPrefix() + new Text("mcm.gui.world.start_unload", lang));
                                try {
                                    Utils.unloadWorld(w);
                                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.gui.world.unloaded", lang));
                                    new WorldsMenu(lang, 0).open(p);
                                } catch (Exception ex) {
                                    MCManager.getLog().log(Level.SEVERE, "Failed to unload world", ex);
                                    p.sendMessage(Text.reportError(ex, lang));
                                }
                            }
                        } else {
                            GUIUtils.showNoAccess(e, lang);
                        }
                        break;
                    case 13:
                        if (ModuleManager.isValid(p, "world.delete")) {
                            e.getView().close();
                            if (!w.getPlayers().isEmpty()) {
                                p.sendMessage(MCManager.getPrefix() + new Text("mcm.gui.world.unload_error", lang));
                            } else {
                                new SafetyPrompt(new Text("mcm.gui.world.delete_check", lang).toString(), lang) {
                                    @Override
                                    public void onApprove(boolean approve) {
                                        if (approve) {
                                            p.sendMessage(MCManager.getPrefix() + new Text("mcm.gui.world.start_delete", lang));
                                            try {
                                                Utils.unloadWorld(w);
                                                FileUtils.deleteDirectory(w.getWorldFolder());
                                                p.sendMessage(MCManager.getPrefix() + new Text("mcm.gui.world.deleted", lang));
                                                new WorldsMenu(lang, 0).open(p);
                                            } catch (Exception ex) {
                                                MCManager.getLog().log(Level.SEVERE, "Failed to unload world", ex);
                                                p.sendMessage(Text.reportError(ex, lang));
                                            }
                                        } else {
                                            this.open(p);
                                        }
                                    }
                                }.open(p);
                            }
                        } else {
                            GUIUtils.showNoAccess(e, lang);
                        }
                        break;
                }
            } else {
                switch (e.getSlot()) {
                    case 31:
                        new WorldsMenu(lang, 0).open(p);
                        break;
                }
            }
        }

        private static class GameruleMenu extends Menu {

            World w;
            int page;

            public GameruleMenu(World w, String lang) {
                this(w, lang, 0);
            }

            private GameruleMenu(World w, String lang, int page) {
                super(new Text("mcm.gui.gamerule.title", lang, w.getName()).toString(), 36, lang);
                this.w = w;
                this.page = page;
                MenuItem black = GUIUtils.black();
                for (int i = 0; i < 9; i++) {
                    setItem(i, black);
                    setItem(27 + i, black);
                }
                setItem(9, black);
                setItem(18, black);
                setItem(17, black);
                setItem(26, black);
                this.setItem(31, GUIUtils.back(lang));
                int counter = -page * 14;
                if (page > 0) {
                    setItem(30, GUIUtils.previous_page(lang, page));
                }
                for (String s : w.getGameRules()) {
                    if (counter < 0) {

                    } else if (counter == 14) {
                        this.setItem(32, GUIUtils.next_page(lang, page + 2));
                        break;
                    } else {
                        String value = w.getGameRuleValue(s);
                        String type;
                        try {
                            //IS INTEGER
                            int i = Integer.parseInt(value);
                            type = new Text("mcm.data.int", lang).toString();
                        } catch (Exception ex) {
                            if (value.equalsIgnoreCase("true") || value.equalsIgnoreCase("false")) {
                                //IS BOOLEAN
                                type = new Text("mcm.data.boolean", lang).toString();
                            } else {
                                //IS STRING
                                type = new Text("mcm.data.string", lang).toString();
                            }
                        }
                        String[] lore = new String[]{
                            new Text("mcm.data.type", lang, type).toString(),
                            new Text("mcm.data.value", lang, value).toString(),
                            " ",
                            new Text("mcm.data.change", lang).toString()
                        };
                        MenuItem rule = new MenuItem(Material.BOOK, "§9§l" + s, lore);
                        this.setItem(GUIUtils.convertCounterSlot(counter), rule);
                    }
                    counter++;
                }
            }

            @Override
            public void onClick(InventoryClickEvent e) {
                Player p = (Player) e.getWhoClicked();
                int converted = GUIUtils.convertInventorySlot(e.getSlot());
                if (converted != -1) {
                    int slot = page * 14 + converted;
                    if (slot >= w.getGameRules().length) {
                        return;
                    }
                    String gr = w.getGameRules()[slot];
                    //String value = w.getGameRuleValue(gr);
                    if (!MCManager.requestInput(p, new SignInputHandler() {
                        @Override
                        public void handleTextInput(String[] lines) {
                            if (lines[0].isEmpty()) {
                                new NotificationPrompt(e.getInventory().getTitle(), NotificationPrompt.Type.ERROR, "You must fill in the first line of the sign. Nothing was changed.", lang) {
                                    @Override
                                    public void onBack(Player p, String lang) {
                                        GameruleMenu.this.open(p);
                                    }

                                    @Override
                                    public void onContinue(Player p, String lang) {
                                        onBack(p, lang);
                                    }
                                }.open(p);
                            } else {
                                w.setGameRuleValue(gr, lines[0]);
                                new GameruleMenu(w, lang, page).open(p);
                            }
                        }

                        @Override
                        public String[] getDefault() {
                            return null;
                        }
                    })) {
                        p.sendMessage(MCManager.getPrefix() + "§cProtocolLib is not installed. Please install it for text input.");
                    }
                } else if (e.getSlot() == 31) {
                    new WorldMenu(w, lang).open(p);
                } else if (e.getSlot() == 30 && e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new GameruleMenu(w, lang, page - 1).open(p);
                } else if (e.getSlot() == 32 && e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new GameruleMenu(w, lang, page + 1).open(p);
                }
            }

        }

    }

    @Override
    public final void setItem(int slot, RawMenuItem stack) {
        super.setItem(slot, stack); //To change body of generated methods, choose Tools | Templates.
    }

}
