/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.gui.world;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.utils.Text;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author dbrosch
 */
public class WorldCreateMenu extends Menu {

    WorldCreator generator;
    public static boolean smartload = false;

    public WorldCreateMenu(WorldCreator toPass, String lang) {
        super(new Text("mcm.gui.createworld.title", lang).toString(), 9, lang);
        this.generator = toPass;
        if (generator == null) {
            generator = new WorldCreator("world" + Bukkit.getWorlds().size());
        }
        File newWorldFile = new File(Bukkit.getWorldContainer(), generator.name());
        if (!newWorldFile.exists()) {
            calculateGenerator();
        } else {
            this.setItem(4, new MenuItem(Material.BEDROCK, new Text("mcm.gui.createworld.exists_t", lang).toString(), new Text("mcm.gui.createworld.exists_d", lang).wordWrapAsArray(40, ChatColor.GRAY)));
        }
        this.setItem(0, getWorldItem(generator));
        this.setItem(8, new MenuItem(Material.STAINED_CLAY, 1, (short) 5, new Text("mcm.gui.createworld.create", lang).toString()));
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        switch (e.getSlot()) {
            case 8:
                e.getView().close();
                p.sendMessage(MCManager.getPrefix() + new Text("mcm.gui.createworld.success", lang));
                if (!smartload) {
                    smartload = true;
                    generator.createWorld();
                    smartload = false;
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.gui.createworld.complete", lang));
                } else {
                    ((Player) e.getWhoClicked()).sendMessage(MCManager.getPrefix() + "§cOops! It seems like someone else is currently loading a SmartLoad™ World. Please try again later.");
                }
                break;
        }
    }

    private void calculateGenerator() {
        switch (generator.type()) {
            default:
                this.setItem(4, new MenuItem(Material.BEDROCK, new Text("mcm.gui.createworld.unsupported_t", lang).toString(), new Text("mcm.gui.createworld.unsupported_d", lang, generator.type().name()).wordWrapAsArray(40, ChatColor.GRAY)));
                break;
        }
    }

    private MenuItem getWorldItem(WorldCreator c) {
        MenuItem item = new MenuItem(Material.GRASS);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName("§9§l" + c.name());
        List<String> lore = new ArrayList<>(6);
        switch (c.environment()) {
            case NETHER:
                item.setType(Material.NETHERRACK);
                break;
            case THE_END:
                item.setType(Material.ENDER_STONE);
                break;
        }
        lore.add(new Text("mcm.gui.createworld.i_env", lang, c.environment().toString()).toString());
        lore.add(new Text("mcm.gui.createworld.i_seed", lang, c.seed()).toString());
        if (generator.generator() == null) {
            lore.add(new Text("mcm.gui.createworld.i_type", lang, c.type().getName()).toString());
            lore.add(new Text("mcm.gui.createworld.i_gs", lang, c.generatorSettings()).toString());
        } else {
            lore.add(new Text("mcm.gui.createworld.i_gc", lang).toString());
        }
        meta.setLore(lore);
        item.setItemMeta(meta);
        return item;
    }

    @Override
    public void onClose(InventoryCloseEvent e) {
    }

}
