/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.gui;

import com.betafase.mcmanager.api.RawMenuItem;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.gui.plugin.MainPluginMenu;
import com.betafase.mcmanager.MCManager;
import static com.betafase.mcmanager.MCManager.getPluginHooks;
import com.betafase.mcmanager.api.BlockColor;
import com.betafase.mcmanager.api.ExecutableMenuItem;
import com.betafase.mcmanager.api.PluginHook;
import com.betafase.mcmanager.api.SignInputHandler;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.SpigotUpdater;
import com.betafase.mcmanager.utils.Text;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.ChatPaginator;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class MainMenu extends Menu {

    private static int builds_behind = 0;
    private static long last_check = 0;

    private int convertConfigSlot(int config) {
        int slot = config % 7;
        int row = 1 + (config - slot) / 7;
        return row * 10 + slot;
    }

    private int convertMenuSlot(int menu) {
        if (menu >= 9 && menu < 27 && menu % 9 < 8 && menu % 9 > 0) {
            int slot = menu % 9 - 1;
            if (slot < 0 || slot > 7) {
                return -1;
            }
            int row = ((menu - slot + 1) / 9) - 1;
            return row * 6 + slot;
        }
        return -1;
    }

    public MainMenu(Player p, String lang) {
        super(new Text("mcm.gui.main.title", lang).toString(), 36, lang);

        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);

        MenuItem info = new MenuItem(Material.NETHER_STAR, new Text("mcm.gui.main.overview", lang).toString());
        this.setItem(4, info);
        for (Entry<Integer, ExecutableMenuItem> en : MCManager.getMenuDesign().entrySet()) {
            setItem(convertConfigSlot(en.getKey()), en.getValue().getDisplayItem(lang));
        }
        if (ModuleManager.isValid("spigot_update")) {
            this.setItem(27, GUIUtils.loading(lang));
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (System.currentTimeMillis() - last_check > 2160000) {
                        last_check = System.currentTimeMillis();
                        SpigotUpdater s = new SpigotUpdater();
                        builds_behind = s.buildsBehind();
                    }
                    MenuItem spigot;
                    if (SpigotUpdater.isUpdating()) {
                        spigot = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.RED, new Text("mcm.gui.main.spigot_update", lang).toString());
                        ItemMeta meta = spigot.getItemMeta();
                        List<String> lore = new LinkedList<>();
                        lore.add(new Text("mcm.gui.main.spigot_active_d1", lang, builds_behind).toString());
                        //lore.add(" ");
                        //lore.add(new Text("mcm.gui.main.spigot_active_a", lang).toString());
                        meta.setLore(lore);
                        spigot.setItemMeta(meta);
                    } else if (builds_behind == 0) {
                        spigot = new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 15, new Text("mcm.gui.main.spigot_no_update", lang).toString());
                        ItemMeta meta = spigot.getItemMeta();
                        List<String> lore = new LinkedList<>();
                        //lore.add(new Text("mcm.gui.main.spigot_update_d1", lang, builds_behind).toString());
                        lore.addAll(new Text("mcm.gui.main.spigot_update_d", lang).wordWrap(40, ChatColor.GRAY));
                        lore.add(" ");
                        lore.add(new Text("mcm.gui.main.spigot_update_a", lang).toString());
                        meta.setLore(lore);
                        spigot.setItemMeta(meta);
                    } else if (builds_behind > 0) {
                        spigot = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.ORANGE, new Text("mcm.gui.main.spigot_update", lang).toString());
                        ItemMeta meta = spigot.getItemMeta();
                        List<String> lore = new LinkedList<>();
                        lore.add(new Text("mcm.gui.main.spigot_update_d1", lang, builds_behind).toString());
                        lore.addAll(new Text("mcm.gui.main.spigot_update_d", lang).wordWrap(40, ChatColor.GRAY));
                        lore.add(" ");
                        lore.add(new Text("mcm.gui.main.spigot_update_a", lang).toString());
                        meta.setLore(lore);
                        spigot.setItemMeta(meta);
                    } else if (builds_behind == -1) {
                        spigot = new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 14, "§cFailed to load Spigot update information!");
                        ItemMeta meta = spigot.getItemMeta();
                        List<String> lore = new LinkedList<>();
                        //lore.add(new Text("mcm.gui.main.spigot_update_d1", lang, builds_behind).toString());
                        lore.addAll(new Text("mcm.gui.main.spigot_update_d", lang).wordWrap(40, ChatColor.GRAY));
                        lore.add(" ");
                        lore.add(new Text("mcm.gui.main.spigot_update_a", lang).toString());
                        meta.setLore(lore);
                        spigot.setItemMeta(meta);
                    } else {
                        spigot = new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 4, new Text("mcm.gui.main.spigot_custom", lang).toString());
                        ItemMeta meta = spigot.getItemMeta();
                        List<String> lore = new LinkedList<>();
                        //lore.add(new Text("mcm.gui.main.spigot_update_d1", lang, builds_behind).toString());
                        lore.addAll(new Text("mcm.gui.main.spigot_update_d", lang).wordWrap(40, ChatColor.GRAY));
                        lore.add(" ");
                        lore.add(new Text("mcm.gui.main.spigot_update_a", lang).toString());
                        meta.setLore(lore);
                        spigot.setItemMeta(meta);
                    }
                    setItem(27, spigot);
                }
            }.runTaskAsynchronously(MCManager.getInstance());
        } else {
            setItem(27, new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 14, "§cSpigotUpdater has been blocked by the system."));
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                if (ModuleManager.isValid("plugin_updater") && System.currentTimeMillis() - last_check > 1200000) {
                    last_check = System.currentTimeMillis();
                    MCManager.getUpdates().clear();
                    for (PluginHook h : getPluginHooks()) {
                        if (!MCManager.getUpdates().contains(h.getPlugin().getName()) && h.hasUpdateChecker()) {
                            if (h.getUpdateChecker().checkUpdate() != null) {
                                MCManager.getUpdates().add(h.getPlugin().getName());
                            }
                        }
                    }
                }
                if (MCManager.getUpdates() != null && !MCManager.getUpdates().isEmpty()) {
                    String[] lore = MCManager.getUpdates().toArray(new String[MCManager.getUpdates().size()]);
                    for (int i = 0; i < lore.length; i++) {
                        lore[i] = "§7- " + ChatColor.stripColor(lore[i]);
                    }
                    lore = Arrays.copyOf(lore, lore.length + 2);
                    lore[lore.length - 2] = " ";
                    lore[lore.length - 1] = new Text("mcm.gui.main.pu_a", lang).toString();
                    MenuItem plugin_updates = new MenuItem(Material.STAINED_GLASS_PANE, MCManager.getUpdates().size(), BlockColor.ORANGE.toShort(), new Text("mcm.gui.main.pu_title", lang).toString(), lore);
                    setItem(28, plugin_updates);
                }
            }
        }.runTaskAsynchronously(MCManager.getInstance());

        MenuItem error = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.BLUE, new Text("mcm.gui.main.settings", lang).toString());
        this.setItem(35, error);

        this.setItem(34, new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.CYAN, new Text("mcm.gui.main.error", lang).toString()));

        MenuItem close = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.RED, new Text("mcm.gui.main.close", lang).toString());
        setItem(31, close);
    }

    @Override
    public final void setItem(int slot, RawMenuItem stack) {
        super.setItem(slot, stack);
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        int slot = e.getSlot();
        final Player p = (Player) e.getWhoClicked();
        switch (slot) {
            case 4:
                new InfoMenu(lang).open(p);
                break;
            case 27:
                if (e.getCurrentItem().getDurability() == BlockColor.ORANGE.toShort() || e.getCurrentItem().getDurability() == BlockColor.BLACK.toShort()) {
                    if (ModuleManager.isValid(p, "spigot_update")) {
                        if (!SpigotUpdater.isUpdating()) {
                            e.getView().close();
                            if (e.getClick() == ClickType.SHIFT_LEFT || e.getClick() == ClickType.SHIFT_RIGHT) {
                                if (!MCManager.requestInput(p, new SignInputHandler() {
                                    @Override
                                    public void handleTextInput(String[] lines) {
                                        SpigotUpdater updater = new SpigotUpdater();
                                        Bukkit.getScheduler().runTaskAsynchronously(MCManager.getInstance(), () -> {
                                            updater.performUpdate(lines[0] + lines[1] + lines[2] + lines[3], p);
                                        });
                                    }

                                    @Override
                                    public String[] getDefault() {
                                        return null;
                                    }
                                })) {
                                    p.sendMessage(MCManager.getPrefix() + "§cProtocolLib is not installed!");
                                }
                            } else {
                                SpigotUpdater updater = new SpigotUpdater();
                                Bukkit.getScheduler().runTaskAsynchronously(MCManager.getInstance(), () -> {
                                    updater.performUpdate(null, p);
                                });
                            }
                        }
                    } else {
                        GUIUtils.showNoAccess(e, lang);
                    }
                }
                break;
            case 28:
                if (ModuleManager.isValid(p, "plugin")) {
                    List<Plugin> pl = new LinkedList<>();
                    for (Plugin plugin : Bukkit.getPluginManager().getPlugins()) {
                        if (MCManager.getUpdates().contains(plugin.getName())) {
                            pl.add(plugin);
                        }
                    }
                    if (!pl.isEmpty()) {
                        new MainPluginMenu(lang, 0, pl.toArray(new Plugin[pl.size()])).open(p);
                    }
                } else {
                    GUIUtils.showNoAccess(e, lang);
                }
                break;
            case 31:
                e.getView().close();
                break;
            case 35:
                new SettingsMenu(lang).open(p);
                break;
            case 34:
                e.getView().close();
                p.sendMessage("§9§lMCManager §8Support Page");
                String text = "If you enjoy the plugin, please consider donating via PayPal as it is offered for free and/or write a review on Spigot. If there is a bug or any other error, please create an issue on Bitbucket. If you need help, please refer to the wiki  (https://bitbucket.org/iZefix/mcmanager/wiki/Home) or create an issue.";
                for (String s : ChatPaginator.wordWrap(text, 55)) {
                    p.sendMessage("§7" + ChatColor.stripColor(s));
                }
                TextComponent donate = new TextComponent("§6§nDonate via PayPal\n");
                donate.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=N4YLZ42CGRP3E"));
                donate.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=N4YLZ42CGRP3E").create()));
                p.spigot().sendMessage(donate);
                TextComponent issue = new TextComponent("§c§nReport an issue on Bitbucket\n");
                issue.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://bitbucket.org/iZefix/mcmanager/issues/new"));
                issue.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7https://bitbucket.org/iZefix/mcmanager/issues/new").create()));
                p.spigot().sendMessage(issue);
                TextComponent spigot = new TextComponent("§e§nVisit Spigot Page\n");
                spigot.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "https://www.spigotmc.org/resources/mcmanager-ingame-server-management.7297/"));
                spigot.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7https://www.spigotmc.org/resources/mcmanager-ingame-server-management.7297/").create()));
                p.spigot().sendMessage(spigot);
                break;
            default:
                int cfg_slot = convertMenuSlot(slot);
                if (cfg_slot >= 0 && MCManager.getMenuDesign().containsKey(cfg_slot)) {
                    MCManager.getMenuDesign().get(cfg_slot).onClick(e);
                }
                break;
        }
    }
}
