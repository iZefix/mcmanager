/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.gui;

import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.SystemInfo;
import com.betafase.mcmanager.utils.Text;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author dbrosch
 */
public class InfoMenu extends Menu {

    public InfoMenu(String lang) {
        super(new Text("mcm.gui.info.title", lang, Bukkit.getServerName()).toString(), 36, lang);
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        this.setItem(31, GUIUtils.back(lang));

        //OperatingSystemMXBean bean = (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();
        SystemInfo info = new SystemInfo();

        MenuItem systemStatus = new MenuItem(Material.BREWING_STAND_ITEM);
        ItemMeta systemMeta = systemStatus.getItemMeta();
        systemMeta.setDisplayName(new Text("mcm.gui.info.system", lang).toString());
        LinkedList<String> lore = new LinkedList<>();
        lore.add(new Text("mcm.gui.info.system_info", lang, info.OsName() + " " + info.OsVersion() + " " + info.OsArch()).toString());
        lore.add(new Text("mcm.gui.info.server_info", lang, Bukkit.getBukkitVersion()).toString());
        lore.add(new Text("mcm.gui.info.ram_usage", lang, FileUtils.byteCountToDisplaySize(info.usedMem()) + "  / " + FileUtils.byteCountToDisplaySize(info.totalMem())).toString());
        lore.add(new Text("mcm.gui.info.disc_usage", lang, FileUtils.byteCountToDisplaySize(info.usedSpace()) + " / " + FileUtils.byteCountToDisplaySize(info.maxSpace())).toString());
        try {
            lore.add("§8Server: §6" + InetAddress.getLocalHost().getCanonicalHostName() + ":" + Bukkit.getPort());
            lore.add("§8WebServer: §6" + (MCManager.getWebServer().isOnline() ? InetAddress.getLocalHost().getCanonicalHostName() + ":" + MCManager.getWebServer().getPort() : "§cOffline"));
        } catch (UnknownHostException ex) {
            Logger.getLogger(InfoMenu.class.getName()).log(Level.SEVERE, null, ex);
        }
        systemMeta.setLore(lore);
        systemStatus.setItemMeta(systemMeta);
        this.setItem(4, systemStatus);

        MenuItem save = new MenuItem(Material.ENDER_CHEST, new Text("mcm.gui.info.save", lang).toString());
        this.setItem(19, save);

        MenuItem reload = new MenuItem(Material.BEDROCK, new Text("mcm.gui.main.reload", lang).toString());
        this.setItem(12, reload);

        if (ModuleManager.isValid("stop")) {
            MenuItem restart = new MenuItem(Material.STAINED_CLAY, 1, (short) 4, new Text("mcm.gui.info.restart", lang).toString());
            this.setItem(14, restart);
            MenuItem stop = new MenuItem(Material.STAINED_CLAY, 1, (short) 6, new Text("mcm.gui.info.stop", lang).toString());
            this.setItem(25, stop);
        }

    }

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        switch (e.getSlot()) {
            case 19:
                p.performCommand("save-all");
                break;
            case 12:
                e.getView().close();
                Bukkit.reload();
                p.performCommand("manager");
                break;
            case 14:
                if (e.getCurrentItem().getType() == Material.STAINED_CLAY) {
                    if (ModuleManager.isValid(p, "stop")) {
                        e.getView().close();
                        Bukkit.spigot().restart();
                    } else {
                        GUIUtils.showNoAccess(e, lang);
                    }
                }
                break;
            case 25:
                if (e.getCurrentItem().getType() == Material.STAINED_CLAY) {
                    if (ModuleManager.isValid(p, "stop")) {
                        e.getView().close();
                        Bukkit.shutdown();
                    } else {
                        GUIUtils.showNoAccess(e, lang);
                    }
                }
                break;
            case 31:
                new MainMenu(p, lang).open(p);
                break;
        }
    }

}
