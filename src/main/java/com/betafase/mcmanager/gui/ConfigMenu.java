/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.gui;

import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.gui.plugin.PluginMenu;
import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.BlockColor;
import com.betafase.mcmanager.api.ConfigChangeEvent;
import com.betafase.mcmanager.utils.Text;
import java.util.LinkedList;
import java.util.List;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import com.betafase.mcmanager.api.SignInputHandler;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class ConfigMenu extends Menu {

    Plugin pl;
    ConfigurationSection current;
    int page;

    public ConfigMenu(Plugin pl, final ConfigurationSection current, final int page, String lang) {
        super(new Text("mcm.gui.config.title", lang, (current.getName().length() > 20 ? current.getName().substring(0, 20) + "..." : current.getName())).toString(), 36, lang);
        this.pl = pl;
        this.current = current;
        /*if (current == pl.getConfig()) {
            this.setItem(26, new MenuItem(Material.GOLDEN_APPLE, "§7§lDefaults"));
        }*/
        this.page = page;
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        this.setItem(31, GUIUtils.back(lang));
        if (page > 0) {
            this.setItem(30, new MenuItem(GUIUtils.previous_page(lang, page)));
        }
        Bukkit.getScheduler().runTaskAsynchronously(pl, () -> {
            int counter = -page * 14;
            for (String s : current.getKeys(false)) {
                if (counter < 0) {
                } else if (counter == 14) {
                    setItem(32, GUIUtils.next_page(lang, page + 2));
                    break;
                } else {
                    MenuItem item;
                    if (current.isConfigurationSection(s)) {
                        item = new MenuItem(Material.CHEST, "§6§l" + StringUtils.capitalize(s));
                    } else if (current.isBoolean(s)) {
                        boolean bln = current.getBoolean(s);
                        if (bln) {
                            item = new MenuItem(Material.EMERALD_BLOCK, "§a§l" + StringUtils.capitalize(s), "§7Value: §6" + bln);
                        } else {
                            item = new MenuItem(Material.REDSTONE_BLOCK, "§c§l" + StringUtils.capitalize(s), "§7Value: §6" + bln);

                        }
                    } else if (current.isInt(s)) {
                        int integer = current.getInt(s);
                        item = new MenuItem(Material.GOLD_BLOCK, "§e§l" + StringUtils.capitalize(s), "§7Value: §6" + integer);
                    } else if (current.isString(s)) {
                        String string = current.getString(s);
                        item = new MenuItem(Material.BOOKSHELF, "§e§l" + StringUtils.capitalize(s), "§7Value: §6" + string);
                    } else if (current.isList(s)) {
                        try {
                            List<?> list = current.getList(s);
                            item = new MenuItem(new ItemStack(Material.LADDER));
                            ItemMeta imeta = item.getItemMeta();
                            imeta.setDisplayName("§9§l" + StringUtils.capitalize(s));
                            LinkedList<String> lore = new LinkedList<>();
                            int c = 0;
                            for (Object o : list) {
                                if (c < 10) {
                                    lore.add("§7 - §6" + o.toString());
                                } else {
                                    lore.add("§7... and §6" + (list.size() - 10) + "§7 more");
                                    break;
                                }
                                c++;
                            }
                            imeta.setLore(lore);
                            item.setItemMeta(imeta);
                        } catch (Exception ex) {
                            item = new MenuItem(Material.BONE, "§c§lERROR");
                            ex.printStackTrace();
                        }
                    } else {
                        Object o = current.get(s);
                        item = new MenuItem(Material.PAPER, "§8§l" + StringUtils.capitalize(s), "§7Value: §6" + o.toString());
                    }
                    setItem(GUIUtils.convertCounterSlot(counter), item);
                }
                counter++;
            }
        });
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        int slot = e.getSlot();
        Player p = (Player) e.getWhoClicked();
        int conv = GUIUtils.convertInventorySlot(slot);
        if (conv != -1) {
            Material type = e.getCurrentItem().getType();
            final String s = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).toLowerCase();
            switch (type) {
                case AIR:
                    break;
                case REDSTONE_BLOCK:
                    boolean bln = true;
                    ConfigChangeEvent event = new ConfigChangeEvent(pl, current.getCurrentPath() + "." + s, bln);
                    Bukkit.getPluginManager().callEvent(event);
                    current.set(s, event.getValue());
                    pl.saveConfig();
                    MenuItem item = new MenuItem(Material.EMERALD_BLOCK, "§a§l" + StringUtils.capitalize(s), "§7Value: §6" + bln);
                    this.setItem(slot, item);
                    break;
                case EMERALD_BLOCK:
                    bln = false;
                    ConfigChangeEvent event2 = new ConfigChangeEvent(pl, current.getCurrentPath() + "." + s, bln);
                    Bukkit.getPluginManager().callEvent(event2);
                    current.set(s, event2.getValue());
                    pl.saveConfig();
                    item = new MenuItem(Material.REDSTONE_BLOCK, "§c§l" + StringUtils.capitalize(s), "§7Value: §6" + bln);
                    this.setItem(slot, item);
                    break;
                case GOLD_BLOCK:
                    e.getView().close();
                    if (!MCManager.requestInput(p, new SignInputHandler() {
                        @Override
                        public void handleTextInput(String[] lines) {
                            ConfigChangeEvent event = new ConfigChangeEvent(pl, current.getCurrentPath() + "." + s, lines[0] + lines[1] + lines[2] + lines[3]);
                            Bukkit.getPluginManager().callEvent(event);
                            current.set(s, event.getValue());
                            pl.saveConfig();
                            GUIUtils.openAsync(getInventory(), p);
                        }

                        @Override
                        public String[] getDefault() {
                            String[] array = new String[4];
                            array[0] = Integer.toString(current.getInt(s));
                            return array;
                        }
                    })) {
                        TextComponent t = new TextComponent("§aClick §nhere§r§a to change             ");
                        TextComponent back = new TextComponent("§c[BACK]");
                        t.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/config " + pl.getName() + " " + current.getCurrentPath() + (current.getCurrentPath().isEmpty() ? "" : ".") + s + " <value>"));
                        if (current.getParent() == null) {
                            back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/config " + pl.getName() + " home"));
                        } else {
                            back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/config " + pl.getName() + " " + current.getCurrentPath()));
                        }
                        p.spigot().sendMessage(t, back);
                    }
                    break;
                case BOOKSHELF:
                    e.getView().close();
                    TextComponent t = new TextComponent("§aClick §nhere§r§a to change             ");
                    TextComponent back = new TextComponent("§c[BACK]");
                    t.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/config " + pl.getName() + " " + current.getCurrentPath() + (current.getCurrentPath().isEmpty() ? "" : ".") + s + " <value>"
                    ));
                    if (current.getParent() == null) {
                        back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/config " + pl.getName() + " home"));
                    } else {
                        back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/config " + pl.getName() + " " + current.getCurrentPath()));
                    }
                    p.spigot().sendMessage(t, back);
                    break;
                case CHEST:
                    new ConfigMenu(pl, current.getConfigurationSection(s), 0, getLanguage()).open(p);
                    break;
                default:
                    p.sendMessage("§cThis item can't be edited yet. Create an issue if you want to change this §csetting here: §6https://bitbucket.org/iZefix/mcmanager/issues/new");
                    break;
            }
        } else if (slot == 30) {
            if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                new ConfigMenu(pl, current, page - 1, getLanguage()).open(p);
            }
        } else if (slot == 31) {
            if (current.getParent() != null) {
                new ConfigMenu(pl, current.getParent(), 0, getLanguage()).open(p);
            } else {
                new PluginMenu(pl, getLanguage()).open(p);
            }
        } else if (slot == 32) {
            if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                new ConfigMenu(pl, current, page + 1, getLanguage()).open(p);
            }
        }/* else if (slot == 26) {
            if (e.getCurrentItem().getType() == Material.GOLDEN_APPLE) {
                new ConfigMenu(pl, current.getRoot().getDefaults(), 0, lang).open(p);
            }
        }*/
    }

}
