/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.gui.player;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.SafetyPrompt;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.listener.DamageHandler;
import com.betafase.mcmanager.utils.Text;
import com.betafase.mcmanager.utils.Utils;
import java.util.LinkedList;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import com.betafase.mcmanager.api.SignInputHandler;
import org.bukkit.attribute.Attribute;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class PlayerMenu extends Menu {

    OfflinePlayer op;

    public PlayerMenu(OfflinePlayer p, String lang) {
        super(new Text("mcm.gui.player.title", lang, p.getName()).toString(), 36, lang);
        this.op = p;
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        this.setItem(31, GUIUtils.back(lang));
        new BukkitRunnable() {

            @Override
            public void run() {
                if (getInventory().getViewers().isEmpty()) {
                    this.cancel();
                } else {
                    refresh();
                }
            }
        }.runTaskTimerAsynchronously(MCManager.getInstance(), 5L, 20L);
    }

    public void refresh() {
        MenuItem m = new MenuItem(new ItemStack(Material.SKULL_ITEM, 1, (short) 3));
        SkullMeta meta = (SkullMeta) m.getItemMeta();
        meta.setOwner(op.getName());
        LinkedList<String> lore = new LinkedList<>();
        MenuItem ban;

//        MenuItem unknown = new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 15, "§8???");
        if (op.isBanned()) {
            ban = new MenuItem(Material.RED_ROSE, "§a§l" + new Text("mcm.gui.player.unban", lang).toString());
        } else {
            ban = new MenuItem(Material.BARRIER, "§c§l" + new Text("mcm.gui.player.ban", lang).toString());
        }
        if (op.isOnline()) {
            MenuItem food;
            MenuItem teleportHere;
            MenuItem teleportTo;
            MenuItem invulnerable;
            MenuItem fly;
            MenuItem spectate;
            MenuItem gamemode;
            MenuItem heal;
            Player o = op.getPlayer();
            meta.setDisplayName("§6§l" + o.getDisplayName());
            lore.add(new Text("mcm.gui.player.health2", getLanguage(), o.getHealth(), o.getMaxHealth()).toString());
            lore.add(new Text("mcm.gui.player.food", getLanguage(), o.getFoodLevel()).toString());
            lore.add(new Text("mcm.gui.player.ping", getLanguage(), Utils.getPing(o)).toString());
            lore.add(new Text("mcm.gui.player.ip", getLanguage(), o.getAddress().getAddress().getHostAddress()).toString());
            spectate = new MenuItem(Material.EYE_OF_ENDER, "§7§l" + new Text("mcm.gui.player.spectate", lang).toString());
            gamemode = new MenuItem(Material.BEACON, "§6§l" + new Text("mcm.gui.player.change_gamemode", lang),
                    new Text("mcm.gui.player.gamemode", lang, o.getGameMode().toString()).toString()
            );
            heal = new MenuItem(Material.POTION, "§c§l" + new Text("mcm.gui.player.heal", lang));
            PotionMeta healMeta = (PotionMeta) heal.getItemMeta();
            healMeta.addCustomEffect(new PotionEffect(PotionEffectType.REGENERATION, 10, 2), true);
            healMeta.setMainEffect(PotionEffectType.REGENERATION);
            heal.setItemMeta(healMeta);
            food = new MenuItem(Material.COOKED_BEEF, "§e§l" + new Text("mcm.gui.player.add_food", lang));
            teleportHere = new MenuItem(Material.ENDER_PEARL, "§5§l" + new Text("mcm.gui.player.teleport_here", lang));
            teleportTo = new MenuItem(Material.ENDER_PEARL, "§5§l" + new Text("mcm.gui.player.teleport_to", lang));
            if (o.getAllowFlight()) {
                fly = new MenuItem(Material.FEATHER, "§c§l" + new Text("mcm.gui.player.fly_off", lang));
            } else {
                fly = new MenuItem(Material.FEATHER, "§a§l" + new Text("mcm.gui.player.fly_on", lang));
            }
            if (DamageHandler.list.contains(op.getName())) {
                invulnerable = new MenuItem(Material.DIAMOND_SWORD, "§c§l" + new Text("mcm.gui.player.damage", lang));
            } else {
                invulnerable = new MenuItem(Material.BEDROCK, "§a§l" + new Text("mcm.gui.player.no_damage", lang));
            }
            this.setMenuItem(0, new MenuItem(Material.CHEST, new Text("mcm.gui.player.manage_inv", lang).toString()));
            this.setMenuItem(2, teleportTo);
            this.setMenuItem(3, spectate);
            this.setMenuItem(4, teleportHere);
            this.setMenuItem(10, gamemode);
            this.setMenuItem(9, heal);
            this.setMenuItem(11, food);

            this.setMenuItem(12, invulnerable);
            this.setMenuItem(8, fly);
        } else {
            meta.setDisplayName("§6§l" + op.getName());
            lore.add(new Text("mcm.gui.player.last_seen", lang, Utils.makeDateReadable(op.getLastPlayed())).toString());
        }
        String status;
        if (op.isBanned()) {
            status = "§cBANNED";
        } else if (op.isOp()) {
            status = "OPERATOR";
        } else {
            status = "PLAYER";
        }
        lore.add(new Text("mcm.gui.player.status", lang, status).toString());
        meta.setLore(lore);
        m.setItemMeta(meta);
        this.setItem(4, m);
        this.setMenuItem(6, ban);

    }

    @Override
    public void onClick(InventoryClickEvent e) {
        int slot = e.getSlot();
        Player p = (Player) e.getWhoClicked();
        int converted = GUIUtils.convertInventorySlot(slot);
        if (converted != -1) {
            switch (converted) {
                case 0:
                    if (op.isOnline()) {
                        new PlayerInventoryMenu(op.getPlayer(), lang).open(p);
                    }
                    break;
                case 2:
                    if (e.getCurrentItem().getType() == Material.ENDER_PEARL) {
                        e.getView().close();
                        p.teleport(op.getPlayer().getLocation());
                    }
                    break;
                case 3:
                    if (e.getCurrentItem().getType() == Material.EYE_OF_ENDER) {
                        e.getView().close();
                        p.setGameMode(GameMode.SPECTATOR);
                        p.setSpectatorTarget(op.getPlayer());
                    }
                    break;
                case 4:
                    if (e.getCurrentItem().getType() == Material.ENDER_PEARL) {
                        e.getView().close();
                        op.getPlayer().teleport(p.getLocation());
                    }
                    break;
                case 9:
                    if (e.getCurrentItem().getType() == Material.POTION) {
                        op.getPlayer().setHealth(op.getPlayer().getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
                    }
                    break;
                case 10:
                    if (e.getCurrentItem().getType() == Material.BEACON) {
                        Player o = op.getPlayer();
                        GameMode current = o.getGameMode();
                        switch (current) {
                            case SURVIVAL:
                                GameMode newMode = GameMode.CREATIVE;
                                op.getPlayer().setGameMode(newMode);
                                e.setCurrentItem(new MenuItem(Material.BEACON, "§6§l" + new Text("mcm.gui.player.change_gamemode", lang),
                                        new Text("mcm.gui.player.gamemode", lang, newMode.toString()).toString()
                                ));
                                break;
                            case CREATIVE:
                                newMode = GameMode.ADVENTURE;
                                op.getPlayer().setGameMode(newMode);
                                e.setCurrentItem(new MenuItem(Material.BEACON, "§6§l" + new Text("mcm.gui.player.change_gamemode", lang),
                                        new Text("mcm.gui.player.gamemode", lang, newMode.toString()).toString()
                                ));
                                break;
                            case ADVENTURE:
                                newMode = GameMode.SPECTATOR;
                                op.getPlayer().setGameMode(newMode);
                                e.setCurrentItem(new MenuItem(Material.BEACON, "§6§l" + new Text("mcm.gui.player.change_gamemode", lang),
                                        new Text("mcm.gui.player.gamemode", lang, newMode.toString()).toString()
                                ));
                                break;
                            case SPECTATOR:
                                newMode = GameMode.SURVIVAL;
                                op.getPlayer().setGameMode(newMode);
                                e.setCurrentItem(new MenuItem(Material.BEACON, "§6§l" + new Text("mcm.gui.player.change_gamemode", lang),
                                        new Text("mcm.gui.player.gamemode", lang, newMode.toString()).toString()
                                ));
                                break;
                        }
                    }
                    break;
                case 11:
                    if (e.getCurrentItem().getType() == Material.COOKED_BEEF) {
                        op.getPlayer().setFoodLevel(20);
                    }
                    break;
                case 12:
                    if (e.getCurrentItem().getType() == Material.BEDROCK) {
                        DamageHandler.list.add(op.getName());
                        e.setCurrentItem(new MenuItem(Material.DIAMOND_SWORD, "§c§l" + new Text("mcm.gui.player.damage", lang)));
                    } else {
                        DamageHandler.list.remove(op.getName());
                        e.setCurrentItem(new MenuItem(Material.BEDROCK, "§a§l" + new Text("mcm.gui.player.no_damage", lang)));
                    }
                    break;
                case 8:
                    if (e.getCurrentItem().getType() == Material.FEATHER) {
                        Player pl = p.getPlayer();
                        if (pl.getAllowFlight()) {
                            pl.setAllowFlight(false);
                            e.setCurrentItem(new MenuItem(Material.FEATHER, "§a§l" + new Text("mcm.gui.player.fly_on", lang)));
                        } else {
                            pl.setAllowFlight(true);
                            e.setCurrentItem(new MenuItem(Material.FEATHER, "§c§l" + new Text("mcm.gui.player.fly_off", lang)));
                        }
                    }
                    break;
                case 6:
                    if (e.getCurrentItem().getType() == Material.BARRIER) {
                        e.getView().close();
                        if (!MCManager.requestInput(p, new SignInputHandler() {
                            @Override
                            public void handleTextInput(String[] lines) {
                                new SafetyPrompt("Ban " + op.getName() + "?", lang) {
                                    @Override
                                    public void onApprove(boolean approve) {
                                        if (approve) {
                                            p.performCommand(MCManager.getConfiguration().getString("ban_command").replaceAll("%player%", op.getName()).replaceAll("%arg0%", lines[0]).replaceAll("%arg1%", lines[1]).replaceAll("%arg2%", lines[2]).replaceAll("%arg3%", lines[3]));
                                        } else {
                                            PlayerMenu.this.open(p);
                                        }
                                    }
                                }.open(p);
                            }

                            @Override
                            public String[] getDefault() {
                                return new String[]{"You are", "banned from", "the server", ""};
                            }
                        })) {
                            TextComponent proceedBan = new TextComponent(new Text("mcm.gui.player.proceed_ban", lang).toString());
                            proceedBan.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/ban " + op.getName() + " "));
                            TextComponent backBan = new TextComponent(new Text("mcm.gui.player.back_ban", lang).toString());
                            backBan.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/manager player " + op.getName()));
                            p.spigot().sendMessage(proceedBan, backBan);
                        }
                    } else {
                        p.performCommand("pardon " + op.getName());
                        refresh();
                    }
                    break;
            }
        } else {
            switch (slot) {
                case 31:
                    new MainPlayerMenu(getLanguage(), 0, !op.isOnline()).open(p);
                    break;
            }
        }
    }
}
