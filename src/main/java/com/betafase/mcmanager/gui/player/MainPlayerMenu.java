/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.gui.player;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.BlockColor;
import com.betafase.mcmanager.api.NotificationPrompt;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.gui.MainMenu;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.utils.Text;
import com.betafase.mcmanager.utils.Utils;
import java.util.LinkedList;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;
import com.betafase.mcmanager.api.SignInputHandler;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class MainPlayerMenu extends Menu {
    
    private final int page;
    private final boolean offline;
    private OfflinePlayer[] loader = null;
    
    public MainPlayerMenu(String lang, int page, boolean offline) {
        this(lang, page, offline, () -> offline ? Bukkit.getOfflinePlayers() : Bukkit.getOnlinePlayers().toArray(new OfflinePlayer[Bukkit.getOnlinePlayers().size()]));
    }
    
    private interface PlayerLoader {
        
        OfflinePlayer[] loadPlayers();
    }
    
    private MainPlayerMenu(String lang, int pg, boolean offl, PlayerLoader ld) {
        super(new Text(offl ? "mcm.gui.players.title2" : "mcm.gui.players.title", lang).toString(), 36, lang);
        this.page = pg;
        this.offline = offl;
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        if (page > 0) {
            this.setItem(30, GUIUtils.previous_page(lang, page));
        }
        this.setItem(31, GUIUtils.back(lang));
        MenuItem change;
        if (offline) {
            change = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.LIME, new Text("mcm.gui.players.online", lang).toString());
        } else {
            change = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.RED, new Text("mcm.gui.players.offline", lang).toString());
        }
        this.setItem(28, change);
        MenuItem search = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.BROWN, new Text("mcm.gui.players.search", lang).toString());
        setItem(27, search);
        new BukkitRunnable() {
            
            @Override
            public void run() {
                if (!getInventory().getViewers().isEmpty()) {
                    if (loader == null) {
                        loader = ld.loadPlayers();
                    }
                    refresh();
                } else {
                    this.cancel();
                }
            }
        }.runTaskTimerAsynchronously(MCManager.getInstance(), 5L, 20L);
    }
    
    public void refresh() {
        int counter = -page * 14;
        //Collection<? extends OfflinePlayer> list = Bukkit.getOnlinePlayers();
        for (OfflinePlayer a : loader) {
            if (counter < 0) {
            } else if (counter == 14) {
                this.setItem(32, GUIUtils.next_page(lang, page + 2));
                break;
            } else if (a.isOnline()) {
                Player o = (Player) a;
                MenuItem player = new MenuItem(Material.SKULL_ITEM, 1, (short) 3, "§a§l" + o.getDisplayName(),
                        new Text("mcm.gui.player.health", getLanguage(), Utils.makeHealthReadable(o.getHealth())).toString(),
                        new Text("mcm.gui.player.food", getLanguage(), o.getFoodLevel()).toString(),
                        new Text("mcm.gui.player.gamemode", getLanguage(), o.getGameMode().toString()).toString(),
                        new Text("mcm.gui.player.ping", getLanguage(), Utils.getPing(o)).toString()
                );
                SkullMeta meta = (SkullMeta) player.getItemMeta();
                meta.setOwner(a.getName());
                player.setItemMeta(meta);
                this.setItem(GUIUtils.convertCounterSlot(counter), player);
            } else {
                MenuItem player = new MenuItem(Material.SKULL_ITEM, 1, (short) 0, (a.isOnline() ? "§a§l" : "§c§l") + a.getName(),
                        "§7Last seen: §6" + Utils.makeDateReadable(a.getLastPlayed()),
                        new Text("mcm.gui.player.status", lang, (a.isBanned() ? "§cBANNED" : (a.isOp() ? "§6OPERATOR" : "§6PLAYER"))).toString()
                );
                SkullMeta meta = (SkullMeta) player.getItemMeta();
                meta.setOwner(a.getName());
                player.setItemMeta(meta);
                this.setItem(GUIUtils.convertCounterSlot(counter), player);
            }
            counter++;
        }
        int size = Bukkit.getOnlinePlayers().size();
        MenuItem info = new MenuItem(Material.SKULL_ITEM, (size < 65 ? size : 1), (short) 3, new Text("mcm.gui.players.online_size", lang, size).toString());
        this.setItem(4, info);
    }
    
    @Override
    public void onClick(InventoryClickEvent e) {
        int slot = e.getSlot();
        Player p = (Player) e.getWhoClicked();
        switch (slot) {
            case 31:
                new MainMenu(p, getLanguage()).open(p);
                break;
            case 28:
                new MainPlayerMenu(lang, page, !offline).open(p);
                break;
            case 27:
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (!MCManager.requestInput(p, new SignInputHandler() {
                            @Override
                            public void handleTextInput(String[] lines
                            ) {
                                String match = lines[0];
                                
                                LinkedList<OfflinePlayer> result = new LinkedList<>();
                                for (OfflinePlayer p : Bukkit.getOfflinePlayers()) {
                                    if (StringUtils.containsIgnoreCase(p.getName(), match)) {
                                        result.add(p);
                                    }
                                }
                                if (result.isEmpty()) {
                                    SignInputHandler handler = this;
                                    new NotificationPrompt(new Text("mcm.gui.players.search_e_t", lang).toString(), NotificationPrompt.Type.ERROR, new Text("mcm.gui.plugin.search_e_nf", lang).toString(), lang) {
                                        @Override
                                        public void onBack(Player p, String lang) {
                                            MCManager.requestInput(p, handler);
                                        }
                                        
                                        @Override
                                        public void onContinue(Player p, String lang) {
                                            MainPlayerMenu.this.open(p);
                                        }
                                    }.open(p);
                                } else if (result.size() == 1) {
                                    new PlayerMenu(result.get(0), lang).open(p);
                                } else {
                                    new MainPlayerMenu(lang, 0, true, () -> {
                                        return result.toArray(new OfflinePlayer[result.size()]);
                                    }).open(p);
                                }
                            }
                            
                            @Override
                            
                            public String[] getDefault() {
                                return null;
                            }
                            
                        })) {
                            new NotificationPrompt(e.getInventory().getTitle(), NotificationPrompt.Type.ERROR, "ProtocolLib is not installed. You need to have it installed for this function to work.", lang) {
                                @Override
                                public void onBack(Player p, String lang) {
                                    MainPlayerMenu.this.open(p);
                                }
                                
                                @Override
                                public void onContinue(Player p, String lang) {
                                    e.getView().close();
                                }
                            }.open(p);
                        }
                    }
                }.runTaskAsynchronously(MCManager.getInstance());
                break;
            case 30:
                if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new MainPlayerMenu(getLanguage(), page - 1, offline, () -> {
                        return loader;
                    }).open(p);
                }
                break;
            case 32:
                if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new MainPlayerMenu(getLanguage(), page - 1, offline, () -> {
                        return loader;
                    }).open(p);
                }
                break;
            default:
                int conv = GUIUtils.convertInventorySlot(slot);
                if (conv >= 0 && e.getCurrentItem().getType() == Material.SKULL_ITEM) {
                    new PlayerMenu(Bukkit.getOfflinePlayer(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())), getLanguage()).open(p);
                }
                break;
        }
    }
    
}
