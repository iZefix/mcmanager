/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.gui.player;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.BlockColor;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.utils.Text;
import java.util.Arrays;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author dbrosch
 */
public class PlayerInventoryMenu extends Menu {
    
    Player pl;
    
    public PlayerInventoryMenu(Player pl, String lang) {
        super(new Text("mcm.gui.player_inv.title", lang, pl.getName()).toString(), 54, lang);
        this.pl = pl;
        MenuItem empty = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.BLACK, " ");
        for (int i = 0; i < 54; i++) {
            getInventory().setItem(i, new ItemStack(Material.AIR));
        }
        for (int i = 36; i < 45; i++) {
            setItem(i, empty);
        }
        setItem(51, empty);
        setItem(49, GUIUtils.backOld(lang));
        setItem(50, new MenuItem(Material.CHEST, new Text("mcm.gui.player_inv.save", lang).toString()));
        
        ItemStack[] contents = pl.getInventory().getStorageContents();
        for (int i = 0; i < 45; i++) {
            try {
                setItem(i, new MenuItem(contents[i]));
            } catch (Exception ex) {
                
            }
        }
        
        ItemStack[] armor = pl.getInventory().getArmorContents();
        int y = 0;
        for (int i = 45; i < 49; i++) {
            try {
                setItem(i, new MenuItem(armor[y]));
            } catch (Exception ex) {
                
            }
            y++;
        }
        
        setItem(52, new MenuItem(pl.getInventory().getItemInOffHand()));
        setItem(53, new MenuItem(pl.getInventory().getItemInMainHand()));
    }
    
    @Override
    public void onClick(InventoryClickEvent e) {
        setAutoCancel(true);
        Player p = (Player) e.getWhoClicked();
        if (e.getSlot() == 49) {
            new PlayerMenu(pl, lang).open(p);
        } else if (e.getSlot() < 36) {
            setAutoCancel(false);
            if (e.getSlot() < 9 && e.getSlot() == pl.getInventory().getHeldItemSlot()) {
                try {
                    setItem(53, new MenuItem(getInventory().getItem(e.getSlot())));
                } catch (Exception ex) {
                    setItem(53, new MenuItem(Material.AIR));
                }
            }
        } else if (e.getSlot() == 50) {
            pl.getInventory().setStorageContents(Arrays.copyOf(e.getInventory().getContents(), pl.getInventory().getStorageContents().length));
            pl.getInventory().setArmorContents(Arrays.copyOfRange(e.getInventory().getContents(), 45, 49));
            if (e.getInventory().getItem(52) != null) {
                pl.getInventory().setItemInOffHand(e.getInventory().getItem(52));
            } else {
                pl.getInventory().setItemInOffHand(new ItemStack(Material.AIR));
            }
            if (MCManager.getConfiguration().getBoolean("player_inv.warn")) {
                pl.sendMessage(MCManager.getPrefix() + new Text("mcm.gui.player_inv.warn", Text.getLanguage(pl), p.getDisplayName()));
            }
        } else if (e.getSlot() == 53) {
            setAutoCancel(false);
            try {
                setItem(pl.getInventory().getHeldItemSlot(), new MenuItem(getInventory().getItem(e.getSlot())));
            } catch (Exception ex) {
                setItem(pl.getInventory().getHeldItemSlot(), new MenuItem(Material.AIR));
            }
        } else if (e.getSlot() == 52 || (e.getSlot() >= 45 && e.getSlot() < 49)) {
            setAutoCancel(false);
        }
    }
    
}
