/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.gui.plugin;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.PluginHook;
import com.betafase.mcmanager.api.UpdateChecker.UpdateInfo;
import com.betafase.mcmanager.gui.ConfigMenu;
import com.betafase.mcmanager.gui.FileMenu;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.ChatPaginator;

/**
 * © Betafase Developers, 2015-2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class PluginMenu extends Menu {

    private Plugin pl;
    private UpdateInfo info = null;

    public PluginMenu(Plugin plugin, String lang) {
        super(new Text("mcm.gui.plugin.title2", lang, plugin.getName()).toString(), 36, lang);
        this.pl = plugin;
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        this.setItem(31, GUIUtils.back(lang));

        String executed_file = "Unknown";
        PluginHook hook = MCManager.getPluginHook(pl);

        try {
            executed_file = new File(pl.getClass().getProtectionDomain().getCodeSource().getLocation().toURI()).getName();
        } catch (Exception ex) {
            Logger.getLogger(MCManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        String authors = plugin.getDescription().getAuthors().toString();
        authors = authors.substring(1, authors.length() - 1);
        if (authors.isEmpty()) {
            authors = "Unknown";
        }
        MenuItem overview = new MenuItem(Material.MAGMA_CREAM, "§6§l" + plugin.getName(),
                "§7Author: §6" + authors,
                "§7Version: §6" + plugin.getDescription().getVersion(),
                "§7File: §6" + executed_file
        );
        this.setItem(4, overview);

        boolean not_mcm = !plugin.getName().equalsIgnoreCase("MCManager") || ModuleManager.isValid("mcmanager");
        for (int i = 0; i < 14; i++) {
            if (hook.isCustomItem(i)) {
                this.setItem(GUIUtils.convertCounterSlot(i), hook.getCustomItem(i));
            } else {
                switch (i) {
                    case 2:
                        if (not_mcm) {
                            MenuItem config = new MenuItem(Material.BOOK_AND_QUILL, new Text("mcm.gui.plugin.config", lang).toString());
                            this.setItem(GUIUtils.convertCounterSlot(i), config);
                        }
                        break;
                    case 7:
                        if (not_mcm) {
                            MenuItem folder = new MenuItem(Material.CHEST, new Text("mcm.gui.plugin.datafolder", lang).toString());
                            this.setItem(GUIUtils.convertCounterSlot(i), folder);
                        }
                        break;
                    case 4:
                        if (pl.isEnabled()) {
                            MenuItem disable = new MenuItem(Material.BARRIER, new Text("mcm.gui.plugin.disable", lang).toString());
                            this.setItem(GUIUtils.convertCounterSlot(i), disable);
                        } else {
                            MenuItem enable = new MenuItem(Material.BLAZE_POWDER, new Text("mcm.gui.plugin.enabled", lang).toString());
                            this.setItem(GUIUtils.convertCounterSlot(i), enable);
                        }
                        break;
                    case 13:
                        MenuItem delete = new MenuItem(Material.LAVA_BUCKET, new Text("mcm.gui.plugin.delete", lang));
                        this.setItem(GUIUtils.convertCounterSlot(i), delete);
                        break;
                }
            }
        }
        if (hook.hasUpdateChecker()) {
            Bukkit.getScheduler().runTaskAsynchronously(pl, () -> {
                info = hook.getUpdateChecker().checkUpdate();
                if (info != null) {
                    PluginMenu.this.info = info;
                    MenuItem update = new MenuItem(Material.REDSTONE_COMPARATOR);
                    ItemMeta updateMeta = update.getItemMeta();
                    updateMeta.setDisplayName("§9§l" + (info.getTitle() == null ? "Update" : info.getTitle()) + " v" + info.getVersion());
                    List<String> lore = new LinkedList<>();
                    if (info.getDescription() != null) {
                        for (String s : ChatPaginator.wordWrap(info.getDescription(), 40)) {
                            lore.add("§7" + ChatColor.stripColor(s));
                        }
                    }
                    lore.add(" ");
                    lore.add(new Text("mcm.gui.spigot.download", lang, "").toString());
                    updateMeta.setLore(lore);
                    update.setItemMeta(updateMeta);
                    this.setItem(GUIUtils.convertCounterSlot(10), update);
                }
            });
        } else if (!hook.isCustomItem(10)) {
            MenuItem nope = new MenuItem(Material.BAKED_POTATO);
            ItemMeta imeta = nope.getItemMeta();
            imeta.setDisplayName("§c§l" + new Text("mcm.gui.plugin.no_updater", lang));
            imeta.setLore(new Text("mcm.gui.plugin.no_updater_text", lang).wordWrap(40, ChatColor.GRAY));
            nope.setItemMeta(imeta);
            this.setItem(GUIUtils.convertCounterSlot(10), nope);
        }

    }

    @Override
    public void onClick(InventoryClickEvent e) {
        int slot = e.getSlot();
        Player p = (Player) e.getWhoClicked();
        if (slot == 31) {
            new MainPluginMenu(lang, 0).open(p);
            return;
        }
        PluginHook hook = MCManager.getPluginHook(pl);
        int convert = GUIUtils.convertInventorySlot(slot);
        if (convert == -1) {
            return;
        }
        if (hook.isCustomItem(convert)) {
            try {
                hook.getCustomItem(convert).onClick(e);
            } catch (Exception ex) {
                MCManager.getLog().log(Level.SEVERE, hook.getPlugin().getName() + " caused an exception at onClick in slot " + slot, ex);
            }
            return;
        }
        switch (convert) {
            case 2:
                if (ModuleManager.isValid(p, "config")) {
                    if (!pl.getName().equalsIgnoreCase("MCManager") || ModuleManager.isValid(p, "mcmanager")) {
                        new ConfigMenu(pl, pl.getConfig(), 0, lang).open(p);
                    }
                }
                break;
            case 10:
                if (e.getCurrentItem().getType() == Material.BAKED_POTATO) {
                    e.getView().close();
                    p.sendMessage("§a§nhttps://bitbucket.org/iZefix/mcmanager/issues/new");
                } else if (info != null) {
                    e.getView().close();
                    Bukkit.getScheduler().runTaskAsynchronously(pl, () -> {
                        if (MCManager.getUpdates().contains(pl.getName())) {
                            MCManager.getUpdates().remove(pl.getName());
                        }
                        info.performUpdate(p);
                    });
                }
                break;
            case 7:
                if (ModuleManager.isValid(p, "file")) {
                    if (!pl.getName().equalsIgnoreCase("MCManager") || ModuleManager.isValid(p, "mcmanager")) {
                        new FileMenu(pl.getDataFolder(), lang, p.getUniqueId(), 0).open(p);
                    } else {
                        GUIUtils.showNoAccess(e, lang);
                    }
                } else {
                    GUIUtils.showNoAccess(e, lang);
                }
                break;
            case 4:
                if (pl.isEnabled()) {
                    try {
                        Bukkit.getPluginManager().disablePlugin(pl);
                        MenuItem enable = new MenuItem(Material.BLAZE_POWDER, new Text("mcm.gui.plugin.enabled", lang).toString());
                        this.setItem(GUIUtils.convertCounterSlot(convert), enable);
                    } catch (Exception ex) {
                        e.getView().close();
                        p.sendMessage("§cFailed to disable plugin: " + ex.getMessage());
                        ex.printStackTrace();
                    }
                } else {
                    try {
                        Bukkit.getPluginManager().enablePlugin(pl);
                        MenuItem disable = new MenuItem(Material.BARRIER, new Text("mcm.gui.plugin.disable", lang).toString());
                        this.setItem(GUIUtils.convertCounterSlot(convert), disable);
                    } catch (Exception ex) {
                        e.getView().close();
                        p.sendMessage("§cFailed to enable plugin: " + ex.getMessage());
                        ex.printStackTrace();
                    }
                }
                break;
            case 13:
                e.getView().close();
                try {
                    if (pl.isEnabled()) {
                        Bukkit.getPluginManager().disablePlugin(pl);
                    }
                    new File(pl.getClass().getProtectionDomain().getCodeSource().getLocation().toURI()).delete();
                    Bukkit.reload();
                    p.performCommand("manager plugin");
                } catch (Exception ex) {
                    MCManager.getLog().log(Level.SEVERE, "Failed to delete Plugin " + pl.getName(), ex);
                }
                break;
            default:
                break;
        }
    }

}
