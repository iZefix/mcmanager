/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.gui.plugin;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.BlockColor;
import com.betafase.mcmanager.api.NotificationPrompt;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.gui.MainMenu;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import java.util.LinkedList;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import com.betafase.mcmanager.api.SignInputHandler;
import java.util.logging.Level;

/**
 * © Betafase Developers, 2015-2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class MainPluginMenu extends Menu {

    private int page;
    private Plugin[] plugins;

    public MainPluginMenu(String lang, int page) {
        this(lang, page, Bukkit.getPluginManager().getPlugins());
    }

    public MainPluginMenu(String lang, int page, Plugin[] plugins) {
        super("§9" + new Text("mcm.gui.plugin.title", lang), 36, lang);
        this.page = page;
        this.plugins = plugins;
        MenuItem black = new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 15, " ");
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        if (page != 0) {
            this.setItem(30, GUIUtils.previous_page(lang, page));
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        this.setItem(31, GUIUtils.back(lang));

        Bukkit.getScheduler().runTaskAsynchronously(MCManager.getInstance(), () -> {
            int counter = -page * 14;
            try {
                MenuItem reload = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.BLUE, new Text("mcm.gui.main.reload", lang).toString());
                this.setItem(35, reload);
                MenuItem search = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.BROWN, new Text("mcm.gui.plugin.search", lang).toString());
                setItem(27, search);
                if (ModuleManager.isValid("spiget")) {
                    MenuItem add = new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.LIME, new Text("mcm.gui.plugin.add", lang).toString());
                    this.setItem(28, add);
                }

                for (Plugin pl : plugins) {
                    if (counter < 0) {
                    } else if (counter == 14) {
                        this.setItem(32, GUIUtils.next_page(lang, page + 2));
                        break;
                    } else {
                        MenuItem plugin = new MenuItem(Material.MAGMA_CREAM);
                        ItemMeta pluginMeta = plugin.getItemMeta();
                        if (pl.isEnabled()) {
                            pluginMeta.setDisplayName("§a§l" + pl.getName());
                        } else {
                            pluginMeta.setDisplayName("§c§l" + pl.getName());
                        }
                        LinkedList<String> lore = new LinkedList<>();
                        lore.add(new Text("mcm.gui.plugin.version", lang, pl.getDescription().getVersion()).toString());
                        String authors = pl.getDescription().getAuthors().toString();
                        authors = authors.substring(1, authors.length() - 1);
                        if (!authors.isEmpty()) {
                            lore.add(new Text("mcm.gui.plugin.author", lang, authors).toString());
                        } else {
                            lore.add(new Text("mcm.gui.plugin.author", lang, "Unknown").toString());
                        }
                        lore.add(" ");
                        if (MCManager.getUpdates().contains(pl.getName())) {
                            pluginMeta.addEnchant(Enchantment.LUCK, 1, true);
                            pluginMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                            lore.add(new Text("mcm.gui.plugin.go_update", lang).toString());
                        } else {
                            lore.add(new Text("mcm.gui.plugin.go", lang).toString());
                        }
                        pluginMeta.setLore(lore);
                        plugin.setItemMeta(pluginMeta);
                        this.setItem(GUIUtils.convertCounterSlot(counter), plugin);
                    }
                    counter++;
                }
            } catch (Exception ex) {
                MCManager.getLog().log(Level.SEVERE, "Failed to load plugins", ex);
            }
        });

    }

    @Override
    public void onClick(InventoryClickEvent e) {
        int slot = e.getSlot();
        Player p = (Player) e.getWhoClicked();
        //String lang = Text.getLanguage(p);
        int conv = GUIUtils.convertInventorySlot(slot);
        if (conv != -1) {
            if (e.getCurrentItem().getType() == Material.MAGMA_CREAM) {
                new PluginMenu(Bukkit.getPluginManager().getPlugin(ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName())), lang).open(p);
            }
        } else if (slot == 27) {
            e.getView().close();
            if (!MCManager.requestInput(p, new SignInputHandler() {
                @Override
                public void handleTextInput(String[] lines) {
                    LinkedList<Plugin> list = new LinkedList<>();
                    String match = lines[0];
                    for (Plugin pl : plugins) {
                        if (StringUtils.containsIgnoreCase(pl.getName(), match)) {
                            list.add(pl);
                        }
                    }
                    if (list.isEmpty()) {
                        SignInputHandler handler = this;
                        new NotificationPrompt(new Text("mcm.gui.plugin.search_e_t", lang).toString(), NotificationPrompt.Type.ERROR, new Text("mcm.gui.plugin.search_e_nf", lang).toString(), lang) {
                            @Override
                            public void onBack(Player p, String lang) {
                                MCManager.requestInput(p, handler);
                            }

                            @Override
                            public void onContinue(Player p, String lang) {
                                MainPluginMenu.this.open(p);
                            }
                        }.open(p);
                    } else if (list.size() == 1) {
                        new PluginMenu(list.getFirst(), lang).open(p);
                    } else {
                        new MainPluginMenu(lang, 0, list.toArray(new Plugin[list.size()])).open(p);
                    }

                }

                @Override
                public String[] getDefault() {
                    return null;
                }
            })) {
                new NotificationPrompt(e.getInventory().getTitle(), NotificationPrompt.Type.ERROR, "ProtocolLib is not installed. You need to have it installed for this function to work.", lang) {
                    @Override
                    public void onBack(Player p, String lang) {
                        MainPluginMenu.this.open(p);
                    }

                    @Override
                    public void onContinue(Player p, String lang) {
                        e.getView().close();
                    }
                }.open(p);
            }
        } else if (slot == 35) {
            if (p.isOp() || p.hasPermission("bukkit.command.reload")) {
                e.getView().close();
                Bukkit.reload();
                p.performCommand("manager plugin");
            } else {
                GUIUtils.showNoAccess(e, lang);
            }
        } else if (slot == 28) {
            if (ModuleManager.isValid(p, "spiget")) {
                new AddPluginMenu(lang).open(p);
            } else {
                GUIUtils.showNoAccess(e, lang);
            }
        } else if (slot == 30) {
            if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                new MainPluginMenu(lang, page - 1, plugins).open(p);
            }
        } else if (slot == 31) {
            new MainMenu(p, lang).open(p);
        } else if (slot == 32) {
            if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                new MainPluginMenu(lang, page + 1, plugins).open(p);
            }
        }
    }

}
