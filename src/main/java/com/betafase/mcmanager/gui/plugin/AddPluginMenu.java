/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.gui.plugin;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.gui.SpigotMenu;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import com.betafase.mcmanager.utils.spiget.PluginInfoRequest;
import com.betafase.mcmanager.utils.spiget.SpigetPlugin;
import java.util.HashMap;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import com.betafase.mcmanager.api.SignInputHandler;

/**
 * © Betafase Developers, 2015-2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class AddPluginMenu extends Menu {

    public AddPluginMenu(String lang) {
        super(new Text("mcm.gui.add_plugin.title", lang).toString(), 36, lang);
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        this.setItem(31, GUIUtils.back(lang));
    }

    @Override
    public void onOpen(Player p) {
        MenuItem trending_plugins = new MenuItem(Material.COMPASS, new Text("mcm.gui.add_plugin.top_plugins", lang).toString());

        MenuItem search = new MenuItem(Material.NAME_TAG, new Text("mcm.gui.add_plugin.search", lang).toString());

        MenuItem new_plugins = new MenuItem(Material.ANVIL, new Text("mcm.gui.add_plugin.new", lang));
        if (ModuleManager.isValid("wget")) {
            this.setItem(19, trending_plugins);
            this.setItem(12, search);
            this.setItem(14, new_plugins);
            MenuItem from_url = new MenuItem(Material.STAINED_CLAY, 1, (short) 5, new Text("mcm.gui.add_plugin.from_url", lang).toString());
            this.setItem(25, from_url);
        } else {
            this.setItem(20, trending_plugins);
            this.setItem(13, search);
            this.setItem(24, new_plugins);
        }
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (ModuleManager.isValid(p, "wget")) {
            switch (e.getSlot()) {
                case 19:
                    new SpigotMenu(lang, 0, new SpigotMenu.PluginDataLoader() {
                        @Override
                        public SpigetPlugin[] loadData() {
                            PluginInfoRequest r = new PluginInfoRequest("resources", new HashMap<String, String>() {
                                {
                                    put("size", Integer.toString(50));
                                    put("sort", "-downloads");
                                }
                            });
                            return r.getPlugins();
                        }

                        @Override
                        public void onBack(Player p) {
                            new AddPluginMenu(lang).open(p);
                        }
                    }).open(p);
                    break;
                case 12:
                    e.getView().close();
                    if (!MCManager.requestInput(p, new SignInputHandler() {
                        @Override
                        public void handleTextInput(String[] lines) {
                            String result = lines[0] + lines[1] + lines[2] + lines[3];
                            if (result == null || result.isEmpty()) {
                                AddPluginMenu.this.open(p);
                            } else {
                                p.performCommand("searchplugin " + result);
                            }
                        }

                        @Override
                        public String[] getDefault() {
                            return null;
                        }
                    })) {
                        p.sendMessage("§lProtocolLib§r is not installed. Please use §c/searchplugin <query>§r to continue.");
                    }
                    break;
                case 14:
                    new SpigotMenu(lang, 0, new SpigotMenu.PluginDataLoader() {
                        @Override
                        public SpigetPlugin[] loadData() {
                            PluginInfoRequest r = new PluginInfoRequest("resources", new HashMap<String, String>() {
                                {
                                    put("size", Integer.toString(50));
                                    put("sort", "-updateDate");
                                }
                            });
                            return r.getPlugins();
                        }

                        @Override
                        public void onBack(Player p) {
                            new AddPluginMenu(lang).open(p);
                        }
                    }).open(p);
                    break;
                case 25:
                    if (ModuleManager.isValid(p, "wget")) {
                        e.getView().close();
                        TextComponent proceed = new TextComponent(new Text("mcm.gui.plugin.add_proceed", lang).toString());
                        proceed.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/wget plugins <url>"));
                        p.spigot().sendMessage(proceed);
                    } else {
                        GUIUtils.showNoAccess(e, lang);
                    }
                    break;
                case 31:
                    new MainPluginMenu(lang, 0).open(p);
                    break;
            }
        } else {
            switch (e.getSlot()) {
                case 20:
                    new SpigotMenu(lang, 0, new SpigotMenu.PluginDataLoader() {
                        @Override
                        public SpigetPlugin[] loadData() {
                            PluginInfoRequest r = new PluginInfoRequest("resources", new HashMap<String, String>() {
                                {
                                    put("size", Integer.toString(50));
                                    put("sort", "-downloads");
                                }
                            });
                            return r.getPlugins();
                        }

                        @Override
                        public void onBack(Player p) {
                            new AddPluginMenu(lang).open(p);
                        }
                    }).open(p);
                    break;
                case 13:
                    e.getView().close();
                    if (!MCManager.requestInput(p, new SignInputHandler() {
                        @Override
                        public void handleTextInput(String[] lines) {
                            String result = lines[0] + lines[1] + lines[2] + lines[3];
                            if (result == null || result.isEmpty()) {
                                AddPluginMenu.this.open(p);
                            } else {
                                p.performCommand("searchplugin " + result);
                            }
                        }

                        @Override
                        public String[] getDefault() {
                            return null;
                        }
                    })) {
                        p.sendMessage("§lProtocolLib§r is not installed. Please use §c/searchplugin <query>§r to continue.");
                    }
                    break;
                case 24:
                    new SpigotMenu(lang, 0, new SpigotMenu.PluginDataLoader() {
                        @Override
                        public SpigetPlugin[] loadData() {
                            PluginInfoRequest r = new PluginInfoRequest("resources", new HashMap<String, String>() {
                                {
                                    put("size", Integer.toString(50));
                                    put("sort", "-updateDate");
                                }
                            });
                            return r.getPlugins();
                        }

                        @Override
                        public void onBack(Player p) {
                            new AddPluginMenu(lang).open(p);
                        }
                    }).open(p);
                    break;
                case 31:
                    new MainPluginMenu(lang, 0).open(p);
                    break;
            }
        }
    }

}
