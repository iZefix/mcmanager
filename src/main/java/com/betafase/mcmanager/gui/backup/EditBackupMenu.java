/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.gui.backup;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.api.SafetyPrompt;
import com.betafase.mcmanager.gui.FileMenu;
import com.betafase.mcmanager.utils.PlayerRestoreData;
import com.betafase.mcmanager.utils.Text;
import com.betafase.mcmanager.utils.WorldLoadManager;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

/**
 *
 * @author dbrosch
 */
public class EditBackupMenu extends Menu {

    File backup;
    short color;

    public EditBackupMenu(File backup, short color, String lang) {
        super("§9Backup §7" + backup.getName(), 36, lang);
        this.color = color;
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        setItem(31, GUIUtils.back(lang));
        this.backup = backup;

        MenuItem backupItem = new MenuItem(Material.STAINED_CLAY);
        backupItem.setDurability(color);
        ItemMeta bMeta = backupItem.getItemMeta();
        bMeta.setDisplayName(new Text(backup.getName().startsWith("worlds-") ? "mcm.gui.backup.world" : "mcm.gui.backup.plugindata", lang, new SimpleDateFormat("dd.MM.yy HH:mm").format(new Date(backup.lastModified()))).toString());
        LinkedList<String> lore = new LinkedList<>();
        if (backup.isDirectory()) {
            lore.add(new Text("mcm.gui.backup.uncompressed", lang).toString());
            lore.add(new Text("mcm.gui.file.size", lang, FileUtils.byteCountToDisplaySize(FileUtils.sizeOfDirectory(backup))).toString());
        } else if (backup.getName().endsWith(".zip")) {
            lore.add(new Text("mcm.gui.backup.compressed", lang).toString());
            lore.add(new Text("mcm.gui.file.size", lang, FileUtils.byteCountToDisplaySize(FileUtils.sizeOf(backup))).toString());
        }
        bMeta.setLore(lore);
        backupItem.setItemMeta(bMeta);
        setItem(4, backupItem);

        setItem(20, new MenuItem(Material.EMERALD, "§a§l" + new Text("mcm.gui.backup.download", lang)));
        MenuItem remove = new MenuItem(Material.BARRIER, new Text("mcm.gui.file.remove", lang).toString(), new Text("mcm.gui.file.remove_warning", lang).toString());
        setItem(24, remove);
        if (backup.getName().startsWith("worlds-")) {
            setItem(13, new MenuItem(Material.DRAGON_EGG, new Text("mcm.gui.backup.restore", lang)));
        }
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        switch (e.getSlot()) {
            case 13:
                if (!backup.getName().startsWith("worlds-")) {
                    break;
                }
                e.getView().close();
                if (backup.getName().endsWith(".zip")) {
                    MCManager.getBackupManager().setPaused(true);
                    boolean experimental = MCManager.getConfiguration().getBoolean("backup.experimental", false);
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.backup.restore", lang).toString());
                    List<PlayerRestoreData> savedData;
                    World w;
                    if (experimental) {
                        w = new WorldCreator("MCManager/LoadingWorld").generateStructures(false).environment(World.Environment.NORMAL)
                                .generatorSettings("3;5*166;8;").type(WorldType.FLAT).createWorld();
                        w.setDifficulty(Difficulty.PEACEFUL);
                        w.setGameRuleValue("doDaylightCicle", "false");
                        savedData = new LinkedList<>();
                        for (Player a : Bukkit.getOnlinePlayers()) {
                            Location loc = a.getLocation();
                            savedData.add(new PlayerRestoreData(a.getUniqueId(), a.getInventory(), a.getHealth(), a.getFoodLevel(), a.getLocation(), a.getGameMode()));
                            a.setGameMode(GameMode.ADVENTURE);
                            loc.setY(5.5);
                            loc.setWorld(w);
                            a.teleport(loc);
                        }
                    } else {
                        w = null;
                        savedData = null;
                        for (Player a : Bukkit.getOnlinePlayers()) {
                            a.kickPlayer("§9§lMCManager\n"
                                    + "§r\n"
                                    + "§cRestoring Worlds from Backup\n"
                                    + "§7Please wait, the server is restarting.\n");
                        }
                    }
                    Bukkit.getScheduler().runTaskLater(MCManager.getInstance(), () -> {
                        try {
                            ZipFile zipFile = new ZipFile(backup);
                            try {
                                Enumeration<? extends ZipEntry> entries = zipFile.entries();
                                while (entries.hasMoreElements()) {
                                    ZipEntry entry = entries.nextElement();
                                    File entryDestination = new File(Bukkit.getWorldContainer(), entry.getName());
                                    if (entryDestination.exists()) {
                                        FileUtils.deleteQuietly(entryDestination);
                                    }
                                    if (entry.isDirectory()) {
                                        entryDestination.mkdirs();
                                    } else {
                                        entryDestination.getParentFile().mkdirs();
                                        InputStream in = zipFile.getInputStream(entry);
                                        OutputStream out = new FileOutputStream(entryDestination);
                                        IOUtils.copy(in, out);
                                        IOUtils.closeQuietly(in);
                                        out.close();
                                    }
                                }
                            } finally {
                                zipFile.close();
                            }
                            if (experimental) {
                                new BukkitRunnable() {

                                    private int counter = 20;

                                    @Override
                                    public void run() {
                                        if (counter > 0) {
                                            if (counter == 20) {
                                                TextComponent prefix = new TextComponent(MCManager.getPrefix());
                                                TextComponent message = new TextComponent(new Text("mcm.backup.restart_info", lang).toString());
                                                message.setColor(net.md_5.bungee.api.ChatColor.RED);
                                                message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Prepare /restart in chat").create()));
                                                message.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/restart"));
                                                p.spigot().sendMessage(prefix, message);
                                            } else if (counter == 1) {
                                                p.sendMessage(MCManager.getPrefix() + new Text("mcm.backup.restart_t2", lang, counter));
                                            } else if (counter <= 5 || counter == 10 || counter == 15) {
                                                p.sendMessage(MCManager.getPrefix() + new Text("mcm.backup.restart_t1", lang, counter));
                                            }
                                            counter--;
                                        } else if (counter == 0) {
                                            MCManager.getLog().log(Level.INFO, "Reloading Worlds from Disc. Expect lag for a short while.");
                                            for (World wu : Bukkit.getWorlds()) {
                                                if (!wu.getName().startsWith("MCManager/")) {
                                                    MCManager.getLog().log(Level.INFO, "Restoring {0}", wu.getName());
                                                    WorldCreator wc = new WorldCreator(wu.getName()).copy(wu);
                                                    WorldLoadManager.unloadWorld(wu);
                                                    World newW = wc.createWorld();
                                                    MCManager.getLog().log(Level.INFO, "World {0} has been restored successfully", newW.getName());
                                                }
                                            }
                                            MCManager.getBackupManager().setPaused(false);
                                            for (PlayerRestoreData data : savedData) {
                                                data.restore(true);
                                            }
                                            p.sendMessage(MCManager.getPrefix() + new Text("mcm.backup.restore_complete", lang));
                                            Bukkit.getScheduler().runTaskLater(MCManager.getInstance(), () -> {
                                                WorldLoadManager.unloadWorld(w);
                                                FileUtils.deleteQuietly(w.getWorldFolder());
                                            }, 60L);
                                            for (Player a : Bukkit.getOnlinePlayers()) {
                                                TextComponent t = new TextComponent(new Text("mcm.backup.restore_warning", Text.getLanguage(a)).toString());
                                                t.setColor(net.md_5.bungee.api.ChatColor.YELLOW);
                                                a.spigot().sendMessage(new TextComponent(MCManager.getPrefix()), t);
                                            }
                                            this.cancel();
                                        }
                                    }
                                }.runTaskTimer(MCManager.getInstance(), 20L, 20L);
                            } else {
                                MCManager.getLog().log(Level.INFO, "Backup restored. Shutting down server.");
                                Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restart");
                                Bukkit.shutdown();

                            }

                        } catch (Exception ex) {
                            p.sendMessage(MCManager.getPrefix() + new Text("mcm.exception.message", lang, ex.getClass().getSimpleName(), ex.getMessage()));
                            MCManager.getLog().log(Level.SEVERE, "Failed to restore backup.", ex);
                        }
                    }, 80L);

                } else {
                    p.sendMessage(MCManager.getPrefix() + "§cRestoring raw backups is currently not supported.");
                }
                break;
            case 20:
                e.getView().close();
                if (backup.exists() && !backup.isDirectory()) {
                    try {
                        p.sendMessage("§a§n" + InetAddress.getLocalHost().getCanonicalHostName() + ":" + MCManager.getWebServer().getPort() + "/download?file=" + backup.getPath());
                    } catch (UnknownHostException ex) {
                        Logger.getLogger(FileMenu.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    p.sendMessage(MCManager.getPrefix() + "§cDownload of RAW backups is not supported, yet.");
                }
                break;
            case 24:
                new SafetyPrompt(new Text("mcm.gui.safety", lang).toString(), lang) {
                    @Override
                    public void onApprove(boolean approve) {
                        if (approve) {
                            FileUtils.deleteQuietly(backup);
                            new BackupsMenu(lang).open(p);
                        } else {
                            new EditBackupMenu(backup, color, lang).open(p);
                        }
                    }
                }.open(p);
                break;
            case 31:
                new BackupsMenu(lang).open(p);
                break;
        }
    }
}
