/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.gui.backup;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.api.NotificationPrompt;
import com.betafase.mcmanager.api.SignInputHandler;
import com.betafase.mcmanager.utils.Text;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

public class BackupSettingsMenu extends Menu {

    public BackupSettingsMenu(String lang) {
        super(new Text("mcm.gui.backup.set_title", lang).toString(), 36, lang);
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);
        setItem(31, GUIUtils.back(lang));

        FileConfiguration cfg = MCManager.getConfiguration();

        this.setItem(10, new MenuItem(Material.ENDER_CHEST, new Text("mcm.gui.backup.set_auto_t", lang).toString(), new Text("mcm.gui.backup.set_auto_d", lang).wordWrapAsArray(40, ChatColor.GRAY)));
        setItem(19, GUIUtils.displayBoolean(cfg.getBoolean("backup.enabled"), lang));

        this.setItem(11, new MenuItem(Material.SKULL_ITEM, new Text("mcm.gui.backup.set_empty_t", lang).toString(), new Text("mcm.gui.backup.set_empty_d", lang).wordWrapAsArray(40, ChatColor.GRAY)));
        setItem(20, GUIUtils.displayBoolean(cfg.getBoolean("backup.empty"), lang));

        this.setItem(3 + 9, new MenuItem(Material.DRAGON_EGG, new Text("mcm.gui.backup.set_zip_t", lang).toString(), new Text("mcm.gui.backup.set_zip_d", lang).wordWrapAsArray(40, ChatColor.GRAY)));
        setItem(21, GUIUtils.displayBoolean(cfg.getBoolean("backup.zip"), lang));

        this.setItem(4 + 9, new MenuItem(Material.GOLDEN_APPLE, new Text("mcm.gui.backup.set_exp_t", lang).toString(), new Text("mcm.gui.backup.set_exp_d", lang).wordWrapAsArray(40, ChatColor.GRAY)));
        setItem(22, GUIUtils.displayBoolean(cfg.getBoolean("backup.experimental"), lang));

        this.setItem(5 + 9, new MenuItem(Material.ANVIL, new Text("mcm.gui.backup.set_int_t", lang).toString(), new Text("mcm.gui.backup.set_int_d", lang).wordWrapAsArray(40, ChatColor.GRAY)));
        // backup.interval
        setItem(23, GUIUtils.displayInteger(cfg.getInt("backup.interval"), lang));

        this.setItem(6 + 9, new MenuItem(Material.BUCKET, new Text("mcm.gui.backup.set_amount_t", lang).toString(), new Text("mcm.gui.backup.set_amount_d", lang).wordWrapAsArray(40, ChatColor.GRAY)));
        // backup.amount
        setItem(24, GUIUtils.displayInteger(cfg.getInt("backup.amount"), lang));

        // backup.folder
        this.setItem(7 + 9, new MenuItem(Material.NAME_TAG, new Text("mcm.gui.backup.set_folder_t", lang).toString(), new Text("mcm.gui.backup.set_folder_d", lang).wordWrapAsArray(40, ChatColor.GRAY)));
        setItem(25, GUIUtils.displayString(cfg.getString("backup.folder"), lang));
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        switch (e.getSlot()) {
            case 31:
                new BackupsMenu(lang).open(p);
                break;
            case 19:
                if (MCManager.getConfiguration().getBoolean("backup.enabled")) {
                    MCManager.getConfiguration().set("backup.enabled", false);
                    MCManager.saveConfiguration();
                    if (MCManager.getBackupManager() != null && MCManager.getBackupManager().isActive()) {
                        MCManager.getBackupManager().cancel();
                    }
                    e.setCurrentItem(GUIUtils.disabled(lang));
                } else {
                    MCManager.getConfiguration().set("backup.enabled", true);
                    MCManager.saveConfiguration();
                    MCManager.getBackupManager().restart();
                    e.setCurrentItem(GUIUtils.enabled(lang));
                }
                break;
            case 20:
                if (MCManager.getConfiguration().getBoolean("backup.empty")) {
                    MCManager.getConfiguration().set("backup.empty", false);
                    MCManager.saveConfiguration();
                    e.setCurrentItem(GUIUtils.disabled(lang));
                } else {
                    MCManager.getConfiguration().set("backup.empty", true);
                    MCManager.saveConfiguration();
                    e.setCurrentItem(GUIUtils.enabled(lang));
                }
                break;
            case 21:
                if (MCManager.getConfiguration().getBoolean("backup.zip")) {
                    MCManager.getConfiguration().set("backup.zip", false);
                    MCManager.saveConfiguration();
                    e.setCurrentItem(GUIUtils.disabled(lang));
                } else {
                    MCManager.getConfiguration().set("backup.zip", true);
                    MCManager.saveConfiguration();
                    e.setCurrentItem(GUIUtils.enabled(lang));
                }
                break;
            case 22:
                if (MCManager.getConfiguration().getBoolean("backup.experimental")) {
                    MCManager.getConfiguration().set("backup.experimental", false);
                    MCManager.saveConfiguration();
                    e.setCurrentItem(GUIUtils.disabled(lang));
                } else {
                    MCManager.getConfiguration().set("backup.experimental", true);
                    MCManager.saveConfiguration();
                    e.setCurrentItem(GUIUtils.enabled(lang));
                }
                break;
            case 23:
                if (!MCManager.requestInput(p, new SignInputHandler() {
                    @Override
                    public void handleTextInput(String[] lines) {
                        try {
                            int i = Integer.parseInt(lines[0]);
                            if (i < -1) {
                                throw new NumberFormatException();
                            }
                            MCManager.getConfiguration().set("backup.interval", i);
                            MCManager.saveConfiguration();
                            e.setCurrentItem(GUIUtils.displayInteger(i, lang));
                            BackupSettingsMenu.this.open(p);
                        } catch (NumberFormatException ex) {
                            SignInputHandler handler = this;
                            new NotificationPrompt(getInventory().getTitle(), NotificationPrompt.Type.ERROR, new Text("mcm.gui.backup.set_invalid_n", lang).toString(), lang) {
                                @Override
                                public void onBack(Player p, String lang) {
                                    MCManager.requestInput(p, handler);
                                }

                                @Override
                                public void onContinue(Player p, String lang) {
                                    BackupSettingsMenu.this.open(p);
                                }
                            }.open(p);
                        }
                    }

                    @Override
                    public String[] getDefault() {
                        return null;
                    }
                })) {
                    new NotificationPrompt(getInventory().getTitle(), NotificationPrompt.Type.ERROR, new Text("mcm.gui.backup.set_no_sign", lang).toString(), lang) {
                        @Override
                        public void onBack(Player p, String lang) {
                            this.open(p);
                        }

                        @Override
                        public void onContinue(Player p, String lang) {
                            onBack(p, lang);
                        }
                    }.open(p);
                }
                break;
            case 24:
                if (!MCManager.requestInput(p, new SignInputHandler() {
                    @Override
                    public void handleTextInput(String[] lines) {
                        try {
                            int i = Integer.parseInt(lines[0]);
                            if (i < 1) {
                                throw new NumberFormatException();
                            }
                            MCManager.getConfiguration().set("backup.amount", i);
                            MCManager.saveConfiguration();
                            e.setCurrentItem(GUIUtils.displayInteger(i, lang));
                            BackupSettingsMenu.this.open(p);
                        } catch (NumberFormatException ex) {
                            SignInputHandler handler = this;
                            new NotificationPrompt(getInventory().getTitle(), NotificationPrompt.Type.ERROR, new Text("mcm.gui.backup.set_invalid_n", lang).toString(), lang) {
                                @Override
                                public void onBack(Player p, String lang) {
                                    MCManager.requestInput(p, handler);
                                }

                                @Override
                                public void onContinue(Player p, String lang) {
                                    BackupSettingsMenu.this.open(p);
                                }
                            }.open(p);
                        }
                    }

                    @Override
                    public String[] getDefault() {
                        return null;
                    }
                })) {
                    new NotificationPrompt(getInventory().getTitle(), NotificationPrompt.Type.ERROR, new Text("mcm.gui.backup.set_no_sign", lang).toString(), lang) {
                        @Override
                        public void onBack(Player p, String lang) {
                            this.open(p);
                        }

                        @Override
                        public void onContinue(Player p, String lang) {
                            onBack(p, lang);
                        }
                    }.open(p);
                }
                break;
            case 25:
                if (!MCManager.requestInput(p, new SignInputHandler() {
                    @Override
                    public void handleTextInput(String[] lines) {
                        try {
                            if (lines[0].isEmpty()) {
                                throw new Exception("Folder name is empty!");
                            }
                            MCManager.getConfiguration().set("backup.folder", lines[0]);
                            MCManager.saveConfiguration();
                            e.setCurrentItem(GUIUtils.displayString(lines[0], lang));
                            BackupSettingsMenu.this.open(p);
                        } catch (Exception ex) {
                            SignInputHandler handler = this;
                            new NotificationPrompt("§c" + ex.getClass().getSimpleName(), NotificationPrompt.Type.ERROR, ex.getMessage(), lang) {
                                @Override
                                public void onBack(Player p, String lang) {
                                    MCManager.requestInput(p, handler);
                                }

                                @Override
                                public void onContinue(Player p, String lang) {
                                    BackupSettingsMenu.this.open(p);
                                }
                            }.open(p);
                        }
                    }

                    @Override
                    public String[] getDefault() {
                        return null;
                    }
                })) {
                    new NotificationPrompt(getInventory().getTitle(), NotificationPrompt.Type.ERROR, new Text("mcm.gui.backup.set_no_sign", lang).toString(), lang) {
                        @Override
                        public void onBack(Player p, String lang) {
                            this.open(p);
                        }

                        @Override
                        public void onContinue(Player p, String lang) {
                            onBack(p, lang);
                        }
                    }.open(p);
                }
                break;
        }
    }

}
