/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.gui.backup;

import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.BlockColor;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.Random;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import com.betafase.mcmanager.gui.MainMenu;

/**
 *
 * @author dbrosch
 */
public class BackupsMenu extends Menu {

    File[] files;
    int page;

    public BackupsMenu(String lang) {
        this(lang, 0);
    }

    public BackupsMenu(String lang, int page) {
        // 1: quick
        super(new Text("mcm.gui.backup.title", lang).toString(), 36, lang);
        this.page = page;
        MenuItem black = GUIUtils.black();
        for (int i = 0; i < 9; i++) {
            setItem(i, black);
            setItem(27 + i, black);
        }
        setItem(9, black);
        setItem(18, black);
        setItem(17, black);
        setItem(26, black);

        this.setItem(31, GUIUtils.back(lang));
        if (page != 0) {
            setItem(30, GUIUtils.previous_page(lang, page));
        }
        this.setItem(4, new MenuItem(Material.ENDER_CHEST, new Text("mcm.gui.backup.quick", lang).toString()));
        this.setItem(35, new MenuItem(Material.STAINED_GLASS_PANE, BlockColor.BLUE, new Text("mcm.gui.backup.settings", lang).toString()));
        new BukkitRunnable() {

            @Override
            public void run() {
                int counter = -page * 14;
                files = new File(MCManager.getConfiguration().getString("backup.folder")).listFiles((File dir, String name) -> name.startsWith("worlds-") || name.startsWith("plugindata-"));
                Arrays.sort(files, (File o1, File o2) -> {
                    if (o1.lastModified() < o2.lastModified()) {
                        return 1;
                    } else {
                        return -1;
                    }
                });
                for (File f : files) {
                    if (counter < 0) {

                    } else if (counter == 14) {
                        setItem(32, GUIUtils.next_page(lang, page + 2));
                        break;
                    } else {
                        /**
                         *
                         * WARNING: STILL UNTESTED
                         *
                         */
                        MenuItem backup = new MenuItem(Material.STAINED_CLAY);
                        backup.setDurability((short) new Random().nextInt(16));
                        ItemMeta bMeta = backup.getItemMeta();
                        LinkedList<String> lore = new LinkedList<>();
                        if (f.getName().startsWith("plugindata-")) {
                            backup.setType(Material.STAINED_GLASS);
                        }
                        bMeta.setDisplayName(new Text(f.getName().startsWith("worlds-") ? "mcm.gui.backup.world" : "mcm.gui.backup.plugindata", lang, new SimpleDateFormat("dd.MM.yy HH:mm").format(new Date(f.lastModified()))).toString());
                        if (f.isDirectory()) {
                            lore.add(new Text("mcm.gui.backup.uncompressed", lang).toString());
                            lore.add(new Text("mcm.gui.file.size", lang, FileUtils.byteCountToDisplaySize(FileUtils.sizeOfDirectory(f))).toString());
                            lore.add(" ");
                            lore.add(new Text("mcm.gui.backup.edit", lang).toString());
                        } else if (f.getName().endsWith(".zip")) {
                            lore.add(new Text("mcm.gui.backup.compressed", lang).toString());
                            lore.add(new Text("mcm.gui.file.size", lang, FileUtils.byteCountToDisplaySize(FileUtils.sizeOf(f))).toString());
                            lore.add(" ");
                            lore.add(new Text("mcm.gui.backup.edit", lang).toString());
                        } else {
                            lore.add("§cInvalid data format. Data could not be parsed.");
                            backup.setType(Material.BARRIER);
                        }
                        bMeta.setLore(lore);
                        backup.setItemMeta(bMeta);
                        setItem(GUIUtils.convertCounterSlot(counter), backup);
                    }
                    counter++;
                }
            }
        }.runTaskAsynchronously(MCManager.getInstance());

    }

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        switch (e.getSlot()) {
            case 4:
                Bukkit.getScheduler().runTaskAsynchronously(MCManager.getInstance(), () -> {
                    MCManager.getBackupManager().backupData();
                    new BackupsMenu(lang).open(p);
                });
                break;
            case 31:
                new MainMenu(p, lang).open(p);
                break;
            case 35:
                if (e.getCurrentItem().getDurability() == BlockColor.BLUE.toShort()) {
                    if (ModuleManager.isValid(p, "backup.settings")) {
                        new BackupSettingsMenu(lang).open(p);
                    } else {
                        GUIUtils.showNoAccess(e, lang);
                    }
                }
                break;
            case 30:
                if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new BackupsMenu(lang, page - 1).open(p);
                }
                break;
            case 32:
                if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new BackupsMenu(lang, page + 1).open(p);
                }
                break;
            default:
                if (e.getCurrentItem().getType() == Material.STAINED_CLAY || e.getCurrentItem().getType() == Material.STAINED_GLASS) {
                    int conv = GUIUtils.convertInventorySlot(e.getSlot());
                    if (conv != -1) {
                        new EditBackupMenu(files[conv], e.getCurrentItem().getDurability(), lang).open(p);
                    }
                }
                break;
        }
    }
}
