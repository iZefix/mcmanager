/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.gui;

import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.BlockColor;
import com.betafase.mcmanager.api.NotificationPrompt;
import com.betafase.mcmanager.api.SafetyPrompt;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import com.betafase.mcmanager.utils.Utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.scheduler.BukkitRunnable;
import com.betafase.mcmanager.api.SignInputHandler;
import java.util.Arrays;
import org.bukkit.event.inventory.ClickType;

/**
 * © Betafase Developers, 2015
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class FileMenu extends Menu {

    private File dir;
    private String home;
    private int page;
    private UUID player;

    public static HashMap<UUID, Clipboard> clipboards = new HashMap<>();

    public FileMenu(File dir, String lang, UUID player, int page) {
        super(dir.getName().length() > 22 ? new Text("mcm.gui.file.title", lang, dir.getName().substring(0, 20) + "...").toString() : new Text("mcm.gui.file.title", lang, dir.getName()).toString(), 36, lang);
        this.dir = dir;
        this.player = player;
        home = System.getProperty("user.dir");
        if (!ModuleManager.isFileEnabled(null, dir)) {
            setInventorySize(18);
            this.setItem(4, GUIUtils.no_access(lang));
            this.setItem(13, GUIUtils.backOld(lang));
            return;
        }
        if (!dir.isDirectory()) {
            MenuItem black = GUIUtils.black();
            for (int i = 0; i < 9; i++) {
                setItem(i, black);
                setItem(27 + i, black);
            }
            setItem(9, black);
            setItem(18, black);
            setItem(17, black);
            setItem(26, black);
            this.setItem(31, GUIUtils.back(lang));

            MenuItem remove = new MenuItem(Material.BARRIER, new Text("mcm.gui.file.remove", lang).toString());
            this.setItem(GUIUtils.convertCounterSlot(13), remove);

            if (ModuleManager.isValid("webserver") && MCManager.getWebServer().isOnline()) {
                MenuItem download = new MenuItem(Material.EMERALD, "§a§l" + new Text("mcm.gui.file.download2", lang));
                this.setItem(GUIUtils.convertCounterSlot(11), download);
            }

            MenuItem rename = new MenuItem(Material.NAME_TAG, "§e§l" + new Text("mcm.gui.file.rename", lang));
            this.setItem(GUIUtils.convertCounterSlot(5), rename);

            MenuItem copy = new MenuItem(Material.STAINED_CLAY, 1, (short) 5, "§a§l" + new Text("mcm.gui.file.copy", lang).toString());
            this.setItem(GUIUtils.convertCounterSlot(9), copy);

            String name = dir.getName();
            //VIEW IN CHAT

            if (name.endsWith(".yml") || name.endsWith(".txt") || name.endsWith(".json") || name.endsWith(".properties") || name.contains(".log") || name.endsWith(".sh")) {
                MenuItem view;
                MenuItem edit;
                view = new MenuItem(Material.EYE_OF_ENDER, "§7§l" + new Text("mcm.gui.file.view", lang));
                edit = new MenuItem(Material.BOOK_AND_QUILL, "§7§l" + new Text("mcm.gui.file.edit", lang));
                this.setItem(GUIUtils.convertCounterSlot(1), edit);
                this.setItem(GUIUtils.convertCounterSlot(3), view);
            }
            if (name.endsWith(".zip")) {
                MenuItem unzip;
                unzip = new MenuItem(Material.DRAGON_EGG, "§a§l" + new Text("mcm.gui.file.extract", lang));
                this.setItem(GUIUtils.convertCounterSlot(10), unzip);
            }
            //RUN FILE
            if ((name.endsWith(".sh") || dir.canExecute()) && ModuleManager.isValid("file.run")) {
                MenuItem run = new MenuItem(Material.BLAZE_POWDER, "§a§l" + new Text("mcm.gui.file.run", lang));
                this.setItem(GUIUtils.convertCounterSlot(10), run);
            }

        } else {
            MenuItem black = GUIUtils.black();
            for (int i = 0; i < 9; i++) {
                setItem(i, black);
                setItem(27 + i, black);
            }
            setItem(9, black);
            setItem(18, black);
            setItem(17, black);
            setItem(26, black);
            this.setItem(31, GUIUtils.back(lang));
            this.page = page;
            if (page != 0) {
                this.setItem(30, new MenuItem(GUIUtils.previous_page(lang, page)));
            }
            setItem(4, GUIUtils.loading(lang));
            new BukkitRunnable() {
                int counter = -page * 14;

                @Override
                public void run() {
                    File[] files = dir.listFiles();
                    Arrays.sort(files, (File o1, File o2) -> o1.getAbsolutePath().compareTo(o2.getAbsolutePath()));
                    for (final File f : files) {
                        if (!ModuleManager.isFileEnabled(null, f)) {
                            continue;
                        }
                        if (counter < 0) {
                        } else if (counter == 14) {
                            setItem(32, new MenuItem(GUIUtils.next_page(lang, page + 2)));
                            break;
                        } else {
                            if (!ModuleManager.isFileEnabled(null, f)) {
                                continue;
                            }
                            String fname = f.getName();
                            Material view = Material.PAPER;
                            if (f.isDirectory()) {
                                if (f.list((File dir, String name) -> name.equalsIgnoreCase("level.dat")).length > 0) {
                                    fname = ChatColor.DARK_GREEN + "§l" + fname;
                                    view = Material.GRASS;
                                } else {
                                    fname = "§6§l" + fname;
                                    view = Material.CHEST;
                                }
                            } else if (fname.endsWith(".jar") || fname.endsWith(".zip") || fname.endsWith(".exe")) {
                                fname = "§4§l" + fname;
                                view = Material.ENDER_CHEST;
                            } else if (fname.endsWith(".sh") || fname.endsWith(".bat")) {
                                fname = "§a§l" + fname;
                            } else if (fname.endsWith(".yml") || fname.endsWith(".txt")) {
                                fname = "§e§l" + fname;
                            } else {
                                fname = "§7§l" + fname;
                            }
                            setItem(GUIUtils.convertCounterSlot(counter), new MenuItem(view, fname,
                                    new Text("mcm.gui.file.size", lang, FileUtils.byteCountToDisplaySize(FileUtils.sizeOf(f))).toString(),
                                    new Text("mcm.gui.file.last_change", lang, Utils.makeDateReadable(f.lastModified())).toString())
                            );
                        }
                        counter++;
                    }
                    String size = "Huuuuge";
                    try {
                        size = FileUtils.byteCountToDisplaySize(FileUtils.sizeOfDirectory(dir));
                    } catch (Exception ex) {

                    }
                    int f_size = dir.list().length;
                    if (f_size <= 0 || f_size > 64) {
                        f_size = 1;
                    }
                    MenuItem dirinfo = new MenuItem(Material.CHEST, f_size, (short) 0, "§6§l" + dir.getName(),
                            new Text("mcm.gui.file.size", lang, size).toString());
                    setItem(4, dirinfo);
                }
            }.runTaskAsynchronously(MCManager.getInstance());
            //MenuItem downloadFile = new MenuItem(Material.STAINED_CLAY, 1, (short) 5, new Text("mcm.gui.file.download", lang).toString());
            MenuItem createDir = new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 5, new Text("mcm.gui.file.folder", lang).toString());
            if (ModuleManager.isValid("webserver") && MCManager.getWebServer().isOnline() && ModuleManager.isValid("file.upload")) {
                setItem(27, new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 5, new Text("mcm.gui.file.upload", lang).toString()));
            }
            this.setItem(28, createDir);
            if (clipboards.containsKey(player)) {
                this.setItem(29, new MenuItem(Material.STAINED_GLASS_PANE, 1, (short) 5, new Text("mcm.gui.file.paste", lang, clipboards.get(player).getFile().getName()).toString()));
            }

            MenuItem remove = new MenuItem(Material.STAINED_GLASS_PANE, 1, BlockColor.ORANGE.toShort(), new Text("mcm.gui.file.remove", lang).toString(), new Text("mcm.gui.file.remove_warning", lang).toString());
            this.setItem(35, remove);

        }

    }

    @Override
    public void onClick(InventoryClickEvent e) {
        if (!dir.isDirectory()) {
            int slot = e.getSlot();
            final Player p = (Player) e.getWhoClicked();
            int converted = GUIUtils.convertInventorySlot(slot);
            if (converted != -1) {
                switch (converted) {
                    case 3:
                        if (e.getCurrentItem().getType() == Material.EYE_OF_ENDER) {
                            e.getView().close();
                            try {
                                List<String> list = FileUtils.readLines(dir);
                                p.sendMessage(new Text("mcm.gui.file.view_loaded", getLanguage(), list.size()).toString());
                                for (String s : list) {
                                    p.sendMessage(s);
                                }
                                TextComponent back = new TextComponent("§c§nClick here to go back");
                                back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/manager file " + dir.getPath()));
                                p.spigot().sendMessage(back);
                            } catch (IOException ex) {
                                p.sendMessage(MCManager.getPrefix() + "§cUnfortunatly, an " + ex.getClass().getName() + " occured ;(. Message: " + ex.getMessage());
                                ex.printStackTrace();
                            }
                        }
                        break;
                    case 10:
                        if (e.getCurrentItem().getType() == Material.BLAZE_POWDER) {
                            if (dir.getName().endsWith(".jar")) {
                                e.getView().close();
                                Bukkit.getScheduler().runTaskAsynchronously(MCManager.getInstance(), () -> {
                                    try {
                                        p.sendMessage("§aRunning Jar-File " + dir.getName() + "...");
                                        Process process = Runtime.getRuntime().exec("java -jar " + dir.getName(), null, dir.getParentFile());
                                        BufferedReader r = new BufferedReader(new InputStreamReader(process.getInputStream()));
                                        String line;
                                        while ((line = r.readLine()) != null) {
                                            p.sendMessage(line);
                                        }
                                        p.sendMessage("§aThe Process has ended.");
                                        TextComponent back = new TextComponent("§c§nClick here to go back");
                                        back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/manager file " + dir.getPath()));
                                        p.spigot().sendMessage(back);
                                    } catch (IOException ex) {
                                        ex.printStackTrace();
                                    }
                                });
                            } else if (dir.getName().endsWith(".sh")) {
                                e.getView().close();
                                Bukkit.getScheduler().runTaskAsynchronously(MCManager.getInstance(), new Runnable() {

                                    @Override
                                    public void run() {
                                        try {
                                            p.sendMessage("§aRunning Script " + dir.getName() + "...");
                                            Process process = Runtime.getRuntime().exec("./" + dir.getName(), null, dir.getParentFile());
                                            BufferedReader r = new BufferedReader(new InputStreamReader(process.getInputStream()));
                                            String line;
                                            while ((line = r.readLine()) != null) {
                                                p.sendMessage(line);
                                            }
                                            p.sendMessage("§aThe Process has ended.");
                                            TextComponent back = new TextComponent("§c§nClick here to go back");
                                            back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/manager file " + dir.getPath()));
                                            p.spigot().sendMessage(back);
                                        } catch (IOException ex) {
                                            ex.printStackTrace();
                                        }
                                    }
                                });
                            }
                        } else if (e.getCurrentItem().getType() == Material.DRAGON_EGG) {
                            try {
                                ZipFile zipFile = new ZipFile(dir);
                                try {
                                    Enumeration<? extends ZipEntry> entries = zipFile.entries();
                                    while (entries.hasMoreElements()) {
                                        ZipEntry entry = entries.nextElement();
                                        File entryDestination = new File(dir.getParentFile(), entry.getName());
                                        if (entry.isDirectory()) {
                                            entryDestination.mkdirs();
                                        } else {
                                            entryDestination.getParentFile().mkdirs();
                                            InputStream in = zipFile.getInputStream(entry);
                                            OutputStream out = new FileOutputStream(entryDestination);
                                            IOUtils.copy(in, out);
                                            IOUtils.closeQuietly(in);
                                            out.close();
                                        }
                                    }
                                } finally {
                                    zipFile.close();
                                }
                                new FileMenu(dir.getParentFile(), Text.getLanguage(p), p.getUniqueId(), 0).open(p);
                            } catch (Exception ex) {
                                e.getView().close();
                                p.sendMessage(MCManager.getPrefix() + "§cAn " + ex.getClass().getName() + " occured while extracting zip-file. Check the console log for details.");
                                ex.printStackTrace();
                            }
                        }
                        break;
                    case 1: //EDIT
                        if (e.getCurrentItem().getType() == Material.BOOK_AND_QUILL) {
                            e.getView().close();
                            MenuItem book = new MenuItem(Material.BOOK_AND_QUILL, "§7§lEdit " + dir.getName(), "§r" + dir.getAbsolutePath());
                            BookMeta bmeta = (BookMeta) book.getItemMeta();
                            try {
                                for (String line : FileUtils.readLines(dir, "UTF-8")) {
                                    bmeta.addPage(line);
                                }
                                book.setItemMeta(bmeta);
                                p.getInventory().addItem(book);
                                p.sendMessage(new Text("mcm.gui.file.book_hint", lang).toString());
                            } catch (IOException ex) {
                                ex.printStackTrace();
                                p.sendMessage("§cAn " + ex.getClass().getName() + " occured. Check the console log for details.");
                            }
                        }
                        break;
                    case 5: //RENAME
                        if (e.getCurrentItem().getType() == Material.NAME_TAG) {
                            if (e.getClick() == ClickType.SHIFT_LEFT || e.getClick() == ClickType.SHIFT_RIGHT) {
                                e.getView().close();
                                clipboards.put(player, new Clipboard(dir, true));
                                TextComponent con = new TextComponent(new Text("mcm.gui.file.proceed", lang).toString());
                                con.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/move " + dir.getParent() + "/<newName>"));
                                TextComponent back = new TextComponent(new Text("mcm.gui.file.back", lang).toString());
                                back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/manager file " + dir.getPath()));
                                p.spigot().sendMessage(con, back);
                            } else if (!MCManager.requestInput(p, new SignInputHandler() {
                                @Override
                                public void handleTextInput(String[] lines) {
                                    String output = lines[0] + lines[1] + lines[2] + lines[3];
                                    if (!output.isEmpty()) {
                                        try {
                                            File newF = new File(dir.getParentFile(), output);
                                            FileUtils.moveFile(dir, newF);
                                            new FileMenu(newF, lang, player, 0).open(p);
                                        } catch (Exception ex) {
                                            new NotificationPrompt(FileMenu.this.getInventory().getTitle(), NotificationPrompt.Type.ERROR, ex.getClass().getSimpleName() + ": " + ex.getMessage(), lang) {
                                                @Override
                                                public void onBack(Player p, String lang) {
                                                    onContinue(p, lang);
                                                }

                                                @Override
                                                public void onContinue(Player p, String lang) {
                                                    FileMenu.this.open(p);
                                                }
                                            }.open(p);
                                        }
                                    } else {
                                        FileMenu.this.open(p);
                                    }
                                }

                                @Override
                                public String[] getDefault() {
                                    return new String[]{dir.getName(), "", "", ""};
                                }
                            })) {
                                p.sendMessage(MCManager.getPrefix() + "§cProtocolLib is not installed!");
                                e.getView().close();
                                clipboards.put(player, new Clipboard(dir, true));
                                TextComponent con = new TextComponent(new Text("mcm.gui.file.proceed", lang).toString());
                                con.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/move " + dir.getParent() + "/<newName>"));
                                TextComponent back = new TextComponent(new Text("mcm.gui.file.back", lang).toString());
                                back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/manager file " + dir.getPath()));
                                p.spigot().sendMessage(con, back);
                            }
                        }
                        break;
                    case 9: //COPY
                        if (e.getCurrentItem().getType() == Material.STAINED_CLAY) {
                            e.setCurrentItem(new ItemStack(Material.AIR));
                            clipboards.put(player, new Clipboard(dir, false));
                        }
                        break;
                    case 11: //DOWNLOAD LINK
                        e.getView().close();
                         {
                            try {
                                p.sendMessage(MCManager.getPrefix() + "§a§n" + InetAddress.getLocalHost().getCanonicalHostName() + ":" + MCManager.getWebServer().getPort() + "/download?file=" + dir.getPath());
                            } catch (UnknownHostException ex) {
                                Logger.getLogger(FileMenu.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        break;
                    case 13:
                        if (MCManager.getConfiguration().getBoolean("file.safe_delete")) {
                            new SafetyPrompt(new Text("mcm.gui.safety", lang).toString(), lang) {
                                @Override
                                public void onApprove(boolean approve) {
                                    if (approve) {
                                        dir.delete();
                                        new FileMenu(dir.getParentFile(), getLanguage(), p.getUniqueId(), 0).open(p);
                                    } else {
                                        new FileMenu(dir, getLanguage(), p.getUniqueId(), 0).open(p);
                                    }
                                }
                            }.open(p);
                        } else {
                            dir.delete();
                            new FileMenu(dir.getParentFile(), getLanguage(), p.getUniqueId(), 0).open(p);
                        }
                        break;
                }
            } else {
                switch (slot) {
                    case 31:
                        new FileMenu(dir.getParentFile(), getLanguage(), p.getUniqueId(), 0).open(p);
                        break;
                }
            }
        } else {
            Player p = (Player) e.getWhoClicked();
            int slot = e.getSlot();
            int conv = GUIUtils.convertInventorySlot(slot);
            if (conv != -1) {
                if (e.getCurrentItem().getType() != Material.AIR) {
                    File f = new File(dir, ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()));
                    if (e.getAction().equals(InventoryAction.DROP_ONE_SLOT) && MCManager.getConfiguration().getBoolean("file.drop_delete")) {
                        if (!f.isDirectory() && ModuleManager.isFileEnabled(p, f)) {
                            f.delete();
                            new FileMenu(dir, lang, player, page).open(p);
                        }
                    } else if (ModuleManager.isFileEnabled(p, f)) {
                        if (ModuleManager.isFileEnabled(p, f)) {
                            new FileMenu(f, getLanguage(), p.getUniqueId(), 0).open(p);
                        }
                    } else {
                        GUIUtils.showNoAccess(e, lang);
                    }
                }
            } else if (slot == 27) {
                //upload
                e.getView().close();
                TextComponent proceed = new TextComponent(new Text("mcm.gui.file.upload_link", lang).toString());
                try {
                    proceed.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, "http://" + InetAddress.getLocalHost().getCanonicalHostName() + ":" + MCManager.getWebServer().getPort() + "/upload"));
                    proceed.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7http://" + InetAddress.getLocalHost().getCanonicalHostName() + ":" + MCManager.getWebServer().getPort() + "/upload").create()));
                } catch (UnknownHostException ex) {
                    MCManager.getLog().log(Level.SEVERE, null, ex);
                }
                TextComponent back = new TextComponent(new Text("mcm.gui.file.back", lang).toString());
                back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/manager file " + dir.getPath()));
                p.spigot().sendMessage(new TextComponent(MCManager.getPrefix()), proceed, back);
            } else if (slot == 28) {
                //CREATE FOLDER
                e.getView().close();
                if (!MCManager.requestInput(p, new SignInputHandler() {
                    @Override
                    public void handleTextInput(String[] lines) {
                        String newS = lines[0] + lines[1] + lines[2] + lines[3];
                        if (!newS.isEmpty()) {
                            p.performCommand("mkdir " + dir.getPath() + "/" + newS);
                            new FileMenu(new File(dir, newS), lang, player, 0).open(p);
                        } else {
                            new FileMenu(dir, lang, player, page).open(p);
                        }
                    }

                    @Override
                    public String[] getDefault() {
                        return null;
                    }
                })) {
                    TextComponent proceed = new TextComponent(new Text("mcm.gui.file.proceed", lang).toString());
                    proceed.setClickEvent(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/mkdir " + dir.getPath() + "/<name>"));
                    TextComponent back = new TextComponent(new Text("mcm.gui.file.back", lang).toString());
                    back.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/manager file " + dir.getPath()));
                    p.spigot().sendMessage(proceed, back);
                }
            } else if (slot == 29) {
                //PASTE
                if (clipboards.containsKey(player)) {
                    Clipboard c = clipboards.get(player);
                    File f = c.getFile();
                    if (!f.exists()) {
                        p.sendMessage(MCManager.getPrefix() + "§cThe File in the Clipboard does not exist!");
                    } else if (f.isDirectory()) {
                        try {
                            FileUtils.copyDirectoryToDirectory(f, dir);
                            p.sendMessage(MCManager.getPrefix() + "§aDirectory successfully copied.");
                        } catch (IOException ex) {
                            p.sendMessage(MCManager.getPrefix() + "§cFailed to copy directory: " + ex.getMessage());
                            p.sendMessage(new Text("mcm.exception.log_hint", lang).toString());
                            ex.printStackTrace();
                        }
                    } else {
                        try {
                            FileUtils.copyFileToDirectory(f, dir);
                            p.sendMessage(MCManager.getPrefix() + "§aFile successfully copied.");
                        } catch (IOException ex) {
                            p.sendMessage(MCManager.getPrefix() + "§cFailed to copy directory: " + ex.getMessage());
                            p.sendMessage(new Text("mcm.exception.log_hint", lang).toString());
                            ex.printStackTrace();
                        }
                    }
                    clipboards.remove(player);
                    new FileMenu(dir, lang, player, page).open(p);
                }
            } else if (slot == 31) {
                if (dir.getParent() != null && dir.getParentFile().getAbsolutePath().startsWith(home)) {
                    new FileMenu(dir.getParentFile(), getLanguage(), p.getUniqueId(), 0).open(p);
                } else {
                    new MainMenu(p, getLanguage()).open(p);
                }
            } else if (slot == 30) {
                if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new FileMenu(dir, getLanguage(), p.getUniqueId(), page - 1).open(p);
                }
            } else if (slot == 32) {
                if (e.getCurrentItem().getDurability() == BlockColor.WHITE.toShort()) {
                    new FileMenu(dir, getLanguage(), p.getUniqueId(), page + 1).open(p);
                }
            } else if (slot == 35) {
                if (!dir.equals(new File(home))) {
                    if (MCManager.getConfiguration().getBoolean("file.safe_delete")) {
                        new SafetyPrompt(new Text("mcm.gui.safety", lang).toString(), lang) {
                            @Override
                            public void onApprove(boolean approve) {
                                if (approve) {
                                    try {
                                        FileUtils.deleteDirectory(dir);
                                    } catch (IOException ex) {
                                        ex.printStackTrace();
                                        p.sendMessage(MCManager.getPrefix() + "§cFailed to delete directory. Check log for more information.");
                                    }
                                    if (dir.getParent() != null && dir.getParentFile().getAbsolutePath().startsWith(home)) {
                                        new FileMenu(dir.getParentFile(), getLanguage(), p.getUniqueId(), 0).open(p);
                                    } else {
                                        new MainMenu(p, getLanguage()).open(p);
                                    }
                                } else {
                                    new FileMenu(dir, lang, player, page).open(p);
                                }
                            }
                        }.open(p);
                    } else {
                        try {
                            FileUtils.deleteDirectory(dir);
                        } catch (IOException ex) {
                            ex.printStackTrace();
                            p.sendMessage(MCManager.getPrefix() + "§cFailed to delete directory. Check log for more information.");
                        }
                        if (dir.getParent() != null && dir.getParentFile().getAbsolutePath().startsWith(home)) {
                            new FileMenu(dir.getParentFile(), getLanguage(), p.getUniqueId(), 0).open(p);
                        } else {
                            new MainMenu(p, getLanguage()).open(p);
                        }
                    }
                }
            }
        }
    }

    public static class Clipboard {

        private File file;
        private boolean move;

        public Clipboard(File file, boolean move) {
            this.file = file;
            this.move = move;
        }

        public File getFile() {
            return file;
        }

        public void setFile(File file) {
            this.file = file;
        }

        public boolean isMove() {
            return move;
        }

        public void setMove(boolean move) {
            this.move = move;
        }

    }

}
