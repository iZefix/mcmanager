/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.web;

import com.betafase.mcmanager.security.ModuleManager;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.RequestContext;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class UploadHandler implements HttpHandler {

    @Override
    public void handle(final HttpExchange t) throws IOException {
        String alert = "";
        if (t.getRequestMethod().equalsIgnoreCase("POST")) {
            DiskFileItemFactory d = new DiskFileItemFactory();
            try {
                if (!ModuleManager.isValid("file.upload")) {
                    throw new SecurityException("Module is not enabled!");
                }
                ServletFileUpload up = new ServletFileUpload(d);
                List<FileItem> result = up.parseRequest(new RequestContext() {

                    @Override
                    public String getCharacterEncoding() {
                        return "UTF-8";
                    }

                    @Override
                    public int getContentLength() {
                        return 0; //tested to work with 0 as return
                    }

                    @Override
                    public String getContentType() {
                        return t.getRequestHeaders().getFirst("Content-type");
                    }

                    @Override
                    public InputStream getInputStream() throws IOException {
                        return t.getRequestBody();
                    }

                });
                String dest = "";
                FileItem file = null;
                for (FileItem fi : result) {
                    try {
                        if (fi.isFormField()) {
                            if (fi.getFieldName().equalsIgnoreCase("destination")) {
                                dest = fi.getString("UTF-8");
                            }
                        } else {
                            file = fi;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
                if (file == null || file.getSize() > 1024 * 1024 * 10) {
                    alert = "<div class=\"alert alert-danger alert-dismissable fade in\">\n"
                            + "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\n"
                            + "  <strong>Error!</strong> The File you are trying to upload is too large.\n"
                            + "</div>";
                    if (file != null) {
                        file.delete();
                    }
                } else {
                    File dir;
                    //System.out.println("[Debug] current ");
                    if (dest == null || dest.isEmpty()) {
                        dir = new File(System.getProperty("user.dir"));
                    } else {
                        dir = new File(dest);
                    }
                    File f = new File(dir, file.getName());
                    if (ModuleManager.isFileEnabled(null, f)) {
                        if (!dir.exists()) {
                            dir.mkdirs();
                        }
                        file.write(f);
                        alert = "<div class=\"alert alert-success alert-dismissable fade in\">\n"
                                + "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\n"
                                + "  <strong>Upload complete!</strong> Saved as " + f.getPath() + "\n"
                                + "</div>";
                    } else {
                        alert = "<div class=\"alert alert-danger alert-dismissable fade in\">\n"
                                + "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\n"
                                + "  <strong>Error!</strong> Access to this file has been denied.\n"
                                + "</div>";
                    }
                }
            } catch (Exception e) {
                alert = "<div class=\"alert alert-danger alert-dismissable fade in\">\n"
                        + "<a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>\n"
                        + "  <strong>Error!</strong> A " + e.getClass().getSimpleName() + " occured. Check log for details.\n"
                        + "</div>";
                e.printStackTrace();
            }
        }
        OutputStream os = t.getResponseBody();
        String html = "<!DOCTYPE html>\n"
                + "<html lang=\"en\">\n"
                + "    <head>\n"
                + "        <title>File Upload | MCManager</title>\n"
                + "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
                + "        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "        <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n"
                + "\n"
                + "        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js\"></script>\n"
                + "        <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js\"></script>\n"
                + "    </head>\n"
                + "    <body>\n"
                + "        <div class=\"container\">\n"
                + alert
                + "            <form method=\"POST\" action=\"upload\" enctype=\"multipart/form-data\" >\n"
                + "                <h3>MCManager File Upload</h3>\n"
                + "                <p>Please select the file to upload and the folder to upload to.</p>\n"
                + "                <div class=\"form-group\">\n"
                + "                    <label for=\"destination\">Upload Directory</label>\n"
                + "                    <input type=\"text\" name=\"destination\" class=\"form-control\" id=\"destination\" aria-describedby=\"destinationHelp\" placeholder=\"Upload Directory\">\n"
                + "                    <small id=\"destinationHelp\" class=\"form-text text-muted\">Enter the upload directory or leave it empty for default.</small>\n"
                + "                </div>\n"
                + "                <div class=\"form-group\">\n"
                + "                    <label for=\"file\">File</label>\n"
                + "                    <input type=\"file\" name=\"file\" class=\"form-control-file\" id=\"file\" aria-describedby=\"fileHelp\">\n"
                + "                    <small id=\"fileHelp\" class=\"form-text text-muted\">Select a file to upload, should be smaller than 10MB</small>\n"
                + "                </div>\n"
                + "                <button type=\"submit\" class=\"btn btn-primary\">Upload</button>\n"
                + "            </form>\n"
                + "        </div>\n"
                + "    </body>\n"
                + "</html>";

//            byte[] b = ("<!DOCTYPE html>\n"
//                    + "<html lang=\"en\">\n"
//                    + "    <head>\n"
//                    + "        <title>File Upload</title>\n"
//                    + "        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n"
//                    + "<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">\n"
//                    + "    </head>\n"
//                    + "    <body>\n"
//                    + "<h3>MCManager File Upload</h3>\n"
//                    + "<p>Please select the file to upload and the folder to upload to.</p>\n"
//                    + "        <form method=\"POST\" action=\"upload\" enctype=\"multipart/form-data\" >\n"
//                    + "            File:\n"
//                    + "            <input type=\"file\" name=\"file\" id=\"file\" /> <br/>\n"
//                    + "            Destination:\n"
//                    + "            <input type=\"text\" value=\"/tmp\" name=\"destination\"/>\n"
//                    + "            </br>\n"
//                    + "<button type=\"submit\" class=\"btn btn-primary\">Upload</button>\n"
//                    //+ "            <input type=\"submit\" value=\"Upload\" name=\"upload\" id=\"upload\" />\n"
//                    + "        </form>\n"
//                    + "    </body>\n"
//                    + "</html>").getBytes("UTF-8");
        byte[] b = html.getBytes("UTF-8");
        t.getResponseHeaders().add("Content-type", "text/html");
        t.sendResponseHeaders(200, b.length);
        os.write(b);
        os.close();
    }
}
