/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.web;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.api.ExecutableMenuItem;
import com.betafase.mcmanager.api.SimpleExecutableMenuItem;
import com.betafase.mcmanager.api.SimpleMenu;
import com.betafase.mcmanager.api.GUIUtils;
import com.betafase.mcmanager.api.Menu;
import com.betafase.mcmanager.api.MenuItem;
import com.betafase.mcmanager.gui.plugin.PluginMenu;
import com.betafase.mcmanager.utils.Text;
import java.util.Map;
import java.util.Map.Entry;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import com.betafase.mcmanager.api.SignInputHandler;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class WebManager implements ExecutableMenuItem {

    @Override
    public MenuItem getDisplayItem(String lang) {
        return new MenuItem(Material.EMERALD, "§7§lWebServer");
    }

    @Override
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        new WebMenu(Text.getLanguage(p)).open(p);
    }

    private class WebMenu extends SimpleMenu {

        public WebMenu(String lang) {
            super("§9WebServer", 9, lang);
            setItem(1, new ExecutableMenuItem() {
                @Override
                public void onClick(InventoryClickEvent e) {

                }

                @Override
                public MenuItem getDisplayItem(String lang) {
                    return new MenuItem(Material.EMERALD, "§7§lWebServer", "§8Status: " + (MCManager.getWebServer().isOnline() ? "§aONLINE" : "§cOFFLINE"));
                }
            });
            setItem(3, new ExecutableMenuItem() {
                @Override
                public void onClick(InventoryClickEvent e) {
                    Player p = (Player) e.getWhoClicked();
                    if (MCManager.getWebServer().isOnline()) {
                        MCManager.getWebServer().stop();
                    } else {
                        MCManager.getWebServer().start(MCManager.getConfiguration().getInt("webserver.port", 8282));
                    }
                    new WebMenu(lang).open(p);
                }

                @Override
                public MenuItem getDisplayItem(String lang) {
                    if (MCManager.getWebServer().isOnline()) {
                        return new MenuItem(Material.STAINED_CLAY, 1, (short) 6, "§c§lStop");
                    } else {
                        return new MenuItem(Material.STAINED_CLAY, 1, (short) 5, "§a§lStart");
                    }
                }
            });
            setItem(4, new ExecutableMenuItem() {
                @Override
                public void onClick(InventoryClickEvent e) {
                    Player p = (Player) e.getWhoClicked();
                    new AccountsMenu(Text.getLanguage(p), 0).open(p);
                }

                @Override
                public MenuItem getDisplayItem(String lang) {
                    return new MenuItem(Material.NAME_TAG, "§7§lAccounts");
                }
            });
            setItem(8, new SimpleExecutableMenuItem(GUIUtils.backOld(lang)) {
                @Override
                public void onClick(InventoryClickEvent e) {
                    new PluginMenu(MCManager.getInstance(), lang).open((Player) e.getWhoClicked());
                }
            });
        }

    }

    private class AccountsMenu extends Menu {

        private int page;
        ConfigurationSection section = MCManager.getConfiguration().getConfigurationSection("webserver.accounts");

        public AccountsMenu(String lang, int page) {
            super("§7Manage Accounts", 27, lang);
            this.page = page;
            if (page > 0) {
                this.setItem(21, GUIUtils.previous_pageOld(lang, page));
            }
            int counter = -page * 18;
            this.setItem(22, GUIUtils.backOld(lang));
            Map<String, Object> values = section.getValues(false);
            for (Entry<String, Object> e : values.entrySet()) {
                if (counter < 0) {
                } else if (counter == 18) {
                    this.setItem(23, GUIUtils.next_pageOld(lang, page + 2));
                    break;
                } else {
                    MenuItem account = new MenuItem(Material.NAME_TAG, "§7§l" + e.getKey(),
                            "§8Password: §e" + e.getValue(),
                            "",
                            "§a» Click to delete");
                    this.setItem(counter, account);
                }
                counter++;
            }
            if (counter >= 0 && counter < 18) {
                MenuItem add = new MenuItem(Material.STAINED_CLAY, 1, (short) 5, "§a§lAdd...", "§8First Line: username, Second Line: password");
                this.setItem(counter, add);
            }
        }

        @Override
        public void onClick(InventoryClickEvent e) {
            Player p = (Player) e.getWhoClicked();
            if (e.getSlot() < 18) {
                if (e.getCurrentItem().getType() == Material.NAME_TAG) {
                    String key = ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName());
                    section.set(key, null);
                    MCManager.saveConfiguration();
                    new AccountsMenu(lang, page).open(p);
                } else if (e.getCurrentItem().getType() == Material.STAINED_CLAY) {
                    e.getView().close();
                    if (!MCManager.requestInput(p, new SignInputHandler() {
                        @Override
                        public void handleTextInput(String[] lines) {
                            if (lines[0].isEmpty() || lines[1].isEmpty()) {
                                p.sendMessage("§cYou didn't enter a text. If you do this again, the plugin will explode.");
                                return;
                            }
                            String user = lines[0];
                            if (section.contains(user)) {
                                p.sendMessage("§cOoops. It looks like the user you are trying to create already exists. Time travelling is dangerous.");
                                return;
                            }
                            section.set(user, lines[1]);
                            if (!lines[2].isEmpty() || !lines[3].isEmpty()) {
                                p.sendMessage("§eWhy did you enter text here? Suspicious.");
                            }
                            new AccountsMenu(lang, page).open(p);
                        }

                        @Override
                        public String[] getDefault() {
                            return new String[]{"<username>", "<password>", "", ""};
                        }
                    })) {
                        p.sendMessage("§cThis would normally open a Sign GUI. It seems like you don't have ProtocolLib installed. If you don't wish to install it, you can edit the accounts in the config, aswell.");
                    }
                }
            } else {
                switch (e.getSlot()) {
                    case 22:
                        new WebMenu(lang).open(p);
                        break;
                    case 21:
                        if (e.getCurrentItem().getType() == Material.STICK) {
                            new AccountsMenu(lang, page - 1).open(p);
                        }
                        break;
                    case 23:
                        if (e.getCurrentItem().getType() == Material.STICK) {
                            new AccountsMenu(lang, page + 1).open(p);
                        }
                        break;
                }
            }
        }

    }

}
