/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.web;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.Map;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class DownloadHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        OutputStream os = httpExchange.getResponseBody();
        try {
            Map<String, String> args = WebServer.getQueryMap(httpExchange.getRequestURI().getQuery());
            if (args.containsKey("file")) {
                File f = new File(args.get("file"));
                if (!f.getAbsolutePath().startsWith(System.getProperty("user.dir"))) {
                    throw new SecurityException("Access denied.");
                }
                if (f.exists() && !f.isDirectory()) {
                    final byte out[] = Files.readAllBytes(f.toPath());
                    httpExchange.getResponseHeaders().add("Content-Disposition", "attachment; filename=" + f.getName());
                    httpExchange.sendResponseHeaders(200, out.length);
                    os.write(out);
                } else {
                    byte[] out = "This File does not exist!".getBytes("UTF-8");
                    httpExchange.sendResponseHeaders(404, out.length);
                    os.write(out);
                }
            } else {
                byte[] out = "Missing argument 'file'".getBytes("UTF-8");
                httpExchange.sendResponseHeaders(400, out.length);
                os.write(out);
            }
        } catch (Exception ex) {
            byte[] out = "An Error occured. See console log for details.".getBytes("UTF-8");
            httpExchange.sendResponseHeaders(500, out.length);
            os.write(out);
            ex.printStackTrace();
        }
        httpExchange.close();
    }

}
