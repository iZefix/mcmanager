/*
 * Copyright 2018 dbrosch.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.betafase.mcmanager.web;

import com.betafase.mcmanager.MCManager;
import com.sun.net.httpserver.BasicAuthenticator;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.ConfigurationSection;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class WebServer {
    
    private HttpServer server;
    private int port;
    private EasyAuth auth;
    
    public WebServer() {
    }
    
    public int getPort() {
        return port;
    }
    
    public boolean isOnline() {
        return server != null;
    }

    //Credit : @verisimilitude
    public static Map<String, String> getQueryMap(String query) throws UnsupportedEncodingException {
        Map<String, String> map = new HashMap<>();
        if (query != null && !query.isEmpty()) {
            String[] params = query.split("&");
            for (String param : params) {
                String name = param.split("=")[0];
                String value = param.split("=")[1];
                map.put(name, URLDecoder.decode(value, "UTF-8"));
            }
        }
        return map;
    }
    
    public void start(int port) {
        if (server != null) {
            stop();
        }
        try {
            auth = new EasyAuth("Please log in to use this service");
            this.port = port;
            server = HttpServer.create(new InetSocketAddress(port), 0);
            HttpContext download = server.createContext("/download");
            HttpContext upload = server.createContext("/upload");
            upload.setHandler(new UploadHandler());
            upload.setAuthenticator(auth);
            download.setHandler(new DownloadHandler());
            download.setAuthenticator(auth);
            server.start();
        } catch (Exception ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.WARNING, "Failed to start WebServer", ex);
        }
    }
    
    public void stop() {
        if (server != null) {
            server.stop(0);
            server = null;
        }
    }
    
    public HttpContext addProtectedContext(String context, HttpHandler handler) {
        HttpContext http = server.createContext(context);
        http.setAuthenticator(auth);
        http.setHandler(handler);
        return http;
    }
    
    private class EasyAuth extends BasicAuthenticator {
        
        public EasyAuth(String string) {
            super(string);
        }
        
        @Override
        public boolean checkCredentials(String string, String string1) {
            if (MCManager.getInstance().getConfig().isConfigurationSection("webserver.accounts")) {
                ConfigurationSection users = MCManager.getInstance().getConfig().getConfigurationSection("webserver.accounts");
                //System.out.println("[Debug] Testing Access for " + string + " with pass " + string1);
                return users.contains(string) && users.getString(string).equals(string1);
            }
            return true;
        }
        
    }
    
}
