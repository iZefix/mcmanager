/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.listener;

import com.betafase.mcmanager.MCManager;
import java.io.File;
import java.util.Collection;
import java.util.LinkedList;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.io.FileUtils;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

/**
     * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class BookHandler implements Listener {

    @EventHandler
    public void onBookClick(PlayerEditBookEvent e) {
        BookMeta previous = e.getPreviousBookMeta();
        if (previous.hasDisplayName() && previous.getDisplayName().startsWith("§7§lEdit ") && previous.hasLore() && e.getPlayer().isOp()) {
            e.getPlayer().getInventory().setItem(e.getSlot(), new ItemStack(Material.AIR));
            BookMeta newMeta = e.getNewBookMeta();
            if (e.isSigning()) {
                try {
                    Collection<String> col = new LinkedList<>();
                    for (String page : newMeta.getPages()) {
                        col.add(ChatColor.stripColor(page));
                    }
                    File f = new File(ChatColor.stripColor(previous.getLore().iterator().next()));
                    FileUtils.writeLines(f, col, false);
                    e.getPlayer().sendMessage(MCManager.getPrefix() + "§aYour changes have been saved.");
                } catch (Exception ex) {
                    ex.printStackTrace();
                    e.getPlayer().sendMessage(MCManager.getPrefix() + "§cFailed to edit file: " + ex.getMessage());
                    e.getPlayer().sendMessage(MCManager.getPrefix() + "§7Check log for detailed information.");
                }
            } else {
                e.getPlayer().sendMessage(MCManager.getPrefix() + "§cNo data was changed.");
            }
        }
    }

    @EventHandler
    public void onBookDrop(PlayerDropItemEvent e) {
        if (e.getItemDrop().getItemStack().getType() == Material.BOOK_AND_QUILL) {
            BookMeta previous = (BookMeta) e.getItemDrop().getItemStack().getItemMeta();
            if (previous.hasDisplayName() && previous.getDisplayName().startsWith("§7§lEdit ") && previous.hasLore()) {
                e.getPlayer().sendMessage(MCManager.getPrefix() + "§cNo data was changed.");
                e.getItemDrop().remove();
            }
        }
    }
}
