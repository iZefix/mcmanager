/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.command;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.gui.FileMenu;
import com.betafase.mcmanager.gui.player.MainPlayerMenu;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author dbrosch
 */
public class QuickCommandHandler implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (cs instanceof Player) {
            Player p = (Player) cs;
            String lang = Text.getLanguage(p);
            if (label.equalsIgnoreCase("files")) {
                if (ModuleManager.isValid(p, "file")) {
                    new FileMenu(new File(System.getProperty("user.dir")), lang, p.getUniqueId(), 0).open(p);
                } else {
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", lang));
                }
            } else if (label.equalsIgnoreCase("players")) {
                if (ModuleManager.isValid(p, "player")) {
                    new MainPlayerMenu(lang, 0, false).open(p);
                } else {
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", lang));
                }
            } else if (label.equalsIgnoreCase("log")) {
                if (ModuleManager.isValid(p, "log")) {
                    File logFile = new File("latest.log");
                    if (!logFile.exists()) {
                        logFile = new File("logs", "latest.log");
                    }
                    if (logFile.exists()) {
                        try {
                            BufferedReader r = new BufferedReader(new FileReader(logFile));
                            String line;
                            p.sendMessage("§a» Displaying " + logFile.getPath() + " «");
                            while ((line = r.readLine()) != null) {
                                p.sendMessage(line);
                            }
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(QuickCommandHandler.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (IOException ex) {
                            Logger.getLogger(QuickCommandHandler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        p.sendMessage(MCManager.getPrefix() + "§cNo logfile found. Is logging disabled?");
                    }
                } else {
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", lang));
                }
            } else if (label.equalsIgnoreCase("backup")) {
                if (ModuleManager.isValid(p, "backup")) {
                    MCManager.getBackupManager().backupData();
                } else {
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", lang));
                }
            } else {
                return false;
            }
            return true;
        }
        return false;
    }

}
