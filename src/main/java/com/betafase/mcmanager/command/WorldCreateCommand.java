/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.command;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.gui.world.WorldCreateMenu;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import java.util.LinkedList;
import java.util.List;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.WorldType;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

/**
 *
 * @author dbrosch
 */
public class WorldCreateCommand implements CommandExecutor, TabCompleter {

    @Override
    public boolean onCommand(CommandSender cs, Command arg1, String arg2, String[] args) {
        if (!(cs instanceof Player)) {
            cs.sendMessage("Command line is not supported for this command ;(");
            return true;
        }
        Player p = (Player) cs;
        if (!ModuleManager.isValid(p, "world.create")) {
            p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", Text.getLanguage(p)).toString());
        } else if (args.length == 0) {
            p.sendMessage(new Text("mcm.command.create_world.usage", Text.getLanguage(p)).toString());
        } else {
            WorldCreator c = new WorldCreator(args[0]);
            for (int i = 1; i < args.length; i++) {
                if (args[i].equalsIgnoreCase("-S") && args.length > i + 1) {
                    try {
                        c.seed(Long.parseLong(args[i + 1]));
                    } catch (Exception ex) {
                        p.sendMessage(MCManager.getPrefix() + "§e-S: Parameter must be a number.");
                    }
                    i++;
                } else if (args[i].equalsIgnoreCase("-t") && args.length > i + 1) {
                    try {
                        c.type(WorldType.valueOf(args[i + 1]));
                    } catch (Exception ex) {
                        p.sendMessage(MCManager.getPrefix() + "§e-t: Type not found. Available types: " + ArrayUtils.toString(WorldType.values()));
                    }
                    i++;
                } else if (args[i].equalsIgnoreCase("-g") && args.length > i + 1) {
                    c.generator(args[i + 1], cs);
                    i++;
                } else if (args[i].equalsIgnoreCase("-gs") && args.length > i + 1) {
                    c.generatorSettings(args[i + 1]);
                    i++;
                } else if (args[i].equalsIgnoreCase("-e") && args.length > i + 1) {
                    try {
                        c.environment(Environment.valueOf(args[i + 1]));
                    } catch (Exception ex) {
                        p.sendMessage(MCManager.getPrefix() + "§e-t: Type not found. Available types: " + ArrayUtils.toString(Environment.values()));
                    }
                    i++;
                } else {
                    p.sendMessage(MCManager.getPrefix() + "§eUnidentified Parameter: " + args[i]);
                }
            }
            new WorldCreateMenu(c, Text.getLanguage(p)).open(p);
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender cs, Command cmd, String label, String[] args) {
        LinkedList<String> list = new LinkedList<>();
        if (args.length >= 1) {
            String previous = args[args.length - 2];
            String current = args[args.length - 1];
            if (previous.equalsIgnoreCase("-e")) {
                for (Environment e : Environment.values()) {
                    if (e.name().startsWith(current)) {
                        list.add(e.name());
                    }
                }
            } else if (previous.equalsIgnoreCase("-t")) {
                for (WorldType t : WorldType.values()) {
                    if (t.name().startsWith(current)) {
                        list.add(t.name());
                    }
                }
            } else if (current.startsWith("-") && !previous.equalsIgnoreCase("-s")) {
                list.add("-t");
                list.add("-gs");
                list.add("-s");
                list.add("-e");
                list.add("-g");
            }
        }
        return list;
    }

}
