/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.command;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class WgetCommand implements CommandExecutor {

    private static final int BUFFER_SIZE = 4096;
    private static BukkitTask download = null;

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] args) {
        if (args.length == 0) {
            return false;
        } else if (args[0].equalsIgnoreCase("cancel")) {
            if (download != null) {
                download.cancel();
                download = null;
                cs.sendMessage(MCManager.getPrefix() + "§aThe download has been cancelled.");
            } else {
                cs.sendMessage(MCManager.getPrefix() + "§cThere is no download active which could be cancelled.");
            }
        } else if (args.length == 2) {
            if (!(cs instanceof Player)) {
                return true;
            }
            final Player p = (Player) cs;
            if (!ModuleManager.isValid(p, "wget")) {
                p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", Text.getLanguage(p)).toString());
                return true;
            }
            if (download == null) {
                final String dir;
                if (args[0].equalsIgnoreCase("home")) {
                    dir = System.getProperty("user.dir");
                } else {
                    dir = args[0];
                }
                final String input = args[1];
                final String lang = Text.getLanguage(p);
                final File f = new File(dir);
                if (!f.getAbsolutePath().startsWith(System.getProperty("user.dir"))) {
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.wget.access_denied", lang).toString());
                    return true;
                }
                if (!ModuleManager.isFileEnabled(p, f)) {
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.wget.access_denied", lang).toString());
                    return true;
                }
                f.mkdirs();
                download = Bukkit.getScheduler().runTaskAsynchronously(MCManager.getInstance(), () -> {
                    downloadFile(input, dir, p);
                });
            } else {
                p.sendMessage(MCManager.getPrefix() + "§cDownload in progress. Please wait for it to finish or use '/wget cancel'");
            }
        } else {

        }

        return true;
    }

    public static void downloadFile(String fileURL, String saveDir, Player notifier) {
        try {
            if (notifier != null) {
                notifier.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent("§ePLEASE WAIT§8- §7Preparing Download..."));
            }
            URL url = new URL(fileURL);
            HttpURLConnection httpConn = (HttpURLConnection) url.openConnection();
            if (fileURL.contains("betafase.com")) {
                httpConn.setRequestProperty("User-Agent", "PluginInstaller");
            } else {
                httpConn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");

            }
            httpConn.setInstanceFollowRedirects(true);
            boolean redirect = false;

            // normally, 3xx is redirect
            int status = httpConn.getResponseCode();
            int cou = 1;
            while (httpConn.getResponseCode() != 200) {
                Map<String, List<String>> map = httpConn.getHeaderFields();
                for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                    System.out.println("Key : " + entry.getKey()
                            + " ,Value : " + entry.getValue());
                }
                if (status != HttpURLConnection.HTTP_OK) {
                    if (status == HttpURLConnection.HTTP_MOVED_TEMP
                            || status == HttpURLConnection.HTTP_MOVED_PERM
                            || status == HttpURLConnection.HTTP_SEE_OTHER) {
                        redirect = true;
                    } else if (notifier != null) {
                        if (httpConn.getResponseCode() == 503) {
                            notifier.sendMessage("§c" + url.getAuthority() + " is currently in Cloudflare Attack mode. This denies bots from downloading files.");
                            TextComponent d = new TextComponent("§6» Download manually");
                            d.setClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, url.toString()));
                            notifier.spigot().sendMessage(d);
                        } else {
                            notifier.sendMessage("§cFailed to download file. Server replied HTTP code " + status + ".");
                        }
                        httpConn.disconnect();
                        download = null;
                        return;
                    } else {
                        MCManager.getLog().log(Level.SEVERE, "No file to download. Server replied HTTP code: {0}", status);
                        httpConn.disconnect();
                        download = null;
                        return;
                    }
                    cou++;
                    if (cou == 10) {
                        MCManager.getLog().log(Level.SEVERE, "No file to download after 10 attempts. Server replied HTTP code: {0}", status);
                        httpConn.disconnect();
                        download = null;
                        return;
                    }
                }
                if (redirect) {
                    if (notifier != null) {
                        notifier.sendMessage("§aPlease wait... Redirecting...");
                    }
                    // get redirect url from "location" header field
                    String newUrl = httpConn.getHeaderField("Location");
                    if (newUrl == null) {
                        continue;
                    }
                    // open the new connnection again
                    httpConn = (HttpURLConnection) new URL(newUrl).openConnection();
                    httpConn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");

                    MCManager.getLog().log(Level.INFO, "Redirecting to URL : {0}", newUrl);
                }
            }
            String contentType = httpConn.getContentType();
            long contentLength = httpConn.getContentLength();
            File f;
            File save = new File(saveDir);
            if (!save.isDirectory()) {
                f = save;
            } else {
                String disposition = httpConn.getHeaderField("Content-Disposition");
                String fileName = "";

                if (disposition != null) {
                    //  ; filename=blupp
                    int index = disposition.indexOf("filename=");
                    if (index > 0) {
                        fileName = disposition.substring(index + 10,
                                disposition.length() - 1);
                    }
                } else {
                    // get ending
                    fileName = fileURL.substring(fileURL.lastIndexOf("/") + 1,
                            fileURL.length());
                }
                f = new File(save, fileName);
            }

            /*
            System.out.println("Content-Type = " + contentType);
            System.out.println("Content-Disposition = " + disposition);
            System.out.println("Content-Length = " + contentLength);
            System.out.println("fileName = " + fileName);
             */
            //delete if exists to avoid errors
            //TODO add warning
            if (f.exists()) {
                //Thread.sleep(4000);
                f.delete();
            }
            InputStream inputStream = httpConn.getInputStream();
            FileOutputStream outputStream = new FileOutputStream(f.getPath());
            int bytesRead = -1;
            long totalDataRead = 0;
            int counter = 0;
            byte[] buffer = new byte[BUFFER_SIZE];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
                totalDataRead = totalDataRead + buffer.length;
                if (counter == 1000) {
                    int progress = (int) (totalDataRead / contentLength * 100);
                    if (progress >= 100) {
                        progress = 99;
                    }
                    if (notifier != null) {
                        notifier.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent("§6" + progress + "% §8- §7Downloading from " + url.getAuthority()));
                    } else {
                        System.out.println(progress + "%");
                    }
                    counter = 0;
                } else {
                    counter++;
                }
            }
            outputStream.close();
            inputStream.close();
            if (notifier != null) {
                notifier.spigot().sendMessage(ChatMessageType.ACTION_BAR, new TextComponent("§aCOMPLETE §8- §7Saved as " + f.getName()));
            } else {
                System.out.println("File downloaded.");
            }
            if (saveDir.contains("plugins") && f.getName().endsWith(".jar")) {
                Plugin pl;
                try {
                    pl = Bukkit.getPluginManager().loadPlugin(f);
                    if (notifier != null) {
                        TextComponent c1 = new TextComponent("§7You need to reload the server in order §7to use §7§l" + pl.getName() + " " + pl.getDescription().getVersion() + ". §7Reload §7now?");
                        TextComponent c2 = new TextComponent("§a§l[YES]");
                        c2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reload"));
                        TextComponent c3 = new TextComponent("  §c§l[NO]");
                        notifier.spigot().sendMessage(c1, c2, c3);
                    }
                } catch (Exception ex) {
                    if (ex.getMessage() != null && ex.getMessage().contains("initialized")) {
                        if (notifier != null) {
                            TextComponent c1 = new TextComponent("§7The Update has been downloaded. §7Install & Reload Server §7now?");
                            TextComponent c2 = new TextComponent("§a§l[YES]");
                            c2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reload"));
                            TextComponent c3 = new TextComponent("  §c§l[NO]");
                            notifier.spigot().sendMessage(c1, c2, c3);
                        }
                    } else {
                        if (notifier != null) {
                            notifier.sendMessage("§cFailed to enable Plugin: " + ex.getClass().getName() + ". See console log for more information.");
                            TextComponent c1 = new TextComponent("§7You might try to reload the server to fix this. §7Reload §7now?");
                            TextComponent c2 = new TextComponent("§a§l[YES]");
                            c2.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/reload"));
                            TextComponent c3 = new TextComponent("  §c§l[NO]");
                            notifier.spigot().sendMessage(c1, c2, c3);
                        }
                        ex.printStackTrace();
                    }

                }
            }
            httpConn.disconnect();
        } catch (Exception ex) {
            ex.printStackTrace();
            if (notifier != null) {
                notifier.sendMessage("§cAn error occured while downloading file: " + ex.getClass().getName() + ". See console log for more information.");
            }
        }
        download = null;
    }
}
