/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.command;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.gui.FileMenu;
import com.betafase.mcmanager.gui.MainMenu;
import com.betafase.mcmanager.gui.player.MainPlayerMenu;
import com.betafase.mcmanager.gui.plugin.MainPluginMenu;
import com.betafase.mcmanager.gui.player.PlayerMenu;
import com.betafase.mcmanager.gui.plugin.PluginMenu;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.CommandExecuter;
import com.betafase.mcmanager.utils.Text;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.logging.Level;
import org.apache.commons.io.FileUtils;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * © Betafase Developers, 2015
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class MainCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] args) {
        if (cs instanceof Player) {
            Player p = (Player) cs;
            if (!ModuleManager.isValid(p, "use")) {
                p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", Text.getLanguage(p)).toString());
            } else if (args.length == 0) {
                new MainMenu(p, Text.getLanguage(p)).open(p);
            } else if (args[0].equalsIgnoreCase("file")) {
                if (args.length >= 1) {
                    if (ModuleManager.isValid(p, "file")) {
                        if (args.length == 1) {
                            new FileMenu(new File(System.getProperty("user.dir")), Text.getLanguage(p), p.getUniqueId(), 0).open(p);
                        } else {
                            File f = new File(args[1]);
                            if (!ModuleManager.isFileEnabled(p, f)) {
                                p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", Text.getLanguage(p)).toString());
                            } else {
                                new FileMenu(f, Text.getLanguage(p), p.getUniqueId(), 0).open(p);
                            }
                        }
                    } else {
                        p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", Text.getLanguage(p)).toString());
                    }
                }
            } else if (args[0].equalsIgnoreCase("player")) {
                if (args.length >= 1) {
                    if (ModuleManager.isValid(p, "player")) {
                        if (args.length == 1) {
                            new MainPlayerMenu(Text.getLanguage(p), 0, false).open(p);
                        } else {
                            OfflinePlayer op = Bukkit.getOfflinePlayer(args[1]);
                            if (op != null) {
                                new PlayerMenu(op, Text.getLanguage(p)).open(p);
                            }
                        }
                    } else {
                        p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", Text.getLanguage(p)).toString());
                    }
                }
            } else if (args[0].equalsIgnoreCase("plugin")) {
                if (ModuleManager.isValid(p, "plugin")) {
                    if (args.length == 1) {
                        new MainPluginMenu(Text.getLanguage(p), 0).open(p);
                    } else {
                        Plugin pl = Bukkit.getPluginManager().getPlugin(args[1]);
                        if (pl != null) {
                            new PluginMenu(pl, Text.getLanguage(p)).open(p);
                        }
                    }
                } else {
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", Text.getLanguage(p)).toString());
                }
            } else if (args[0].equalsIgnoreCase("language")) {
                if (args.length == 2) {
                    String new_lang = args[1];
                    MCManager.getConfiguration().set("language", new_lang);
                    MCManager.saveConfiguration();
                    MCManager.getInstance().reloadLanguage();
                    p.sendMessage(MCManager.getPrefix() + "§aLanguage has been changed to " + new_lang);
                } else {
                    p.sendMessage(MCManager.getPrefix() + "§aAvailable Languages: en_US, de_DE, <custom file path>. Visit §nhttps://bitbucket.org/iZefix/mcmanager/wiki/Language§r§a for more information.");
                }
            } else if (args[0].equalsIgnoreCase("backup")) {
                if (ModuleManager.isValid(p, "backup")) {
                    switch (args.length) {
                        case 1:
                            Bukkit.getScheduler().runTaskAsynchronously(MCManager.getInstance(), MCManager.getBackupManager()::backupData);
                            break;
                        case 2:
                            if (args[1].equalsIgnoreCase("restart")) {
                                MCManager.getBackupManager().restart();
                                break;
                            }
                        default:
                            p.sendMessage(MCManager.getPrefix() + "§cThis Subcommand is not vaild.");
                            break;
                    }
                } else {
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", Text.getLanguage(p)).toString());
                }
            } else if (args[0].equalsIgnoreCase("usr")) {
                if (args.length == 2) {
                    try {
                        File update = new File("SpigotUpdater", args[1]);
                        File current = new File(Bukkit.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath());
                        if (update.exists() && current.exists()) {
                            current.delete();
                            FileUtils.moveFile(update, current);
                            Bukkit.spigot().restart();
                        } else {
                            Text.reportError(new FileNotFoundException("Update or current File does not exist!"), Text.getLanguage(p));
                        }
                    } catch (URISyntaxException ex) {
                        Text.reportError(ex, Text.getLanguage(p));
                        MCManager.getLog().log(Level.SEVERE, null, ex);
                    } catch (IOException ex) {
                        Text.reportError(ex, Text.getLanguage(p));
                        MCManager.getLog().log(Level.SEVERE, null, ex);
                    }
                }
            } else if (CommandExecuter.items.containsKey(args[0])) {
                if (CommandExecuter.items.get(args[0]).onExecute(p)) {
                    CommandExecuter.items.remove(args[0]);
                }
            } else {
                p.sendMessage(MCManager.getPrefix() + "§cThis Command is not vaild.");
            }
        } else if (ModuleManager.isValid("backup")) {
            if (args.length == 1 && args[0].equalsIgnoreCase("backup")) {
                MCManager.getBackupManager().backupData();
            }
        } else {
            cs.sendMessage("Command line is currently only supported for the Backup Command.");
        }
        return true;
    }
}
