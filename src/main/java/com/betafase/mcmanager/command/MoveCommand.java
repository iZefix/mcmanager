/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.command;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.gui.FileMenu;
import com.betafase.mcmanager.gui.FileMenu.Clipboard;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import java.io.File;
import org.apache.commons.io.FileUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class MoveCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] args) {
        if (cs instanceof Player) {
            Player p = (Player) cs;
            if (!ModuleManager.isValid(p, "file")) {
                p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", Text.getLanguage(p)).toString());
            } else if (!FileMenu.clipboards.containsKey(p.getUniqueId())) {
                p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.move.empty_clipboard", Text.getLanguage(p)).toString());
            } else if (args.length != 1) {
                p.sendMessage(MCManager.getPrefix() + "§c/move <newfile>");
            } else {
                File f = new File(args[0]);
                if (!ModuleManager.isFileEnabled(p, f)) {
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", Text.getLanguage(p)).toString());
                    return true;
                }
                Clipboard c = FileMenu.clipboards.get(p.getUniqueId());
                File board = c.getFile();
                try {
                    if (!board.exists()) {
                        throw new NullPointerException("File does not exist!");
                    } else if (board.isDirectory()) {
                        FileUtils.moveDirectory(board, f);
                        p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.move.directory_success", Text.getLanguage(p)));
                    } else {
                        FileUtils.moveFile(board, f);
                        p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.move.file_success", Text.getLanguage(p)));
                    }
                } catch (Exception ex) {
                    p.sendMessage(MCManager.getPrefix() + "§cAn error occured: " + ex.getMessage());
                    ex.printStackTrace();
                }
            }
        }
        return true;
    }

}
