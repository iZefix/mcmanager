/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.betafase.mcmanager.command;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.gui.plugin.AddPluginMenu;
import com.betafase.mcmanager.gui.SpigotMenu;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import com.betafase.mcmanager.utils.spiget.PluginInfoRequest;
import com.betafase.mcmanager.utils.spiget.SpigetPlugin;
import java.util.HashMap;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author dbrosch
 */
public class SearchPluginCommand implements CommandExecutor {
    
    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] args) {
        if (!(cs instanceof Player)) {
            return false;
        }
        Player p = (Player) cs;
        String lang = Text.getLanguage(p);
        if (!ModuleManager.isValid(p, "spiget")) {
            p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", lang).toString());
        } else if (args.length == 0) {
            p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.searchplugin.usage", lang));
        } else {
            StringBuilder b = new StringBuilder(args[0]);
            for (int i = 1; i < args.length; i++) {
                b.append(" ");
                b.append(args[i]);
            }
            new SpigotMenu(lang, 0, new SpigotMenu.PluginDataLoader() {
                @Override
                public SpigetPlugin[] loadData() {
                    return new PluginInfoRequest("search/resources/" + b.toString(), new HashMap<String, String>() {
                        {
                            put("size", Integer.toString(50));
                            put("sort", "-downloads");
                        }
                    }).getPlugins();
                }
                
                @Override
                public void onBack(Player p) {
                    new AddPluginMenu(lang).open(p);
                }
            }).open(p);
        }
        return true;
    }
    
}
