/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.command;

import com.betafase.mcmanager.api.ConfigChangeEvent;
import com.betafase.mcmanager.gui.ConfigMenu;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class ConfigCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] args) {
        // /config plugin section/value
        if (!(cs instanceof Player)) {
            return true;
        }
        Player p = (Player) cs;
        String lang = Text.getLanguage(p);
        if (!ModuleManager.isValid(p, "config")) {
            p.sendMessage(new Text("mcm.command.no_permission", lang).toString());
        } else if (args.length == 0) {
            p.sendMessage(new Text("mcm.command.config.usage", lang).toString());
        } else if (args.length == 1) {
            Plugin pl = Bukkit.getPluginManager().getPlugin(args[0]);
            if (pl != null) {
                new ConfigMenu(pl, pl.getConfig(), 0, lang).open(p);
            }
        } else if (args.length >= 2) {
            boolean value = args.length == 2;
            String key = args[1];
            if (key.startsWith(".")) {
                key = key.substring(1);
            }
            Plugin pl = Bukkit.getPluginManager().getPlugin(args[0]);
            if (pl != null && pl.isEnabled()) {
                if (pl.getName().equalsIgnoreCase("MCManager") && !ModuleManager.isValid(p, "mcmanager")) {
                    p.sendMessage(new Text("mcm.command.no_permission", lang).toString());
                    return true;
                }
                FileConfiguration cfg = pl.getConfig();
                if (cfg.contains(key)) {
                    if (cfg.isConfigurationSection(key)) {
                        new ConfigMenu(pl, cfg.getConfigurationSection(key), 0, lang).open(p);
                    } else if (cfg.isString(key)) {
                        if (value) {
                            p.sendMessage("Current Value: " + cfg.getString(key));
                        } else {
                            StringBuilder b = new StringBuilder(args[2]);
                            if (args.length > 3) {
                                for (int i = 3; i < args.length; i++) {
                                    b.append(" ");
                                    b.append(args[i]);
                                }
                            }
                            String newString = b.toString();
                            ConfigChangeEvent e = new ConfigChangeEvent(pl, key, newString);
                            Bukkit.getPluginManager().callEvent(e);
                            cfg.set(key, e.getValue());
                            pl.saveConfig();
                            p.sendMessage("" + new Text("mcm.command.config.saved", lang));
                        }
                    } else if (cfg.isInt(key)) {
                        if (value) {
                            p.sendMessage("Current Value: " + cfg.getInt(key));
                        } else {
                            try {
                                int newInt = Integer.parseInt(args[2]);
                                ConfigChangeEvent e = new ConfigChangeEvent(pl, key, newInt);
                                Bukkit.getPluginManager().callEvent(e);
                                cfg.set(key, e.getValue());
                                pl.saveConfig();
                                p.sendMessage("" + new Text("mcm.command.config.saved", lang));
                            } catch (NumberFormatException ex) {
                                p.sendMessage("" + new Text("mcm.command.config.invalid_int", lang));
                            }
                        }
                    } else {
                        p.sendMessage("§cThis element can not be edited yet. Edit booleans via the gui.");
                    }
                } else if (key.equalsIgnoreCase("home")) {
                    new ConfigMenu(pl, pl.getConfig(), 0, lang).open(p);
                } else if (!value) {
                    cfg.set(key, value);
                    pl.saveConfig();
                    p.sendMessage("" + new Text("mcm.command.config.saved", lang));
                } else {
                    p.sendMessage("" + new Text("mcm.command.config.not_found2", lang));
                }
            } else {
                p.sendMessage("" + new Text("mcm.command.config.not_found", lang));
            }
        }
        return true;
    }

}
