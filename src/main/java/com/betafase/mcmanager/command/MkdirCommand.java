/*
 * This work is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by-sa/4.0/.
 * This Plugin is owned by Betafase Developers (betafase.com) and currently developed by iZefix (iZefix).
 */
package com.betafase.mcmanager.command;

import com.betafase.mcmanager.MCManager;
import com.betafase.mcmanager.security.ModuleManager;
import com.betafase.mcmanager.utils.Text;
import java.io.File;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 * © Betafase Developers, 2018
 *
 * You are not allowed to modify or edit this given code.
 *
 * @author Dominik B. (iZefix)
 */
public class MkdirCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender cs, Command cmnd, String string, String[] args) {
        if (cs instanceof Player) {
            Player p = (Player) cs;
            String lang = Text.getLanguage(p);
            if (!ModuleManager.isValid(p, "file")) {
                p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.no_permission", lang).toString());
            } else if (args.length == 0) {
                p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.mkdir.usage", lang).toString());
            } else if (args.length == 1) {
                File dir = new File(args[0]);
                if (!dir.getAbsolutePath().startsWith(System.getProperty("user.dir"))) {
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.mkdir.access_denied", lang).toString());
                } else if (dir.exists()) {
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.mkdir.file_exists", lang).toString());
                } else {
                    dir.mkdirs();
                    p.sendMessage(MCManager.getPrefix() + new Text("mcm.command.mkdir.success", lang).toString());
                }
            } else {

            }
        }
        return true;
    }

}
